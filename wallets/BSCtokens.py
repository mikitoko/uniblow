# Uniblow ERC20 list for BSC generated on 2021-12-13T10:38:02
tokens_values = [
    # Mainnet
    {
        "Binance Ethereum Token (ETH)": "0x2170ed0880ac9a755fd29b2688956bd959f933f8",
        "Wrapped BNB (WBNB)": "0xbb4CdB9CBd36B01bD1cBaEBF2De08d9173bc095c",
        "Binance BSC-USD (BSC-USD)": "0x55d398326f99059ff775485246999027b3197955",
        "Binance Cardano Token (ADA)": "0x3ee2200efb3400fabb9aacf31297cbdd1d435d47",
        "Binance USD Coin (USDC)": "0x8ac76a51cc950d9822d68b83fe1ad97b32cd580d",
        "Binance XRP Token (XRP)": "0x1d2f0da169ceb9fc7b3144628db156f3f6c60dbe",
        "Binance Polkadot Token (DOT)": "0x7083609fce4d1d8dc0c979aab8c869ea2c873402",
        "Binance Dogecoin Token (DOGE)": "0xba2ae424d960c26247dd6c32edc70b295c744c43",
        "Binance Avalanche Token (AVAX)": "0x1ce0c2827e2ef14d5c4f29a091d735a204794041",
        "Binance SHIBA INU Token (SHIB)": "0x2859e4544c4bb03966803b044a93563bd2d0dd4d",
        "Binance BUSD Token (BUSD)": "0xe9e7cea3dedca5984780bafc599bd69add087d56",
        "Binance Litecoin Token (LTC)": "0x4338665cbb7b2485a8855a139b75d5e34ab0db94",
        "Binance Uniswap (UNI)": "0xbf5140a22578168fd562dccf235e5d43a02ce9b1",
        "TRON (TRX)": "0x85eac5ac2f758618dfa09bdbe0cf174e7d574d5b",
        "Binance ChainLink Token (LINK)": "0xf8a0bf9cf54bb92f17374d9e9a321e6a111a51bd",
        "Wrapped UST Token (UST)": "0x23396cf899ca06c4472205fc903bdb4de249d6fc",
        "Binance Bitcoin Cash Token (BCH)": "0x8ff795a6f4d97e7887c79bea79aba5cc76444adf",
        "Binance Axie Infinity Shard Token (AXS)": "0x715d400f88c167884bbcc41c5fea407ed4d2f8a0",
        "Binance Dai Token (DAI)": "0x1af3f329e8be154074d8769d1ffa4ee058b1dbc3",
        "Binance NEAR Protocol (NEAR)": "0x1fa4a73a3f0133f0025378af00236f3abdee5d63",
        "Binance Cosmos Token (ATOM)": "0x0eb3a705fc54725037cc9e008bdede697f62f335",
        "Binance BTCB Token (BTCB)": "0x7130d2a12b9bcbfae4f2634d864a1ee1ce3ead9c",
        "Binance Elrond Token (EGLD)": "0xbf7c81fff98bbe61b40ed186e4afd6ddd01337fe",
        "Binance Ethereum Classic (ETC)": "0x3d6545b08693dae087e957cb1180ee38b9e3c25e",
        "pTokens GALA (GALA)": "0x7ddee176f665cd201f93eede625770e2fd911990",
        "Binance Tezos Token (XTZ)": "0x16939ef78684453bfdfb47825f8a5f714f12623a",
        "Fantom (FTM)": "0xad29abb318791d579433d831ed122afeaf29dcfe",
        "Binance EOS Token (EOS)": "0x56b6fb708fc5732dec1afc8d8556423a2edccbd6",
        "MIOTAC (IOTA)": "0xd944f1d1e9d5f9bb90b62f9d45e447d989580782",
        "Binance BitTorrent Token (BTT)": "0x8595f9da7b868b1822194faed312235e43007b49",
        "Binance FLOW Token (FLOW)": "0xc943c5320b9c18c153d1e2d12cc3074bebfb31a2",
        "PancakeSwap Token (Cake)": "0x0e09fabb73bd3ade0a17ecc321fd13a19e81ce82",
        "Binance Maker (MKR)": "0x5f0da599bb2cccfcf6fdfd7d81743b6020864350",
        "Binance Zcash Token (ZEC)": "0x1ba42e5193dfa8b03d15dd1b86a3113bbbef8eeb",
        "Binance eCash Token (XEC)": "0x0ef2e7602add1733bfdb17ac3094d0421b502ca3",
        "Binance Basic Attention Token (BAT)": "0x101d82428437127bf1608f699cd651e6abf9766e",
        "Frax (FRAX)": "0x90c97f71e18723b0cf0dfa30ee176ab653e89f40",
        "Binance TrueUSD Token (TUSD)": "0x14016e85a25aeb13065688cafb43044c2ef86784",
        "Binance Compound Coin (COMP)": "0x52ce071bd9b1c4b00a0b92d298c512478cad67e8",
        "Binance Pax Dollar Token (USDP)": "0xb3c11196a4f3b1da7c23d9fb0a3dde9c6340934f",
        "Binance IoTeX Network (IOTX)": "0x9678e42cebeb63f23197d726b29b1cb20d0064e5",
        "1INCH Token (1INCH)": "0x111111111117dc0aa78b770fa6a738034120c302",
        "SafeMoon (SAFEMOON)": "0x8076c74c5e3f5852037f31ff0093eeb8c8add8d3",
        "Binance Paxos Standard (PAX)": "0xb7f8cd00c5a06c0537e2abff0b58033d02e5e094",
        "Ankr (ANKR)": "0xf307910a4c7bbc79691fd374889b36d8531b08e3",
        "Zilliqa (ZIL)": "0xb86abcb37c3a4b64f74f59301aff131a1becc787",
        "Binance Bancor Network Token (BNT)": "0xa069008a669e2af00a86673d9d584cfb524a42cc",
        "Binance yearn.finance (YFI)": "0x88f1a5ae2a3bf98aeaf342d26b30a79438c9142e",
        "Frax Share (FXS)": "0xe48a3d7d0bc88d552f730b62c006bc925eadb9ee",
        "Binance Ontology Token (ONT)": "0xfd7b3a77848f1c2d67e05e54d78d174a0c850335",
        "Binance Synthetix Network Token (SNX)": "0x9ac983826058b8a9c7aa1c9171441191232e8404",
        "wazirx token (WRX)": "0x8e17ed70334c87ece574c9d537bc153d8609e2a3",
        "JUST (JST)": "0xea998d307aca04d4f0a3b3036aba84ae2e409c0a",
        "Coin98 (C98)": "0xaec945e04baf28b135fa7c640f624f8d90f1c3a6",
        "Binance Celer Token (CELR)": "0x1f9f6a696c6fd109cd3956f45dc709d2b3902163",
        "Binance Cartesi Token (CTSI)": "0x8da443f84fea710266c8eb6bc34b71702d033ef2",
        "Binance PAX Gold (PAXG)": "0x7950865a9140cb519342433146ed5b40c6f210f7",
        "AlphaToken (ALPHA)": "0xa1faa113cbe53436df28ff0aee54275c13b40975",
        "Reef.finance (REEF)": "0xf21768ccbc73ea5b6fd3c687208a7c2def2d966e",
        "Alien Worlds Trilium (TLM)": "0x2222227e22102fe3322098e4cbfe18cfebd57c95",
        "Swipe (SXP)": "0x47bead2563dcbf3bf2c9407fea4dc236faba485a",
        "PolkastarterToken (POLS)": "0x7e624fa0e1c4abfd309cc15719b7e2580887f570",
        "Anyswap-BEP20 (ANY)": "0xf68c9df95a18b2a5a5fa1124d79eeeffbad0b6fa",
        "Binance COTI Token (COTI)": "0xadbaf88b39d37dc68775ed1541f1bf83a5a45feb",
        "Sun Token (SUN)": "0xa1e6c58d503e4eee986167f45a063853cefe08c3",
        "Trust Wallet (TWT)": "0x4b0f1812e5df2a09796481ff14017e6005508003",
        "BSC Conflux (bCFX)": "0x045c4324039dA91c52C55DF5D785385Aab073DcF",
        "DODO bird (DODO)": "0x67ee3cb086f8a16f34bee3ca72fad36f7db929e2",
        "ALICE (ALICE)": "0xac51066d7bec65dc4589368da368b212745d63e8",
        "BakeryToken (BAKE)": "0xE02dF9e3e622DeBdD69fb838bB799E3F168902c5",
        "Binance ELF Token (ELF)": "0xa3f020a5c92e15be13caf0ee5c95cf79585eecc9",
        "SafePal Token (SFP)": "0xd41fdb03ba84762dd66a0af1a6c8540ff1ba5dfb",
        "Venus BTC (vBTC)": "0x882c173bc7ff3b7786ca16dfed3dfffb9ee7847b",
        "Venus (XVS)": "0xcf6bb5389c92bdda8a3747ddb454cb7a64626c63",
        "Prometeus (PROM)": "0xaf53d56ff99f1322515e54fdde93ff8b3b7dafd5",
        "pTokens TLOS (TLOS)": "0xb6c53431608e626ac81a9776ac3e999c5556717c",
        "Wrapped MIR Token (MIR)": "0x5b6dcf557e2abe2323c48445e8cc948910d8c2c9",
        "RFOX (RFOX)": "0x0a3a21356793b49154fd3bbe91cbc2a16c0457f5",
        "Binance Band Protocol Token (BAND)": "0xad6caeb32cd2c308980a548bd0bc5aa4306c6c18",
        "StandardBTCHashrateToken (BTCST)": "0x78650b139471520656b9e7aa7a5e9276814a38e9",
        "FEGtoken (FEG)": "0xacfc95585d80ab62f67a14c566c1b7a49fe91167",
        "Venus ETH (vETH)": "0xf508fcd89b8bd15579dc79a6827cb4686a3592c8",
        "Ellipsis (EPS)": "0xa7f552078dcc247c2684336020c03648500c6d9f",
        "Binance Kyber Network Crystal Token (KNC)": "0xfe56d5892bdffc7bf58f2e84be1b2c32d21c308b",
        "BNB48 Club Token (KOGE)": "0xe6df05ce8c8301223373cf5b969afcb1498c5528",
        "Binance Smooth Love Potion (SLP)": "0x070a08beef8d36734dd67a491202ff35a6a16d97",
        "Automata (ATA)": "0xa2120b9e674d3fc3875f415a7df52e382f141225",
        "beefy.finance (BIFI)": "0xCa3F508B8e4Dd382eE878A314789373D80A5190A",
        "bZx Protocol Token (BZRX)": "0x4b87642aedf10b642be4663db842ecc5a88bf5ba",
        "Venus USDC (vUSDC)": "0xeca88125a5adbe82614ffc12d0db554e2e2867c8",
        "Chess (CHESS)": "0x20de22029ab63cf9a7cf5feb2b737ca1ee4c82a6",
        "Binance YFII.finance Token (YFII)": "0x7f70642d88cf1c4a3a7abb072b53b929b653eda5",
        "CertiK Token (CTK)": "0xa8c2b8eec3d368c0253ad3dae65a5f2bbb89c929",
        "Tokocrypto Token (TKO)": "0x9f589e3eabe42ebc94a44727b3f3531c0c877809",
        "LTO Network (LTO)": "0x857b222fc79e1cbbf8ca5f78cb133d1b7cf34bbd",
        "Binance Komodo Token (KMD)": "0x2003f7ba57ea956b05b85c60b4b2ceea9b111256",
        "Binance APENFT Token (NFT)": "0x1fc9004ec7e5722891f5f38bae7678efcb11d34d",
        "decentral.games on xDai from xDai ($DG)": "0x9fdc3ae5c814b79dca2556564047c5e7e5449c19",
        "AlpacaToken (ALPACA)": "0x8f0528ce5ef7b51152a59745befdd91d97091d2f",
        "Litentry (LIT)": "0xb59490ab09a0f526cc7305822ac65f2ab12f9723",
        "Cryptonovae (YAE)": "0x4ee438be38f8682abb089f2bfea48851c5e71eaf",
    },
    # Testnet
    {},
]

# From Ledger trusted tokens data
# extracted on 2022-01-18
# from https://github.com/LedgerHQ/ledgerjs/raw/master/packages/cryptoassets/data/bep20.js
ledger_tokens = {
    "0x111111111117dc0aa78b770fa6a738034120c302": {
        "ticker": "1INCH",
        "signature": "3045022100f458aefa96e25e6cb5a94844e2ed97b44423e8ffcc9a7d5c58b4c3c06940363602206f0abd560f43bb48a34c65d3bb405ef0dcaaae6ad1929d3944188cd884968a75",
    },
    "0xa35d95872d8eb056eb2cbd67d25124a6add7455e": {
        "ticker": "2030FLOKI",
        "signature": "304402203386107b2e6df4c2990e8b7ab14c2007e367f3adab828b53905e2b80c6fafd5a02202eaefe459911f8ddea5d2b509dd79662dc75ca0f1713a5a68656e54edebe13fc",
    },
    "0xbacc6021b4bf2839b479b1624b50fe53175ea3c9": {
        "ticker": "ABPS",
        "signature": "304402200d5a316c7d00e41dfb34fb55d8ddabc919c1975404398deb00db35cdbbb8fc5502200541076307d50200e93beee541e9bd29fca71b4f52c3f964c4f2e9379f42d90b",
    },
    "0x4197c6ef3879a08cd51e5560da5064b773aa1d29": {
        "ticker": "ACS",
        "signature": "30450221009db57aa43958ff0a161ad57cd06de88bcb8ee706902111cce327b6b94c8888b002206b93c192bdafb37ef509752fd49e69f2cdc18d433f35e2e3142fa14226f15aa8",
    },
    "0x6bff4fb161347ad7de4a625ae5aa3a1ca7077819": {
        "ticker": "ADX",
        "signature": "30440220038d0609e07fc7bd49bd7eac1346c4f47e2c9e66eea1d706a41d2c35a9591b15022013db6d0678ab538a656189606013df31be8a34b31a146e51eee888e6abfa9ba4",
    },
    "0x82d2f8e02afb160dd5a480a617692e62de9038c4": {
        "ticker": "ALEPH",
        "signature": "304402205bb71dff2af470a017a95e74c2b899ea93b97c38ebc96be9926f689c578510f302205639049579d2414e8d40658cad383b07e04e4506917f6484d4c5edda61b5ea24",
    },
    "0xac51066d7bec65dc4589368da368b212745d63e8": {
        "ticker": "ALICE",
        "signature": "3045022100cdc2fcac9243455589263f391ad602313e14042dfc09f49296a85c17689d248902203742a633d9492b9751f794aec20a3ee933f97b393f6ccdd94804f86310eeb47a",
    },
    "0x2222227e22102fe3322098e4cbfe18cfebd57c95": {
        "ticker": "TLM",
        "signature": "3045022100f2cdbd413d3730b20cf202388b90361c8eabc8efa7425b82ad66d773c36788640220248321f8e722496587a7ee020ff8c6125519c376084bbf72331ed705d6220dcd",
    },
    "0xaf6bd11a6f8f9c44b9d18f5fa116e403db599f8e": {
        "ticker": "ALIX",
        "signature": "304402207c2748fec4a398471e490c8b86e22274f796f884a06c598636e0ff12fc732d3602204418194f7fad146beea1985d80ab67bc5e825ac4a8a94f0e61891085b8ff3d4b",
    },
    "0x72faa679e1008ad8382959ff48e392042a8b06f7": {
        "ticker": "BALBT",
        "signature": "304402201484d548d250d6474d38ca346157fe6822677ecc2c7e7278160b7e938fe73c0202202df4d2ab14ea0b364d54a219983f4eb610ae842f83aa30d98ecd4f04a3df8f8f",
    },
    "0x8f0528ce5ef7b51152a59745befdd91d97091d2f": {
        "ticker": "ALPACA",
        "signature": "304402202d18a43ad7cfb5e0d28f863eaa497fd4d943a1fb532d2fd8c53f51c19e679b05022066ead1024d13184fde7ecad18de6bd71ab7c5f72041f973e43b763f0ccf10061",
    },
    "0xc5e6689c9c8b02be7c49912ef19e79cf24977f03": {
        "ticker": "ALPA",
        "signature": "30450221008c2cac65098b9a821cd4ea52b6c46d1f9a3780c665b1cea955fb941ad1a64a1502201fe087f6fa40c704746ff89419550590908857bbd99a19befdaef25f7a8bb090",
    },
    "0xa1faa113cbe53436df28ff0aee54275c13b40975": {
        "ticker": "ALPHA",
        "signature": "304302207a2126615316bded163a00be992254e05cbd91ffcf6b9e6658fa0db29f00e9d7021f05715d1add0fd77d649701196434e53640d758c9a93ac2d4c49fb5c47cb3ae",
    },
    "0x8263cd1601fe73c066bf49cc09841f35348e3be0": {
        "ticker": "ALU",
        "signature": "3044022060daf4feddc30398c794e20eb78a46ea30b5d4d0d8f0f5b010ff85c6aab0dd3b0220717168e3e78d18fe02eada538f4538b83c3653a544e273fe9a88bfb8b8b58d90",
    },
    "0x1496fb27d8cf1887d21cac161987821859ca56ba": {
        "ticker": "AMC",
        "signature": "3045022100d82751a86f7c1a9ece49336727f4b0553d5a1dfd3dc66b18a3dbc2211a0c739602202c108e682dd1f8d4f0f02356064ff85376a9fd8f1ec6cf18238c749e20fd8114",
    },
    "0xdb021b1b247fe2f1fa57e0a87c748cc1e321f07f": {
        "ticker": "AMPL",
        "signature": "3045022100a79fac1e35ca7704737e2553047f93f81c98db83e70205d1de7a7ac8275dbb24022013e27384c5accba40810bebfe298ca2d8cdf5e7b94becc82b1b30c141cf56d3c",
    },
    "0xf307910a4c7bbc79691fd374889b36d8531b08e3": {
        "ticker": "ANKR",
        "signature": "304402202add7916acffae47b8f0759f4d63be12af634a2f5bcc1d39813a4b82ee41044702206ff71760a3d9221872239f1c47b657ee49b8b0a6e74cceca3ffb079c9ab8f886",
    },
    "0xd283ce7acd177f7cf6cabb7ce5b11fda3605659e": {
        "ticker": "ANT",
        "signature": "3045022100aecedf9306e1d20d117431c493a467ddd21ee404fb2d41f943a47a3636f919e402202bbe78eb8e62d497be6ffa77dffe4beee7be64ffb8c59b31674595b4aa93e5c0",
    },
    "0x1c9491865a1de77c5b6e19d2e6a5f1d7a6f2b25f": {
        "ticker": "MATTER",
        "signature": "304502210098c9cfd045b70a041c7ef86010c925dafec6e268f9cdf8c2dcd7b063b6bedc3f022058f65872d4eed5e5c828b5ec84fb5f62bc78cccde1287e11260cbdd8622107ba",
    },
    "0xf68c9df95a18b2a5a5fa1124d79eeeffbad0b6fa": {
        "ticker": "ANY",
        "signature": "30440220735b8a50c59469e10a4ebb47b53bf2dce2f729eb969d2090e7a64b5e00d8c12f02207ea78aa34999644c682c11a5ba1da681075e95b52ab6e5ef870ecfa4d1607307",
    },
    "0x603c7f932ed1fc6575303d8fb018fdcbb0f39a95": {
        "ticker": "BANANA",
        "signature": "3045022100815163c98e170a4040276777988c30a57efedc3eac7765597405804d2378fd6b022074ddb0fbf6ba84a103a18f3038c94fadb2dcab43103328d2da7a8a956eee0dfa",
    },
    "0x37dfacfaeda801437ff648a1559d73f4c40aacb7": {
        "ticker": "APYS",
        "signature": "3045022100ce6a94159a39d8133162df62c3bf14df055ef86a233c1dd6588487a10516a582022046ca53550997dde57c95f5d1e50c9653ed27dd03e7e1f081c4b6c22fe8e96768",
    },
    "0x851f7a700c5d67db59612b871338a85526752c25": {
        "ticker": "ARGON",
        "signature": "3045022100ed3d4dbc957f6552fad6a1816775b35c372f8814c4c9f0a2fdc98cd9d0a66aa30220228f89b65fec25416045d1f17b2f63c110f329dfd41c6b3fc3301b76842012c8",
    },
    "0x6679eb24f59dfe111864aec72b443d1da666b360": {
        "ticker": "ARV",
        "signature": "304402206658baedaed45bd0817c375c1834782e48413e4eff91d185393b24531b8e51920220270d41be4cc4b60291a6f8f2139b34cb01a77fd851ee05e9a563f5c7b89c9b13",
    },
    "0x6f769e65c14ebd1f68817f5f1dcdb61cfa2d6f7e": {
        "ticker": "ARPA",
        "signature": "3045022100e8baadd41acfaccdbad905fd9e848bd7626c89726b14901b6205ad0e6e8e7ad502201d8fe7a40960f99b4ddf8b6ca81bf67504f3a9cb466e5ab1e73927630a472638",
    },
    "0xb63a911ae7dc40510e7bb552b7fcb94c198bbe2d": {
        "ticker": "ANDX",
        "signature": "304402204f839dd862ab8dc58d2eed1bcdfed8b74d39670030cc835f412afa852a47f6a502203db98be602bcf875ef6a5227c746290e9b078df0a8a61f7238ac301822195d58",
    },
    "0x8ea2f890cb86dfb0e376137451c6fd982afefc15": {
        "ticker": "AU",
        "signature": "3045022100fa89f199a007b7c311b934828fb401db0096804aeea4581a0101b744f9769f53022027f89e912c6912d066382dc6b501a2911c9d1d7009977520b7f76adec7e47efe",
    },
    "0xa2120b9e674d3fc3875f415a7df52e382f141225": {
        "ticker": "ATA",
        "signature": "3044022062c9174c8883a589f4baca24f411c2d540d36dd03bf3b4a7516b81743ffc0e5f02202e3b88d614101d579eaaceb95ff7258652d82a662c65ea7b5e0995e73b209a0d",
    },
    "0xa184088a740c695e156f91f5cc086a06bb78b827": {
        "ticker": "AUTO",
        "signature": "3045022100d6e8db850af803d2ca8518018c679290c9c4ebda1828165ce1d384158d31943b0220092463c0307ca34df503c38623c5643250791bf0be490433fa8fafbcfc3f6a5c",
    },
    "0xdb8d30b74bf098af214e862c90e647bbb1fcc58c": {
        "ticker": "BABYCAKE",
        "signature": "304402205aee7cc15d1cab508266ea97438c6f9951718716d9a01293e647ba6dc333a9900220782122970b8e1db1258c44068c79a3cc2336009a53896568084f60c358907721",
    },
    "0xc748673057861a797275cd8a068abb95a902e8de": {
        "ticker": "BABYDOGE",
        "signature": "3045022100d976a6ff1b1d7542d53329d0e1a9bf64d86c5db6df704dc829db10ed0a03c0c002205e0c9b4cb87e700f9b4be7d292cc8f1c0a41b03fe7cde92ed33eedc67dec56a8",
    },
    "0xe24f6bc7635501d5684a963f7e75c8062b9c0ea4": {
        "ticker": "BABYFD",
        "signature": "30440220314c9990ab8d6563949d36659aeadefa4682161da19f013d37717538f0c34240022070b29118dbeff415ec103a555c239c527d5e9bedf876a378167f194c92b44239",
    },
    "0x1f7216fdb338247512ec99715587bb97bbf96eae": {
        "ticker": "BBADGER",
        "signature": "3044022015d0950a161b1c1015a8d610ee1ed37bcbd3148b0245611677d4a2fa97ed4459022024258f2f79bcad1a09be1f08e3bd239c0dddea0dfb6d71874267cd2862edcfa8",
    },
    "0x5986d5c77c65e5801a5caa4fae80089f870a71da": {
        "ticker": "BDIGG",
        "signature": "3045022100ad8c95042810ee02d328d04baee99f22242cdedd0c164317bdd91e7fdb5a96480220540989095ce8c772e8bfe3a8e4aeb21846e4efe8cf48880cdb02f09a5ef64d4b",
    },
    "0xe02df9e3e622debdd69fb838bb799e3f168902c5": {
        "ticker": "BAKE",
        "signature": "304402200920cfe3227750f1284f8eb3cc7d02d3f78bc6c7d286f318e96009b2b17093ec022048f40792fc07accb1cbe6a9820f0b7712b93a7262fccbf4d7483b7e27cf65624",
    },
    "0xd1102332a213e21faf78b69c03572031f3552c33": {
        "ticker": "BTD",
        "signature": "304402203e81e9a342161898ef32ae7e1c0e239b3bf2f96e2f91db465b4711628d4ce7420220662a846d816cf5d260050c8b019427d3b3b307cf28e3b6029d31958180189b94",
    },
    "0xc2e1acef50ae55661855e8dcb72adb182a3cc259": {
        "ticker": "BTS",
        "signature": "3044022049ab4464b5eb403fd61d323465b2eb07a46b4fe41102937ff016ee5182d1b62c02205710e789443b9d40bd4ac64a9f0682f33fbf2b1a2bd1520e1de4fdfcc0cca6e6",
    },
    "0x190b589cf9fb8ddeabbfeae36a813ffb2a702454": {
        "ticker": "BDO",
        "signature": "304502210083c83b95375cf923e699e96c26976da3e482ca2612e996eca92dba8ad6853a0602202619a70e00e95d011ba3163c4d01902a4db50f8baa515f85df2119cf6fc2462c",
    },
    "0x0d9319565be7f53cefe84ad201be3f40feae2740": {
        "ticker": "SBDO",
        "signature": "3045022100dfa10eebc2e9f6daa9236fd1b8718e05d2c147f63179b4112e83c2c97577584502202c05c9b403b22cbc37c1a840497b1147fb2b81cdca0eefd1f10e785b31ece901",
    },
    "0xc3eae9b061aa0e1b9bd3436080dc57d2d63fedc1": {
        "ticker": "BEAR",
        "signature": "3045022100a25917a158032d5cd8e93d36ee00bcdff85097d3a28a9c0736e2fc141910748a02205b9672e52202d2c93037d74e482070be1012ba5f54e5216522eb3b1f39ce00d9",
    },
    "0x81859801b01764d4f0fa5e64729f5a6c3b91435b": {
        "ticker": "BFI",
        "signature": "304402204f7ed49718aac3dbaae58d8319a31206621d9d65b3a635f1925e9509d41d842a0220314bc8026232c8deecc261357f920d0ed2a10714e850b67c78c907b3bac56222",
    },
    "0xca3f508b8e4dd382ee878a314789373d80a5190a": {
        "ticker": "BIFI",
        "signature": "3045022100ac554cb95ff98b8f187500fe37767d5a80843a7a9bf1d53f531eb7da9e65e0a2022069ba439a0d51d2adf380dfa03e6e49aeac4d8e39642591984b16fce1c89f9118",
    },
    "0x8443f091997f06a61670b735ed92734f5628692f": {
        "ticker": "BEL",
        "signature": "304402200a3b085899b2d4c752afb1850b7b6d8d4c11bc63829c4ff2ebf03676658eb0910220582235f28974d4b1a299967f036b2dca6aa538af1eae75f78a598a3e5bc53588",
    },
    "0xe0e514c71282b6f4e823703a39374cf58dc3ea4f": {
        "ticker": "BELT",
        "signature": "304402205c57da4cf8775a880920fa8a2560dd215eb83156e72e2fe81c61d69b08495cbf022031e09cd7a519d2d0b56073ef84240916bf78769ffe461fc5f9fcdb80d27a94ef",
    },
    "0x7b86b0836f3454e50c6f6a190cd692bb17da1928": {
        "ticker": "BEM",
        "signature": "3044022040592cba4cd3db822d7d0e615c1742c6e9203e2b1d816b2526b51168b676244302200fbb5169eba444a86ff905c8be6551673eb92784272a8b01e4866e3e8c778f1d",
    },
    "0xf859bf77cbe8699013d6dbc7c2b926aaf307f830": {
        "ticker": "BRY",
        "signature": "3045022100af07d19d5f0e1d58bc15e9164270413f4ae521c70b50236521a0e6ee3b68aa1602207c5c6b23d24f5fc9d79d2df1f35378589dde1da32d3dc602fa0e7cab85baf4dd",
    },
    "0xbe1a001fe942f96eea22ba08783140b9dcc09d28": {
        "ticker": "BETA",
        "signature": "3044022005d9961efec6f1ab580207a6a5a53b5ff12e462d5fe336f697e1c75ae2bbb5e7022069543b6760af9ef5d84a7869791d55027cb51a3aeb4682c1310ac00a34d467c0",
    },
    "0xf8e026dc4c0860771f691ecffbbdfe2fa51c77cf": {
        "ticker": "BGOV",
        "signature": "3045022100cf79ba48dfb2c445fde5690218b0002f80e1a125a9d122610e80dc805d8388cb02200d03f6cabd29425b14bde330dd544615c28d506dc70381f389d2fa106d9b406d",
    },
    "0x9a2f5556e9a637e8fbce886d8e3cf8b316a1d8a2": {
        "ticker": "BIDR",
        "signature": "304502210088c768f6e9986c7d33825bd32764a1fa450da0703070eb05f234a63d28ee93fe02203341dd1e296a55a2db521be17bd30567783b19b766091c9143bee8face1ee2c7",
    },
    "0x6fd7c98458a943f469e1cf4ea85b173f5cd342f4": {
        "ticker": "BHC",
        "signature": "30440220267b139031d4f08fc190ae6847ea6d5a02d910446e287687dce6cf59e08de0df022055516400d6f26b4c6bfc712f3a4069d10fc04af4fce554488e9731e96335989f",
    },
    "0x08ba0619b1e7a582e0bce5bbe9843322c954c340": {
        "ticker": "BMON",
        "signature": "3044022024fc1ba1d2bc77213204645c178e68b14d0d77fdfb634761c799720247933f62022074b6aaeaa50cb9ba30443e08972109425f38abdfeb4512a497bb8563ce3025b0",
    },
    "0x1fc9004ec7e5722891f5f38bae7678efcb11d34d": {
        "ticker": "NFT",
        "signature": "3045022100fd1a905a42079ab7ba5aca84d94e0811ad9d8f4b00bf2c00bd29f086293c3944022047941c9f777ed3ab2d95c6c380e3082eab21d5767d553d383e2af3527d11dc49",
    },
    "0x80d5f92c2c8c682070c95495313ddb680b267320": {
        "ticker": "ASR",
        "signature": "3045022100ec035f727b79416b26f719dc43520b8ffe97b263314ba614f6ebf7d22bdb69d602202447bf01cfd0591e7cb351145535ec6e22da69d89a76e25aaeb21e8d6483fc57",
    },
    "0x25e9d05365c867e59c1904e7463af9f312296f9e": {
        "ticker": "ATM",
        "signature": "3045022100e758cfcbc3660212f49519f0c6dfb3d1e61bc8adb79fbdfcaa0980142b579f6102202525563a7821c93e8088ca2e0f6848e3fc12dad083aae4ef2cab3d5bb61d2f35",
    },
    "0x715d400f88c167884bbcc41c5fea407ed4d2f8a0": {
        "ticker": "AXS",
        "signature": "30440220691a3f644df94eec922082948417fb14d9e53ea4739cde2606f4234d3f9fc04302203709295b811f3270a16cdf05f190f3687df9c4b12314547c7e85ca3992ff18bd",
    },
    "0xad6caeb32cd2c308980a548bd0bc5aa4306c6c18": {
        "ticker": "BAND",
        "signature": "30440220541e7015f2438030d38e8bde19e1e3115bb690c1783aedb168b1d58cc8751a2f02204d33e627417bac5a7080879950d7e6cf4edcb654918ead0f46c2315e01dcb8e6",
    },
    "0x101d82428437127bf1608f699cd651e6abf9766e": {
        "ticker": "BAT",
        "signature": "30450221009bce750c593485069075887224100edd1ddb1c1c19cbec73b83a73228e3567d302202708bd7e5821ac2f7990de1912ac978efc09cf0ac59cab3118a21cd5dec39bd0",
    },
    "0xd475c9c934dcd6d5f1cac530585aa5ba14185b92": {
        "ticker": "BCHA",
        "signature": "3045022100d48bbd491db608f0947b3564160302affcfbbcca33b8c2bfc591c59f8d4402490220404696d386357fe5cdd11c0f354a49be4c3f2cf011578f2c28cf86041321da75",
    },
    "0x8ff795a6f4d97e7887c79bea79aba5cc76444adf": {
        "ticker": "BCH",
        "signature": "30440220233969c3747ec78543eb1d94bd7f0cf20cbffffe84ec6135de77a909baaf2f0b022071d8918e188de7cf0805bb1382d7fefdb604aacad7c5867f0737289499670525",
    },
    "0x55d398326f99059ff775485246999027b3197955": {
        "ticker": "BSC-USD",
        "signature": "3045022100993a05cf5f51ffc13a81fadb447b786554878b86a6f90de134f20801b19eb69c02205d96974ec1d6d273cbebab802b2a865e07aab7453dc1040b045bc7b4df827e3f",
    },
    "0x7130d2a12b9bcbfae4f2634d864a1ee1ce3ead9c": {
        "ticker": "BTCB",
        "signature": "304502210096f3bcb3258c81fecd1bd4719dcb76a16702ba3bf6e868cf39b6329527ea747a0220130660724fe448a9cd5d82ac7739012af01ddd27c287a0dcc2d7cf8e88b1e5dd",
    },
    "0xe9e7cea3dedca5984780bafc599bd69add087d56": {
        "ticker": "BUSD",
        "signature": "304502210083b2c0ea8e139a888102c27c92cc72122ccceb1d6d0c3b8fa0dd4ae812ef5f8e022006a81899fee9303289661835a214b4a3075800c399bbcefd9b82164e6d9bd2d6",
    },
    "0x3ee2200efb3400fabb9aacf31297cbdd1d435d47": {
        "ticker": "ADA",
        "signature": "304402204aef1d3138897d64f744f9079da345f16ab1d6c87e73f6d13ae31519f2f8b789022010113193f7a20c5711d86053a5ff2917ee1e8f3dba62a087cd9ddb1d40c15122",
    },
    "0x1f9f6a696c6fd109cd3956f45dc709d2b3902163": {
        "ticker": "CELR",
        "signature": "30440220322f50dcaad786a24d8a2aae394ba4e923fb2d7253bc89a7bb08a56bbe9ceb5402201123873fa6f6b9c2511ab97eae832a06344772d237a9de4e95896022c23e3dd7",
    },
    "0xf8a0bf9cf54bb92f17374d9e9a321e6a111a51bd": {
        "ticker": "LINK",
        "signature": "304402205431e59f9522a8c01000362fa46ce3a89581755d2cad6c6754bded24fa9cc2e602201094d4d25697370f6975a856240638b2f0be30ae843354428853baa5d0637c7b",
    },
    "0x52ce071bd9b1c4b00a0b92d298c512478cad67e8": {
        "ticker": "COMP",
        "signature": "30450221008df86e4ee8e9eaaf159eb83789b755c9d649f0ad82a537602190309d41812ebe02203b4f46fe6758c149ed129129a5b27d88e2ba71f58395d01ba40bf4e895db153d",
    },
    "0x0eb3a705fc54725037cc9e008bdede697f62f335": {
        "ticker": "ATOM",
        "signature": "304402200265f641211f4c7e1664b12cd7f279f0706182fefba819250324e95bcec22ba8022029ccd6cc80cb62ef900eaa793c6b3717aa01ee9728fe6e0473e487123aca7d6c",
    },
    "0x1af3f329e8be154074d8769d1ffa4ee058b1dbc3": {
        "ticker": "DAI",
        "signature": "3044022032f0a880722af8c9e2196b5c0fc5273e2088f23692bdd2b35f6cf41c4001213f02205226e2023e409c73b031c790c64ae24db67c04b0aefd0d979b8c5002ca969b7b",
    },
    "0xba2ae424d960c26247dd6c32edc70b295c744c43": {
        "ticker": "DOGE",
        "signature": "3045022100d602d79c48bfd25c7557e77acf44fa8232d985ac9170cc4f2db1bcda4911a63a022021bced9bf580c60a8d704ecbb1077f33b7d82a0ea0319d1857e38758d9c02840",
    },
    "0xa3f020a5c92e15be13caf0ee5c95cf79585eecc9": {
        "ticker": "ELF",
        "signature": "3045022100a9c2e713f2cb904296141ac797b34b2eddae36f36b251bd724b126416bded24402207bfae23672e959106ed750ebc279e11fa8c9e56754008953942a062caf7db7eb",
    },
    "0xbf7c81fff98bbe61b40ed186e4afd6ddd01337fe": {
        "ticker": "EGLD",
        "signature": "30440220647de155e8219c377998b4973d7dea1c2ccd5e56676d0e408b3ae6347d09db8602203ff9be2beb490222c40580737e6cde604f826fc893582d4f12a2c2e020990b45",
    },
    "0x56b6fb708fc5732dec1afc8d8556423a2edccbd6": {
        "ticker": "EOS",
        "signature": "3045022100e417b8c4ce7e76014bb7fcc6f6510656b0731e4b2868c840f034137b38ae9d4e022026c0a0e29d81f25d686617748a91fd8bd1511677a689318c7a9f5cb5772f3960",
    },
    "0x3d6545b08693dae087e957cb1180ee38b9e3c25e": {
        "ticker": "ETC",
        "signature": "3044022019d614d743530878e1cbb2c1fea75dae3f88a4c8c585b36dc266f628aedb443002204c83516c9a3c57d1f1b25c62fc9a25caf7dec17dd0fa3c2cbafebc3751281f8c",
    },
    "0x2170ed0880ac9a755fd29b2688956bd959f933f8": {
        "ticker": "ETH",
        "signature": "3045022100878f1594ddcc979473abdf4766c0d3fedcad37f82723402e7be382fb8042d40b022021e1c0c4bf00e22e277b43aab7017fb2150992071a81e2c38bdf2f3e6252d7a1",
    },
    "0x0d8ce2a99bb6e3b7db580ed848240e4a0f9ae153": {
        "ticker": "FIL",
        "signature": "3045022100c0d73d3db2fca55676a87244eb5b3c24cf5e8b9fec3db0421ef88a34e7732b8702206ca6243b6652b279041fb710dfa3aed7bcd8d9c5ca01c945d989096356b56fb4",
    },
    "0x9678e42cebeb63f23197d726b29b1cb20d0064e5": {
        "ticker": "IOTX",
        "signature": "3044022056acbdcb27811703724a7db8633489b23f7d140aaa0fb2e05a0f74654fdfbf2a02205e5aef8ec78652a8bf5e3ccd16c7ebabd3dd09016b9a51cb3352fa1e1de3f291",
    },
    "0xc40c9a843e1c6d01b7578284a9028854f6683b1b": {
        "ticker": "JUV",
        "signature": "3044022050d3671d19c8b74f77eef13df1bdbf2a3b1ad46eee6e1cb1f0fc443ea699c9d902201b833869d05bbc295af0a0cc965b970ebd5bc14c7cf238547026083d0f8c3f0d",
    },
    "0x4338665cbb7b2485a8855a139b75d5e34ab0db94": {
        "ticker": "LTC",
        "signature": "304402204c3f04dd82e5e0bdc2dba057b33110bb1639dc351f6fe4c288f1f0465fa06191022068bc63c2a0afb33bc684a6c04f3622c4538b6c160abb47e06eb298a26ed0faa4",
    },
    "0x5f0da599bb2cccfcf6fdfd7d81743b6020864350": {
        "ticker": "MKR",
        "signature": "3045022100f2646fd21db7873ab60ab660d3e6743faffda8a0d31b9430c2c0feacaec3f589022011f0d99965303a6c4be7d37f5ea6ecbd59d24b53319c2598b865f2f82bceea88",
    },
    "0x1fa4a73a3f0133f0025378af00236f3abdee5d63": {
        "ticker": "NEAR",
        "signature": "30450221009b575a5e68b3b1ebef9c87dc37c3ca204b9e710317b1c4ff22c38c6fc957d85e02203105546e8cfe49a3bdc3ac7991f95bc7e898426343718d7270a316376f3d597b",
    },
    "0xf05e45ad22150677a017fbd94b84fbb63dc9b44c": {
        "ticker": "OG",
        "signature": "3045022100faff7630ea7ca1955a56432e17f7c0fe4b4ef28f9161e856356369e33f705929022049522ec4d3775dd01471303a155fa7fd99f73c7110298de00754ed4488cf1f85",
    },
    "0xfd7b3a77848f1c2d67e05e54d78d174a0c850335": {
        "ticker": "ONT",
        "signature": "3045022100e255c684cff078713c2b06500a2f64de6521a75350965a4ec309be877e3a537802203827b03bcd63075da4e66b67432270da8b4896b4a04d5b28fbfa5d1ce72541bc",
    },
    "0xbc5609612b7c44bef426de600b5fd1379db2ecf1": {
        "ticker": "PSG",
        "signature": "3045022100957438dbfbcc9f813eb22b19aedaaa71fd69688757f89d297144be10cf5dfe6502206bda87f9ca9a98fe9e8ee90b8ab8c56c2f6b8faf198e9858c902a7a857ab5d80",
    },
    "0x7950865a9140cb519342433146ed5b40c6f210f7": {
        "ticker": "PAXG",
        "signature": "3045022100d574787460d598e2850d13ff878c044760de596638dcf8e74d45d4ef8e7ee460022032bf9fa0c79daaf1b7014f351d7ee5d7b70acc82631c90a2306b3bd07c806ef4",
    },
    "0xb7f8cd00c5a06c0537e2abff0b58033d02e5e094": {
        "ticker": "PAX",
        "signature": "3045022100838e97908f67b324513e26a0b1a5e95ffb4870b47ff148aaebc64ab8703e06f10220701bc29b881dc98d71164cd5f0d1eec940de963555fa91d9114b5a995ba55d27",
    },
    "0x0112e557d400474717056c4e6d40edd846f38351": {
        "ticker": "PHA",
        "signature": "3045022100babb917993009e57a01f97f36e042c782604f79ee01a606a4bd40f1474c130c802203de0869f3f71d4561944cede9c6046d057cc4fa84f763d45224417b3ddaeef04",
    },
    "0x7083609fce4d1d8dc0c979aab8c869ea2c873402": {
        "ticker": "DOT",
        "signature": "3045022100cabec243c94be137588d34ddd414930656db44b7a7e639beefa0ba602b65866302206a3c6d7f869a02247d4441c111382468e8be5154ec39dddedda94f1ca7a6cad5",
    },
    "0x070a08beef8d36734dd67a491202ff35a6a16d97": {
        "ticker": "SLP",
        "signature": "3044022078826d7e891788673ba7fc76f6f08cacbad293a7232163bb8f8725966bbd0b2302200b7614a8a15833c5a7f551acdb4fa84dce253073f457e736f6de94825c4662e6",
    },
    "0x947950bcc74888a40ffa2593c5798f11fc9124c4": {
        "ticker": "SUSHI",
        "signature": "304402201ff768e7ccd4ea57c495bc84647cd34464e513278721d2a17f0ea996157a394a0220241f7131d9e22c64a793188a863c52017a0ba33a35d5580974676e242daf5f36",
    },
    "0x9ac983826058b8a9c7aa1c9171441191232e8404": {
        "ticker": "SNX",
        "signature": "304402206842b6147fb84cf4777b4dc46b3c545afc09e2769973db6cc343582b486deaeb02201cec631b2cd68064d09ab96b79f8f44568c20f9b7d156cfb827bcc9f8fbfc9b7",
    },
    "0x16939ef78684453bfdfb47825f8a5f714f12623a": {
        "ticker": "XTZ",
        "signature": "30440220125118a2ffeba944c149d96c49357341df6ad69f3068a9a804211a05f6c98d3802200eab3af7855afe4c3e3f6a8c1275227bd9d78b67617c6ec8a6801baea375f8c7",
    },
    "0xca0a9df6a8cad800046c1ddc5755810718b65c44": {
        "ticker": "TCT",
        "signature": "304402206eb0f6a0882dac690dd31ba1a9aa820e4b17edcfe15af5410b74dc647219990d022048b8ae77ce5b995fd646fa25fbbaab510858a7fbeb033dba596e0faa9eecc191",
    },
    "0x1ba8d3c4c219b124d351f603060663bd1bcd9bbf": {
        "ticker": "TORN",
        "signature": "304502210090682fe144ea267049e08b3e7f7484d69da8d531a587d3f8013d97e196e3b8ba02207e70901fc38ac7e6caa542f5907bb9a52b9f0e210e96bcdae68efd700e31b169",
    },
    "0x14016e85a25aeb13065688cafb43044c2ef86784": {
        "ticker": "TUSD",
        "signature": "3045022100f0e3223e33b59b779c28e5a16000f4d036f99a100bae7f6204beec799c7c73c302206a34d91b67dce72f423ba2437c52343ea882e0732a89c1f06ee662c224943def",
    },
    "0xbf5140a22578168fd562dccf235e5d43a02ce9b1": {
        "ticker": "UNI",
        "signature": "304402205673c99c2f001e6ec6203d3cd91d41dc56f6bc4defc30f193b44897600dc82bc022008717e1b72a6f3c572200cc7367994dce2ecedf2f1edbe1c3aee7487d9004a1a",
    },
    "0x8ac76a51cc950d9822d68b83fe1ad97b32cd580d": {
        "ticker": "USDC",
        "signature": "304402207f3646c152858f149b2f42ddd0494029943f978141d6b8738d2c374cb6a226260220381e2b61a56fc4935ac31c9bd9069c102deb3f4f642593c2d4b40931d28f7fc2",
    },
    "0x1d2f0da169ceb9fc7b3144628db156f3f6c60dbe": {
        "ticker": "XRP",
        "signature": "3045022100e537a0735a23d60464f2c4d7b70ca37664dd469db3e63890f965baaac7484fd8022005659afd4fe81bd2f300f33f1c365040da3644d470fb239c1f5daecfe34de4b4",
    },
    "0x88f1a5ae2a3bf98aeaf342d26b30a79438c9142e": {
        "ticker": "YFI",
        "signature": "3045022100a00526482764a6c459805cf5db1de35c37bd0aabda01f8440f5d277fd581881a02200268bf6e923c6db5b50dc45432347c1f49aad0ff6b456c539c4df4145bafeb72",
    },
    "0x7f70642d88cf1c4a3a7abb072b53b929b653eda5": {
        "ticker": "YFII",
        "signature": "30450221008c40710f4a82233333c3606839233a07d1b224ff0968974845e5fd652ab6067c022067d2f35a5c35a66ecb5893c0994ec072e7a0e675f78ac3feab9ea5a02375e7b9",
    },
    "0x1ba42e5193dfa8b03d15dd1b86a3113bbbef8eeb": {
        "ticker": "ZEC",
        "signature": "30440220515edc881d64df6cf2801a350d67fa12f346334a673cc1d1c1ebe43ffa3acdb50220069c4dcbfde448d7ac378abe5c4a0603cbb6efad0a03d4ad214803ac1a3851d1",
    },
    "0x250632378e573c6be1ac2f97fcdf00515d0aa91b": {
        "ticker": "BETH",
        "signature": "304402200e45fbcbe33ecc8168633464545d98fbf96b76ad6e4bb8e38c01cc5a5958aa9702202603b107e3cd9045d71e2586de8fcd0e97cb0e9f5e55fc92d53f4cbf5800bd8d",
    },
    "0x8c851d1a123ff703bd1f9dabe631b69902df5f97": {
        "ticker": "BNX",
        "signature": "3045022100d327feb400d6fc3a3b0d1ed3b7f0542812705619fa463f044a07ec98eff851ba022067ddcad06d896642c4788dd3056ba0a7c1fee82743b7930994656b3f07bcdca6",
    },
    "0xe56842ed550ff2794f010738554db45e60730371": {
        "ticker": "BIN",
        "signature": "30450221009c054cdb9c34d496f2500f78baa221eb1e9a1051679fcc50281be803cb593fc002204e2175189709a956236b671ae47d9c631ce0f8783059840dddf44ee7b3bb898e",
    },
    "0x5a16e8ce8ca316407c6e6307095dc9540a8d62b3": {
        "ticker": "BTR",
        "signature": "3045022100c1983ebdc81688427a60a82fe2b793c0d2570c66f82cdc2dc6a1b6c18e8a679e022062c62fbc61ca4b1e3ff09b29251cc8fec5f104213f25e7a9c5a64f458efae892",
    },
    "0x8595f9da7b868b1822194faed312235e43007b49": {
        "ticker": "BTT",
        "signature": "304402204d6885b398474d27210a28843c025c9de2b46454a1aa3f76018290b89ffddefa02203dfcbbb646bd20f610bfb2a908e53ef647108e0be30d81873967be6b7ee8f989",
    },
    "0xf07a32eb035b786898c00bb1c64d8c6f8e7a46d5": {
        "ticker": "WELL",
        "signature": "30450221008f214e16f490a8df9028bde6effcebb3dfb6660531d3a15fcd544c3b173af1170220224f343f110100516873390912b3cc04e73e9984c4d10d18588eaf8af32d5f23",
    },
    "0x63870a18b6e42b01ef1ad8a2302ef50b7132054f": {
        "ticker": "BLINK",
        "signature": "3045022100e5b6cb9f2c9c5bb17d8199d46b090c5b224e41b9414acf14e7d27b6f87e541090220562f7c89fe6aec5a6b78632ba4d6af3ef4b1d5c7c67cbe74a90f0d79328a4485",
    },
    "0xe6df05ce8c8301223373cf5b969afcb1498c5528": {
        "ticker": "KOGE",
        "signature": "3044022014630f3a3ef2ba7601e1ae129a115c3416090fb89be5d51497eb23e9c21b08ab02203be093d1958a316019a50696238276b61f2a4df7542b9c7b7dce85e293684a07",
    },
    "0xf8047d81b1f1d2f11603710ae32a723c1016f584": {
        "ticker": "BNBNET",
        "signature": "3045022100f08475f3eae0f1918fb837c084a6ccae3e3d1842ef68fde4473b70fbe08997d2022004bd1c4d665c00d36939edc5a262b9aee157911afa82476667b94f52fca34f43",
    },
    "0x5d0158a5c3ddf47d4ea4517d8db0d76aa2e87563": {
        "ticker": "BONDLY",
        "signature": "3045022100cc0c803c21cead73ef72bd679aa8a6c656670b4e8274cbe2e6aded34bca87e8002203d74137ad5eca3f2584ae7cbddb3337ffaa9ed771556f9e270713e582a4fd8a1",
    },
    "0xffeecbf8d7267757c2dc3d13d730e97e15bfdf7f": {
        "ticker": "BORING",
        "signature": "3045022100b4cbf7a49be3eb7b492853d97b8a27ed3d95764d45bf78381634c1f870413b190220336b7f2a4f587f1e3f00b407c2a32e6b7ec2113c0ef9ab97212744e49249149e",
    },
    "0x39ae9a782b1f9c60d0f1839ef4c2d0174c5edae4": {
        "ticker": "BRANK",
        "signature": "3045022100a502515bc160d95e291ca4248f2c63dce126fcef1208f42e6f1933fd6f4dc97a022049d4d69f578e63d6295439cf152d642bf66d8350d8e84c1876d91fde25c443ce",
    },
    "0x045c4324039da91c52c55df5d785385aab073dcf": {
        "ticker": "BCFX",
        "signature": "3044022014762ec68c892c94a81061e6ced27689e5da8670709d74aec7907ce08f552a88022017a6f80b8f484aad37009a8cc7c9f2753ab854a78227488a2c5d57dcbd152966",
    },
    "0x40e46de174dfb776bb89e04df1c47d8a66855eb3": {
        "ticker": "BSCDEFI",
        "signature": "3044022002092c06d4fc6b059f650e39e96d7aa0f8d2dd0cc8ffa07f764f6b0c5627a18e022063d058bf797a741b99578776b1fff39ab842bfd3f39f5932c65e46f6ec4e355c",
    },
    "0x5a3010d4d8d3b5fb49f8b6e57fb9e48063f16700": {
        "ticker": "BSCPAD",
        "signature": "304402205cf2757550c9c988f08d1fdc67bcfbd94f9482150d2c3f474695fb60fe9d739102204b60c28823252b90802493d74d9a474de39ebec935dd5b827e49d162845edfe7",
    },
    "0x5ac52ee5b2a633895292ff6d8a89bb9190451587": {
        "ticker": "BSCX",
        "signature": "3044022054c406b214a513de399dc7500c8dd4300e2c43f49f787ae8e3d95f6c9217944a0220226d1fdd7bcb540d5458597bcf7e33b70bf59af297bbd8fcdb8d6c189028470e",
    },
    "0xc9849e6fdb743d08faee3e34dd2d1bc69ea11a51": {
        "ticker": "BUNNY",
        "signature": "3045022100bc091edd6d362ba5221672f54b32fc1071b700ff62f4c1cf32a74a05e552ed440220697afa1eaf46e1e06ce22a4f73534bd0e420563ee3e225ddaf73552c743ec506",
    },
    "0xacb8f52dc63bb752a51186d1c55868adbffee9c1": {
        "ticker": "BP",
        "signature": "304402201c774b2c3ef5e0e3bab2defde18c744aed467cb28e5ece439b37678c7994e26c02204095cd48ddffdc6916634dbfa13d91dd0e39c82727ac347958c34e3f9794bd62",
    },
    "0xae9269f27437f0fcbc232d39ec814844a51d6b8f": {
        "ticker": "BURGER",
        "signature": "304402203dda247ee5532d68ff684ba828e0c0e38c39534baafa192a71d75b92dcfaf2d402205248d5ca6e1088ce927ef7b115758aeffc82f1224e2e0650e0dd43f953e63cdb",
    },
    "0x04132bf45511d03a58afd4f1d36a29d229ccc574": {
        "ticker": "BUSD",
        "signature": "3045022100c1df5030d681aa888b22a2de9c77b3b51790ec23b5a08f7434ec466d2b057cc702202158cd99f2b697861f0b624cbb1c80950c642297dfb0cbdf81cb0eefb0b7c999",
    },
    "0x211ffbe424b90e25a15531ca322adf1559779e45": {
        "ticker": "BUX",
        "signature": "304402204a4294c3549c44b178cbed7a77004d8e4c9e5fa395607b3b6a371bfc732123a9022052e96935592620f6dd0e3d470c286bedb8a353311c2e6e5ebb8d9ea18d52be78",
    },
    "0x007ea5c0ea75a8df45d288a4debdd5bb633f9e56": {
        "ticker": "CAN",
        "signature": "304402201465b74d27d5ca317f48ecab132ad19cb4d71a22f4a47a5f42e7818a766595bb0220723412c2ac92daec0bec560e41c9711ca917bc238cee98049783f88a672afcf4",
    },
    "0x176a25637e5078519230a4d80a7a47350940264a": {
        "ticker": "CAR",
        "signature": "3043021f30dc7f42d1ddcd8564c2ef2efd27218ae23f3bd1a45bea38b950a68d056c4f022002c22e7856df71f8cd9806f2f5f145e60440c3e307ab4e97a0a2bf88d590f6c6",
    },
    "0x8da443f84fea710266c8eb6bc34b71702d033ef2": {
        "ticker": "CTSI",
        "signature": "30440220303d3c1acdf562ad31ab9e49be30e614240ef12847fd657149ef5c265fece9c102201001b2d86245521f514998ca204c2e96c1e589994e8140ed3778ada7b9da8b50",
    },
    "0xe0f94ac5462997d2bc57287ac3a3ae4c31345d66": {
        "ticker": "CEEK",
        "signature": "3045022100bfe33683a7ee7fb41101f19da0d3ef9b008193f0c7529702b80286bb1dcb4f28022003312c1737010c0bd9ee50c0821ede3c9e8130db2eddd3035a6edb533c45edc3",
    },
    "0xa8c2b8eec3d368c0253ad3dae65a5f2bbb89c929": {
        "ticker": "CTK",
        "signature": "3045022100b73459cf8dffcf1912a2b7310578f9451d99d1b79bf2425cef9a0a24ff8a8d35022034587e93beb93ba86b2d6c3e7e37b246650927bd575986c38ede791eb597e21e",
    },
    "0x431e0cd023a32532bf3969cddfc002c00e98429d": {
        "ticker": "XCAD",
        "signature": "3045022100956895044cb0e43f54916aaeed4e5f4547e9193b977f07a2818cc7507417d8e8022002349342407702431688e3ba833cd1789a4df712e6a96fe61c39cd4107aa7583",
    },
    "0xadd8a06fd58761a5047426e160b2b88ad3b9d464": {
        "ticker": "CHS",
        "signature": "304402201f497b725dd23ed2c45bc25599191c48eb79269338a9c6042368f082a5c02d7202207ca6c3b48fc2f8a34d9eb1d977966082b555ff1b9442e3c28a7749f06a65d1c2",
    },
    "0x20de22029ab63cf9a7cf5feb2b737ca1ee4c82a6": {
        "ticker": "CHESS",
        "signature": "30440220757e7784e7c4c823cb694c94fcfda3c820468bef0e338b3640e53b0a752988dc02201c55c59c6b94fb89157929ccd3be816d33bb433a06d0f7ec461a9bb132241367",
    },
    "0x0000000000004946c0e9f43f4dee607b0ef1fa1c": {
        "ticker": "CHI",
        "signature": "304402207d6b1f7e65de18daa24d4b261483dab364b2c8a8bf0c93d68117521e33ce807a02203ef93cc34077b6847c982e39ba7fde49ea710964f0af9de181aa44c9a4663adf",
    },
    "0xf9cec8d50f6c8ad3fb6dccec577e05aa32b224fe": {
        "ticker": "CHR",
        "signature": "304402200096c5821aae62c22a5db8ec83f803b323fbfbb0d2449dcf5dec89b815980fb70220478381cbe5cb4dd42142189c099e1ce1e0bdd56e3a94d4d9920ce73ca1ff90db",
    },
    "0x3f670f65b9ce89b82e82121fd68c340ac22c08d6": {
        "ticker": "CTI",
        "signature": "3044022044d73f048d35b43bb28e3ec2eb0e0c140ff1ad1b7c8b2073039fea5fca617073022003afce36492d3fc6898d71130318499d154bb3c6c2ea8f5d454bc3e8d55cb37f",
    },
    "0x1162e2efce13f99ed259ffc24d99108aaa0ce935": {
        "ticker": "CLU",
        "signature": "3044022001c63b9180e3a17298c1f1151aa333702fe558094c10a1ff5e4d61abcb338e2f02202f0a43c6940ac859584a6bd8067a2879c324096b29aa142da433834d7a841513",
    },
    "0xaec945e04baf28b135fa7c640f624f8d90f1c3a6": {
        "ticker": "C98",
        "signature": "3045022100880b312c5ece768a40a08b1ff91a036634490f56aa529e6b3aca81127d81a62e0220593983f52ff03068187764f1c9e19d1585f4d77db8eab9a9317a14bb5b3a4144",
    },
    "0x8d3e3a57c5f140b5f9feb0d43d37a347ee01c851": {
        "ticker": "CMERGE",
        "signature": "3044022034f70d1a045a5c53c8efeb3e0b0461f7621ec6762474735ee020dc2927fdbbac02203669720216819e4768c07f5e2ecbbfdf4bd3d8ae16e962f64251683711bb35f9",
    },
    "0xc00e94cb662c3520282e6f5717214004a7f26888": {
        "ticker": "COMP",
        "signature": "3044022050bcef83c8c255e55735acffc6b3a63b031b177d0cdac1d63cd09807a116aa95022030aad1dd1f4c96c47b5c1fd2d9dea049b8efd2e00d05982d7f5fd0fbdbab1826",
    },
    "0x5ec3adbdae549dce842e24480eb2434769e22b2e": {
        "ticker": "CVP",
        "signature": "30440220567b41ad19aa3a533994db18f5bf646f59c1d54279deaef4e3748855f01c600a0220570a13d041b6e994e849f7772c149ac12b94ef2b32d8f569ec130f541905c18d",
    },
    "0x96dd399f9c3afda1f194182f71600f1b65946501": {
        "ticker": "COS",
        "signature": "3045022100d0187dd53cb74d75b8345a88afc87e93871cc9db40b312142d162e15764fdce802201ea8e6ac11c36855ce497f7c29a930f1eb02d97f1a6c3d854ca9643dd674ad9b",
    },
    "0x8789337a176e6e7223ff115f1cd85c993d42c25c": {
        "ticker": "COP",
        "signature": "304402200679d67bae219c85cb8f985e6b58717401208d9851d4d196e53be61a1861ff18022017c87f5a260f5da267d0cd97091cd201cd9b7479e1d99be5fdc0f7769a2e040c",
    },
    "0x82c19905b036bf4e329740989dcf6ae441ae26c1": {
        "ticker": "CP",
        "signature": "3045022100a24d81f95026c88a78d08ea4ef1ca65e792a6720414928b751da030c4da253190220102d175297cf14ebbb48e05c7a9587e8d013fcbd5324ca1739881dd11bc92ce9",
    },
    "0xd4cb328a82bdf5f03eb737f37fa6b370aef3e888": {
        "ticker": "CREAM",
        "signature": "304402202fe372550187051a0a37f1dae513e12baf269abccc07aabbff620bfb473ec76d02204cf05b571e735c1e2be464c1e220caf6ef528ade5e3cb934dcb62425e26402ac",
    },
    "0x81c15d3e956e55e77e1f3f257f0a65bd2725fc55": {
        "ticker": "CRADA",
        "signature": "3045022100d7852e7403ba94eb4a3378bc1c4087f7b562b0c8c1d8e6e7ba58b6fa6c184161022067ec2716945fa21c6c2f16abf5a02039c5d33dc681c3b045ac26b3fd3c3962b3",
    },
    "0x1ad8d89074afa789a027b9a31d0bd14e254711d0": {
        "ticker": "CRP",
        "signature": "30450221008c0491521a6e3b9fd3b56b278bbf593adf55e20b997c4d94586be286778a4a4e02201ba23a375108b02021aa531351a60a66302948e2fdbaca933a62237d1240d89f",
    },
    "0x5a726a26edb0df8fd55f03cc30af8a7cea81e78d": {
        "ticker": "CWT",
        "signature": "3045022100e5943920696fa8f674aa11bd2511c87b749e1f492832ce1eef8fd93282317c7902201d25fb98cd946d8fd3ebc3e7477d9fd205ba335d8a739a11743762e93e1f72d3",
    },
    "0x5c8c8d560048f34e5f7f8ad71f2f81a89dbd273e": {
        "ticker": "CART",
        "signature": "3045022100dc063f9170ac4269acaeab0f3821dae144b5d82506ea007ae9cf09b05d10180102201b648a1ad189a01a22a6c0c149533b2babca85bc050b5cb7b30d06f2e0dfd73e",
    },
    "0x154a9f9cbd3449ad22fdae23044319d6ef2a1fab": {
        "ticker": "SKILL",
        "signature": "30440220780bcd833f616720518087cab49634af6dc88c5bc2b49ed0dc0bed624338810d022072f7e1bd8f6a1afd19ef8898b493b5ae1546499621fb657d92bb0fa33201f799",
    },
    "0x50332bdca94673f33401776365b66cc4e81ac81d": {
        "ticker": "CCAR",
        "signature": "304502210092ccf2aba0d9a9a6085ba90f551aeea21f0905ee0e61df32f10f7e4bb49330f402203b00f9d360ccc107f2f56315480ad3ca1addd26d463ba7bf1ae8020854ceaa6e",
    },
    "0x34681c1035f97e1edcccec5f142e02ff81a3a230": {
        "ticker": "CBIX",
        "signature": "3045022100e7c5cd240c362ed368b757f2593b4425b3e0203b96935139d4995dabaaf2ac8f0220300640d1e6083b20fd46bdbefba8196e9e50d52ccd61a2eb1431bb99d2822f0e",
    },
    "0x9f0b91e4ee5aaf23f257782dedec19dc5cdbd11b": {
        "ticker": "CULTE",
        "signature": "30440220796bfc389f950df2865cf2ccbb9a07446e83c456e4cfab2adace06d5334a5b6d0220728bad55c4f66a002c06d212ab7099828a9e32debd3c72e009e6b0e3a81a02e4",
    },
    "0x708c671aa997da536869b50b6c67fa0c32ce80b2": {
        "ticker": "XCUR",
        "signature": "304402203febc554f777595fe89cdcf75a9db9bd1fa75c9028610b9e4497d6aeea8be40f02207e18aaf7e3d639d943195a30b084cc09b13f9c487a8b39c7a05a99ea0db28fc1",
    },
    "0xb3a6381070b1a15169dea646166ec0699fdaea79": {
        "ticker": "GOLD",
        "signature": "3045022100cacd1e8aff8fcf95ff47690da626f937bbc0fb3e7f70f989ba381eb59e33ab1902203a2fade0169fe85d4f8818ee5d0497141646e8cf5e046bd922a3c9ec257413e6",
    },
    "0x810ee35443639348adbbc467b33310d2ab43c168": {
        "ticker": "CYC",
        "signature": "304402204ec203a6755e5c5035e4284124b89af413652bd75585c2672015e8d5d34e6e3502206773a9a73b3a383f55801b45e3869381a28bb0f921c234529f9344f318abd164",
    },
    "0xa6c897caaca3db7fd6e2d2ce1a00744f40ab87bb": {
        "ticker": "DRACE",
        "signature": "3045022100da7926cf0619113f247e7084d8085b3cb402ecabb16f9fa48d3835697fce342102205ad23ca8cceaccd8da9c635c924b3a67fe7842ae427e84cc063e6e7f2c97a9c0",
    },
    "0x9fdc3ae5c814b79dca2556564047c5e7e5449c19": {
        "ticker": "$DG",
        "signature": "304402204e3db731aa44ebc9b7777cecb9c73d8653483aa2ea0ef8e7388cd304cadd54a00220523b1003692f98350f314b38a84730dd2eece96e35380a2a197c74a5b2413c2a",
    },
    "0x308fc5cdd559be5cb62b08a26a4699bbef4a888f": {
        "ticker": "DCIP",
        "signature": "3045022100b08381b77b31dad0f945b48dafc76d7c087fea4db12740f3f9c173dd65af871502200884251f657dd55a60a43eddd1e152197b75e4dbb5067e506300919a408548a4",
    },
    "0x9d8aac497a4b8fe697dd63101d793f0c6a6eebb6": {
        "ticker": "D100",
        "signature": "3045022100e9eb12fbc3a1fa2cc1475e3269b2bdbcef561a3742badc245e4713df5162e2d20220146ee90974f6e7f1f44aef789b5514cd98f8fe43bf947ab2233ce2658e5ccd49",
    },
    "0xd98560689c6e748dc37bc410b4d3096b1aa3d8c2": {
        "ticker": "DFY",
        "signature": "304402200c6f4b8f4318ee2f5c63dfc3dcb495df0acc16aa2215ae035b0fb4a35de736a702206b8714a50e47d8d038836213aad397b6000636449621e9f366d1477d57c19f46",
    },
    "0x9899a98b222fcb2f3dbee7df45d943093a4ff9ff": {
        "ticker": "DFD",
        "signature": "3044022022fb3deeae1611b5ae696d18b78683d50c0c526216d42de2dddf411eaf9c8ff402204f6a582c9c98e3f02d92dd101608f3285c0e19def2e70a4779b0a8268d783b07",
    },
    "0x426c72701833fddbdfc06c944737c6031645c708": {
        "ticker": "FINA",
        "signature": "3045022100f9820b848683c4d88453f9e3b4ef2d03f2c7f046c8432b92b811a5e930d8b9c10220691010859ef1738963af0a8e828b8c1a2166894fa4613562f3f711800370339a",
    },
    "0x72f28c09be1342447fa01ebc76ef508473d08c5c": {
        "ticker": "DGN",
        "signature": "304402201499c3628498193761ec02741ec7dc54467c040cd62f41f124f4f6b354cbc64a022054ca68b3930ddb1e2caab462dac83313c1fd00e168658735617afbb0bc664a9f",
    },
    "0x3fda9383a84c05ec8f7630fe10adf1fac13241cc": {
        "ticker": "DEGO",
        "signature": "3045022100a5391c12223d3bf6515d0c1fc5dd5957814bf228c96adfd6cdb22e7751e32c8902206625f28482ff51d2220bbc18d95a146c4473536b788021600f80f69a55981a9b",
    },
    "0x373e768f79c820aa441540d254dca6d045c6d25b": {
        "ticker": "DERC",
        "signature": "30450221008a24359007c2bf85485001786b8aea505add7892c227bf40b405c85f0263842b02203b4b8286e934d10222e5ce56c8e68a4cf860690877d352d655bafd18ca6466be",
    },
    "0xe60eaf5a997dfae83739e035b005a33afdcc6df5": {
        "ticker": "DERI",
        "signature": "3045022100f46153554aee2c0e8033a4d3517a1df1b657eb897d53acb0943112a40605d523022046afa2b695461549b71d7fce70ffc7a7b43d1f563cbb71b9a9bcb68493813fe5",
    },
    "0x039cb485212f996a9dbb85a9a75d898f94d38da6": {
        "ticker": "DEXE",
        "signature": "304402207a271e0f93a9f96b120b3f78e47ffe0e0df81e754c06852417652ae31788accd0220231669bf9bc97983351806f2057b18dff13db49195fba9ecee9e84b01f00119a",
    },
    "0x4a9a2b2b04549c3927dd2c9668a5ef3fca473623": {
        "ticker": "DF",
        "signature": "3045022100d301264e9eb6317d5a9c2cf71f9e3decb2d91953cb5bfed7db429522007f64d8022052a509904ff19a4d8c2ad7f1c4e10db2cf0d561c0a09f021c496c9e9f28ff95d",
    },
    "0x42712df5009c20fee340b245b510c0395896cf6e": {
        "ticker": "DFT",
        "signature": "304402206bd33cfce64fecbca5fc30c14463c050d2326468c50c1f66b1b51de26b5ef0340220498edb81b571d66e28c5957e51be39e9a880f74a484bfa1c0934b75c164dd7b3",
    },
    "0x8fb1a59ca2d57b51e5971a85277efe72c4492983": {
        "ticker": "DAF",
        "signature": "3045022100af27c4cfbe80670de9b342f026e96f8ba4e38a88651488cbac0392da8afa801d022013e939f50ade2ee0f4d5c13b9e7e10b114dac34c3a8a13e8006516ddb8b5e6f5",
    },
    "0x233d91a0713155003fc4dce0afa871b508b3b715": {
        "ticker": "DITTO",
        "signature": "3044022019256550198ed0560b5c2e37a07acc13e6a5a8d00a6f16e54758c8fcf2120f81022017d29ba87003e9a3b7ef5a4840d427fc4918f412798ba333bea3116916ea688b",
    },
    "0x67ee3cb086f8a16f34bee3ca72fad36f7db929e2": {
        "ticker": "DODO",
        "signature": "304402204657878d9768482532e679ada519997694096eea6e8b7090fa9adf4b66acc36602207bae567df4dd8882a1f9ae7978bcf4a393c5d977cba5e367b99e2a125e41e380",
    },
    "0x7ae5709c585ccfb3e61ff312ec632c21a5f03f70": {
        "ticker": "DOGEDASH",
        "signature": "304402203f465d025557ae19dab1e755c1f32c29a040e62a18b9a9b91cc4463257caa0cb022027345d82db485ff7800a9600007ee3d307dfc52f7bc440e51451557009162f91",
    },
    "0x74926b3d118a63f6958922d3dc05eb9c6e6e00c6": {
        "ticker": "DOGGY",
        "signature": "3044022036c0983feef9de590b6e12b78dbc74974654aac747fb6f406c6eb79dfae44946022078541a0643fb86da8bc83a88e6911fb8bf7815a0882f704d0cd1c9f7617b51c4",
    },
    "0x844fa82f1e54824655470970f7004dd90546bb28": {
        "ticker": "DOP",
        "signature": "3045022100d3cb3d9c2b50bed3545e75002979a0cc59cf3ae1577477e1c5d44edc7cc8f30402206688311f8ebaf88df6dbd0f784209cf784c1cf32eef490a7562dd633061988ec",
    },
    "0xdc0f0a5719c39764b011edd02811bd228296887c": {
        "ticker": "DOS",
        "signature": "3045022100fa48339cf113ebdb8f177c5056808086086ad7c8619b4d60a590a72be010963a022008ca9ec6796286fed096fd7a10a8a82de377e20e4267d3930717da9ba155236b",
    },
    "0xc8e8ecb2a5b5d1ecff007bf74d15a86434aa0c5c": {
        "ticker": "DRS",
        "signature": "3045022100e55da1d9a4f6f41f19bb6630b0c511d2d1a5b4b1d9675aa6034be7cbdc6dc6b402207f1164df57482136c5fc0eb6fc284903dd523849de2b1ac3d0794cc8c28f4148",
    },
    "0xc9132c76060f6b319764ea075973a650a1a53bc9": {
        "ticker": "DDIM",
        "signature": "3044022022a5444e4e5c0951c533d1a08eca10d878048197dece3460a82b9e2123dff848022051fe2068e13df2d845f8eff64e85cac683d5e706e91c6636ba2b54bf1ca652e5",
    },
    "0xb2bd0749dbe21f623d9baba856d3b0f0e1bfec9c": {
        "ticker": "DUSK",
        "signature": "3045022100dbea4dc673954f4b44ee55b6b12d3d88515cd532da19db0218ee7405bce4142202204b93390de28ba11504daebfd2996aaeed4b8b5e6922a153301176e92b9eb4a84",
    },
    "0x758fb037a375f17c7e195cc634d77da4f554255b": {
        "ticker": "DVI",
        "signature": "30440220627616a7cb5323e7981328247b1d9197d8ccf7d0453c8e2942f09ea686cee8e3022026e4d1e349dfe9fefc1ba454785f896b320fe7003ba8fc234190240f70201ace",
    },
    "0x6db3972c6a5535708e7a4f7ad52f24d178d9a93e": {
        "ticker": "DRIVENX",
        "signature": "30450221008d0cf0aaed5a445aab90a55073e608125ead6da5e7bdc192fd97b86d57ebfeea0220694eca71d5cd846beef66d83e5a9984ae805eda41a295bccbf0e6963352e3cd7",
    },
    "0x5512014efa6cd57764fa743756f7a6ce3358cc83": {
        "ticker": "EZ",
        "signature": "3044022068fc512fcccea22dbc546dd7449013edcb7519e04f7300f34fa96204f8984d7e022065a5ac55526d5a75e32f0b10eec2a426f5fa08026f506fd59fff0fb9950000bf",
    },
    "0xbbf33a3c83cf86d0965a66e108669d272dfe4214": {
        "ticker": "EIFI",
        "signature": "30440220751197b47f655d8aab7f789613bc46ea97c1949603adb2d41429abaeecbb09ba022005d5ea4c9348a6a3e6334e48c0d589aa93e52b2c1fa1634890d4da776d86bd5a",
    },
    "0xe3233fdb23f1c27ab37bd66a19a1f1762fcf5f3f": {
        "ticker": "ELMON",
        "signature": "3045022100c72bd21bf33d3334210fce5b7c8ae7a36dd3d2d16482c417451b50f04d21be96022068ead300c998a465c7945936002c6c39d4b39ca004a10137fd8a18ea4d5d7f07",
    },
    "0x380291a9a8593b39f123cf39cc1cc47463330b1f": {
        "ticker": "ELTB",
        "signature": "304402202cf48e5f972403924dbf9da53fd4f84c006b4246f194a2318cbf56ca4ce00615022014e7c6ba0efed452ef3bc6e7df2784c3b596af044be363158ce0481ceb1d88d1",
    },
    "0xa7f552078dcc247c2684336020c03648500c6d9f": {
        "ticker": "EPS",
        "signature": "3044022072900611f433e5f0cdb5160cc8a839fe41c10e39107f67b0c2a5538042bd99150220233255b823b9ecd5ad3b43efcede0059f6400760c079ebb495c40e59c783e3e0",
    },
    "0x293c3ee9abacb08bb8ced107987f00efd1539288": {
        "ticker": "EMPIRE",
        "signature": "3044022009b58e4ddbb7106dd14dba64adb4d8a54f499d958bce04099b32c04be4af48b002202d2cbb3419a9c644737d656ff1b0e9ed43e209227a55241c38212cebd0b9ed33",
    },
    "0x86a45b508a375ac8f0fd387e7532b70f71291152": {
        "ticker": "EMP",
        "signature": "3045022100fa699f51a9422059dac2af0618a0dddccddfe2c6cb1ba98b4f9a35be377d54ae022018ed4e8376e9b322ac6848a991ef0c06ae991901f395f01429ceef76359d0d88",
    },
    "0x8d047f4f57a190c96c8b9704b39a1379e999d82b": {
        "ticker": "ECC",
        "signature": "3045022100ea79455b72573029fc0cc58777982433877d6fdffa7350e2a3af70c8ea4fda7702200a0599a2968177cfa97c31b7a7cdfbc1698abd3e9250146a26aab2adcc9e705b",
    },
    "0xc3b4c4ef3fe02ad91cb57efda593ed07a9278cc0": {
        "ticker": "EVDC",
        "signature": "304402204a22624e7e2e28ebdb2b00ef1f956db18d6738dc1b5f77320e24d42cd4b6966002203a56bfeafe0c447576bc208b035eeeaffbd6e938ebddaf60ff27e6e70d67fa90",
    },
    "0x16dcc0ec78e91e868dca64be86aec62bf7c61037": {
        "ticker": "EVERETH",
        "signature": "3045022100edd12429759de94e1388e06a7913558752576ddd2518f5b709eb037b8d8d1fe302203034d4bdefa691caa477ab555a48cfb89e245e6f905bcf4d92ecdd386d2d4170",
    },
    "0xc001bbe2b87079294c63ece98bdd0a88d761434e": {
        "ticker": "EGC",
        "signature": "304402202c94f30a3524063628acdca6e4810adff6d2f796fd9497444623090adb39bbc3022029469635f0dbe1cc3a7bdafdd6a18056928fa7b6a3327c7fde3f4b2a76d749d5",
    },
    "0x5621b5a3f4a8008c4ccdd1b942b121c8b1944f1f": {
        "ticker": "XED",
        "signature": "30440220220c93452c230dec64ac0eb7f63f42c525c8ff165d408fd47dccafedeff26cea02206cf46af266d9c25499788cf591c0b828beeaef20bfc353da266472991abf6097",
    },
    "0x6306e883493824ccf606d90e25f68a28e47b98a3": {
        "ticker": "EXF",
        "signature": "3045022100d56cfa58513fda0db2167ea711b5337d8568ef2e116af3abe6d414255af66bd9022053345ff4bceb15af6daca9ba178b1c3759b59e38a8a1f66c54b1032a6271951b",
    },
    "0xad29abb318791d579433d831ed122afeaf29dcfe": {
        "ticker": "FTM",
        "signature": "30450221008a11bfa0c69cd5c5b5146bfbed88b13a2e8d15e611b68f86a70e4d1b22294d4702206ebcd89a1aec8d654aee95f03a6c62d5261c29070c7a657405eef0de41e95463",
    },
    "0xf4ed363144981d3a65f42e7d0dc54ff9eef559a1": {
        "ticker": "FARA",
        "signature": "3045022100e8fcfc5e16e8e9b510017577d22dca27e96e9f083ff16b4fc6a1a27cd62ba61802207eecf2fe1ea932173e3504e7a582847409f9e822f9bf9c1670a4d4782401eb86",
    },
    "0xacfc95585d80ab62f67a14c566c1b7a49fe91167": {
        "ticker": "FEG",
        "signature": "3045022100ace7eb2c5b5d950d6b9a5ffad303cc6f3250a9fe63692bda25100e8ed59a2460022068a3694b1470f8ff282032d4e7a60dd0601efe9f3ea9f9b5e281b7df3a3ce75a",
    },
    "0x82030cdbd9e4b7c5bb0b811a61da6360d69449cc": {
        "ticker": "FEVR",
        "signature": "3045022100b1773ee6bea9697c9149616b6ba875d9e14b33e8dfe4b7f8e71e69b0ffd5761f02206e65506238ac37c9bb4c9ba239c4f26e4e3ebdaca811906d1b011ba61439f3ce",
    },
    "0xdfd9e2a17596cad6295ecffda42d9b6f63f7b5d5": {
        "ticker": "FNX",
        "signature": "3045022100ab2bd495880e55db55102c505b5fff2f627dba88bbf6b171a3be67c5db7436df022056a394d737ebb774322db9a1fee380fc657c85eb69b1c498eb5a68a1d6a1c761",
    },
    "0xd5d0322b6bab6a762c79f8c81a0b674778e13aed": {
        "ticker": "FIRO",
        "signature": "3045022100f19b1cb8088a3e2aa438f571027651ef5673780d642e07a67e26fc10cffe8d6002200f4c291af2e27e5a697b3eccd43b6678f29d76013a7b2071ca34e3aa365efc28",
    },
    "0xa94b7a842aadb617a0b08fa744e033c6de2f7595": {
        "ticker": "FSXU",
        "signature": "304402206a0edd0b79bace11aad26defb29588342dbcb013106890be37b0e503cf6116ab02207817775325b94882d778067fe7402d40b42eea0469d47e0f6d574a2cde5226fd",
    },
    "0x2b3f34e9d4b127797ce6244ea341a83733ddd6e4": {
        "ticker": "FLOKI",
        "signature": "3044022066067a78a320ff91067b727fe4b40e09f836d55a9290d9a3cae6d057e099534a02202c6ab77bd3b5f5d44dfb52111abc4a74f17c5aa5899d7ecb66e5f2b7fe8c3463",
    },
    "0x1476e96fadb37668d7680921297e2ab98ec36c2f": {
        "ticker": "RLOKI",
        "signature": "304402202326927447b1426db2fc511bbf1c0bae8d67b69eb3bef2f31779b42d194f8ae1022072c839b999acd492cd014e34e59ce618836e5f1d37f1bbb4fbf89db05a5a21d9",
    },
    "0x25a528af62e56512a19ce8c3cab427807c28cc19": {
        "ticker": "FORM",
        "signature": "30450221008ef1d3e2daeb314ce068b5b77b8bfd7ba0ad1e6d1f3462412b354029453d64f7022008302b958ba1d391a31317db57a527f2381a2822aebb44b6acb0d697d01599bf",
    },
    "0x12e34cdf6a031a10fe241864c32fb03a4fdad739": {
        "ticker": "FREE",
        "signature": "30450221009e4c965c94ca9c99685c9c7e16887ed055c7db2412bf7b37da17e1cce8f8c9f40220397903a6cf8241e3cc1b16386f029930d86e239b5560ab8a3ff01b56d78863ed",
    },
    "0x928e55dab735aa8260af3cedada18b5f70c72f1b": {
        "ticker": "FRONT",
        "signature": "3045022100ec1c015b91493527aee541cbbd675568c89f0f904958b93ca5693bc1bfd7c7d702204026f3c04badf9d5ccc945d411d43336bb7b9e533ec05f36fdae1b98c0bf38e8",
    },
    "0x15b3d410fcd0d695e1bbe4f717f8e1b6d0fb2d0c": {
        "ticker": "KHP",
        "signature": "3044022028f4e11187edc1cadc1743dd8adbd9120233efbe6659c17abc0afc2ab58baf5602207994f34cd9838ab9d0c59b3f56bafa5bd7973e129b78bec70103deda9816ba73",
    },
    "0x393b312c01048b3ed2720bf1b090084c09e408a1": {
        "ticker": "FRIES",
        "signature": "3045022100b09318518adbc6e19f1bd1a2b25864b0a35b12d88f14c8ac7cfbc0bc1def3b120220227ee735e1d708ce7eba46ed79b5fe57a4cbc9134a380667263fb8453cfb017c",
    },
    "0x2090c8295769791ab7a3cf1cc6e0aa19f35e441a": {
        "ticker": "FUEL",
        "signature": "3044022015d989fbe30df797e62fbb0a2b38b2fce61735ff2d3ed4f72ce648b563dc1f0c0220361bc794d5e47ab350498df617b313becabaed2a65e6375574be667a67db4c23",
    },
    "0x5857c96dae9cf8511b08cb07f85753c472d36ea3": {
        "ticker": "FUSE",
        "signature": "3044022025cf22af71dd5d2ffb1754b939aed3083b6a47617ee8a5197c17f3e4408db87102200f865602febf0ca591e90decf05ceec473e9a6875701f463b8e4e87b97489b1a",
    },
    "0x3a50d6daacc82f17a2434184fe904fc45542a734": {
        "ticker": "FUSII",
        "signature": "304402204d1567cceb85b623d2018468c1c09d827cbe817b9f3262b1e96810ec4a60322f0220425bedfc9418a601bc454d995d6732672c97f8aabe22055cacaa187c55ef4984",
    },
    "0x89af13a10b32f1b2f8d1588f93027f69b6f4e27e": {
        "ticker": "GAFI",
        "signature": "3045022100e6a2295981f168533ab212cb89356a4e2347a0371e3db464cfc82bfaa59221cf022000a59a9637bd9ae12696492ca9567b628302902b9d5f60fb5c2d17acb1151db5",
    },
    "0xe4fa3c576c31696322e8d7165c5965d5a1f6a1a5": {
        "ticker": "GFX",
        "signature": "3045022100911be36f33bd2e33ca3fa1fb5bfef36d1ff5bca28a67e3aa2046b06d52fb80cb02200cb50a72a1a6130f17ae0aa9d68204668ad2e7696f99e7662ce850776293ba43",
    },
    "0x7f4959528d4c9c7379b3437c7eea40376fd96b8a": {
        "ticker": "GENA",
        "signature": "304402203b8a675c82137f0444b97982c15810fa4a0812a6997ba4afce9e33f3fc07f02b022017cc7f80899cc1ed57340788834580f130e0a60f74ebee42239950db4fd4bf12",
    },
    "0x94babbe728d9411612ee41b20241a6fa251b26ce": {
        "ticker": "GFCE",
        "signature": "304402202928c0c8a14c142d25f0ae8a81c4fd9db4b6e906fee852064922791bd64bbd2a02202115ac360e2f2d1d2cfb3c990528f727dd83ea6f01bc3bdbdc86605574821817",
    },
    "0x7ddc52c4de30e94be3a6a0a2b259b2850f421989": {
        "ticker": "GMT",
        "signature": "3044022068c3207fb4458ace11a20d5c88ea26073b1a54350f4dcbfdb6853cf32a2ebcbd022052a2574a6213d0dc4169a7ba09ebe50107bd1269b190979aef9b7872380bed62",
    },
    "0xfb11d7ffde8d643f5368c62fa9943bceabcb0c36": {
        "ticker": "GOOSE",
        "signature": "30440220214fc31a56c6e7eaba0bc75d8d08fc151bbb1455c12848736268bed695e41f93022057548eba800655f599ed8b46da4e8c418f358e99fbefc42d551a1032af4aa32d",
    },
    "0xf952fc3ca7325cc27d15885d37117676d25bfda6": {
        "ticker": "EGG",
        "signature": "304402203747d64d38c1ca3ac9ad1f66d5103df88d970783a830472c8c55a8819397df2b0220520983ab13ece942d0bfc1522535def817b1620d36aae09095870c5e2f4c2eb1",
    },
    "0xb7f2bca9b034f8cc143339dd12bb31d3d50cf27a": {
        "ticker": "GDT",
        "signature": "304402203272002f03632a17d4164f2ec1282b2ed7e3a7b70291bf63f4164bc26345788202201147705ad2f9c35ce6fc22c37c2481db6c4b2c6a5e1404e52cdc61a3215f1b90",
    },
    "0x6b8227a0e18bd35a10849601db194b24aab2c8ea": {
        "ticker": "GOFI",
        "signature": "304402204e122eef6a48fc629fc42ff5dde9923a0ccc173ec4d28e5fd6817b1732b32bc60220124631fec63573ad37d70013fcdeec89a470ccf769b6dd97a7ce91ef7b95f10b",
    },
    "0xc53708664b99df348dd27c3ac0759d2da9c40462": {
        "ticker": "GUM",
        "signature": "3044022014dc4711e4019de63916db801016936874cf64577b75d78c3f387b10dc30f8e2022037922196d4acf2b46c7006d65df9772d71321a7f051fe194a6d1f3613368604e",
    },
    "0x1e4ffa373d94c95717fb83ec026b2e0e2f443bb0": {
        "ticker": "WGC",
        "signature": "304402203fe03bc7be443ad935bc61d6df81f7fd6ee91b132c0971e626a94af94eb1c4810220299daeecbd86c6c8f0556b45ec1f2bf04192bd5e78a0714e00d64845bf2d0769",
    },
    "0xf750a26eb0acf95556e8529e72ed530f3b60f348": {
        "ticker": "GNT",
        "signature": "3045022100e743e02ad704b2a0adcd9cf9900e4e701fb5d59f445fdd4092ea22591c56c7d60220523dfea31acea18a31347ef9c3e24d231e458122215c98b03f0a6403a74721fc",
    },
    "0x10d3e0c38c286a04aee44d9b01cac916b56ee05a": {
        "ticker": "GUMMY",
        "signature": "3045022100a16c409e76086156db5b7e6683117298d1f6e55d1641a86e2e6f80610f71f03f022071ab0c4d2ee760b6766009e513a3b979443eaa8816ee6a6ed5444325a595d217",
    },
    "0xaa9e582e5751d703f85912903bacaddfed26484c": {
        "ticker": "HAI",
        "signature": "3045022100e16342a9b6680c726852b7bfaa615726f0d4ebe72c8926f2d2e09563226684d602207a4ce548fec7b9c94b62847a77818606993f89cedbc9da0c39fcd6d09217841b",
    },
    "0x1d1eb8e8293222e1a29d2c0e4ce6c0acfd89aaac": {
        "ticker": "HAKKA",
        "signature": "3045022100cfd02275c79d1eb56de118834231f49e296cef7506999bddfe4d8869673b36dd022075bb2ebbf46007345dcf876da018f9d8e1986518b25326b5ebe629c511f148e9",
    },
    "0xeda21b525ac789eab1a08ef2404dd8505ffb973d": {
        "ticker": "HPS",
        "signature": "30440220199608887bdcb2e36a7014c20f11cf834bdb0255b8e924c1ebce021897403cca02203d8b44b1f24750d091d288af0f090362ec521601d3ef63a99cadc275e436b236",
    },
    "0xf79037f6f6be66832de4e7516be52826bc3cbcc4": {
        "ticker": "HARD",
        "signature": "3045022100968ededfe57ab7a6480399c45ab6e3191257469f8dbf6607e44d4b9636cb5e5f022045f4bb6d38993ef053935d55e69139060e10a3d8922e49c4d0d9d57a3e680c63",
    },
    "0x03ff0ff224f904be3118461335064bb48df47938": {
        "ticker": "ONE",
        "signature": "304402205d97bcc2c5073985441edc97672aaf4474a24c5b05f24bba88e62d9bd382bec302201d68078f876210e8078371d9b1c0a21ad343f5e03e38ad1fa22d2a0d48462fe1",
    },
    "0xe7784072fc769d8b7f8c0a3fa008722eef5dddd5": {
        "ticker": "HEDGE",
        "signature": "3044022030c886cf15e3b60a6279614df4073d175b73e9a3ba4113a794cbee02dac44cb202207bec94539a0409aaf6e661aae45fc7503f52222135707f434cf97b83b029cedc",
    },
    "0xc7d8d35eba58a0935ff2d5a33df105dd9f071731": {
        "ticker": "HGET",
        "signature": "30440220665426b3c7d0d1a58a5df9e40840b65476e1f0d04b433a55dd41672eb711a75e02207f3a0ae1dcaeca74ef13c4ceef5b0dc7a8d0a994f3b55cd50677de4ace6a70ff",
    },
    "0x948d2a81086a075b3130bac19e4c6dee1d2e3fe8": {
        "ticker": "HELMET",
        "signature": "3045022100d23a294060d70aac65256c251981e402ecf106e6f944978c5860fecccdfe66f9022072781271597eccdcc18434514a8ff7cd3c1a88d1b8c8e565190f13188aa111da",
    },
    "0x186866858aef38c05829166a7711b37563e15994": {
        "ticker": "HFT",
        "signature": "3045022100856a0b27b4fd222358ea6b4a1f079e426f2ecfbd2e435dd25f9e76ebca0a2cde02205f38d8931f44f165db53f6278f7b47677143a3312c3e38a61e8678b7823500c4",
    },
    "0xfa363022816abf82f18a9c2809dcd2bb393f6ac5": {
        "ticker": "HONEY",
        "signature": "3045022100d091979961683e03a1d0e8758d35c13819016e3f90cee01b256b48a586b2e84902200747e0ff02c920f6d0ebcbe0d685c628a97813a5c6d95f5f80d393d6190ea168",
    },
    "0xe1d1f66215998786110ba0102ef558b22224c016": {
        "ticker": "HOO",
        "signature": "3045022100f7e98e11baec5bd6cdcf6c08e28cbacfdcda21fb64995b8451047d757331145302200d4f96c4e645e3c85fe95a6e8646167bbafb9b82dc43a1b09180d23ab52cc955",
    },
    "0xc0eff7749b125444953ef89682201fb8c6a917cd": {
        "ticker": "HZN",
        "signature": "3045022100eb1b810d69476400902e5410303f907d4e50c2098be0cd5b03ab579982693dfd022029dd9211a1727aca613bfe1251c6cf804bc478af6ebd3241572089dd2f3b402b",
    },
    "0x4fa7163e153419e0e1064e418dd7a99314ed27b6": {
        "ticker": "HOTCROSS",
        "signature": "3045022100cd7d821bc37e678bfea137ae9752322f0d2be691a5cf0cca17914f69290848780220074f2cc020cae1729e808b7194485fbcf8eef15132507ecb3e1f3d8cfe9f4b1f",
    },
    "0x4e840aadd28da189b9906674b4afcb77c128d9ea": {
        "ticker": "HTB",
        "signature": "304402206da737df0072829225c0c715d936df010a85603918135afd1f345645b25bc57002207124820b225027a0ef951e78c2b7b582bd083c777ec8e3062832b266f423995b",
    },
    "0x565b72163f17849832a692a3c5928cc502f46d69": {
        "ticker": "HUNNY",
        "signature": "3045022100d103c897ee82ee0afb71a2628b7117e50e6f330c32f13b0adf3b63b0d775813f02207e952652c11de8d4f5a4c96f7b0b52623ddd953dacb5a3252ebdcdd366cd6521",
    },
    "0x9a319b959e33369c5eaa494a770117ee3e585318": {
        "ticker": "HYFI",
        "signature": "3045022100d3fa72874498f397ab28741cdb8b76b15307fc8157008a691c00367835b40fe202207c278c1296a66f2f386e714a74d2f56e2f687f3b2f5ece52c64b2945c4f05423",
    },
    "0xb0e1fc65c1a741b4662b813eb787d369b8614af1": {
        "ticker": "IF",
        "signature": "3045022100e659e5b8fefed59e23e27ece1c13e538ce304a7529ad0fbd725b52f4f680e471022002049f65ae76bed081f57969a6b687ea62f554ce0d91c5f571cfebd2f8d416a1",
    },
    "0xa2b726b1145a4773f68593cf171187d8ebe4d495": {
        "ticker": "INJ",
        "signature": "30450221008539e722919ca42b78d7a6f309a6e217cd1574cf2ad0a7fce4c6b877294369630220443bc94530e6dac4ea931892594a67c489b7d4698bb0a9e11eb98771f18c89fa",
    },
    "0xdf1f0026374d4bcc490be5e316963cf6df2fff19": {
        "ticker": "INNBC",
        "signature": "3045022100b4975fb7b3a1ce319c88c31231e1962c8503aa5bbbb14519f2d7133a2fc7872802207881f4075ae6f85dc1293cb7b1a053954d5112f1a85032894c8893d10c0848d5",
    },
    "0x04c747b40be4d535fc83d09939fb0f626f32800b": {
        "ticker": "ITAM",
        "signature": "3045022100fce241e1f8a5cbb6d56a642d07114356fd332e10d06583da4a0c50b84961693502204a729a1ec3f4ed4c091a405d27eeb2e2ac06f84f7291cb7ee905e1394af28b9c",
    },
    "0x058a7af19bdb63411d0a84e79e3312610d7fa90c": {
        "ticker": "JED",
        "signature": "304402203c1f534efba2e2f5176a0748cadc662ebfd2ee64e1ed307a5dfd2bea639bd92602202d4e71f0a3ca8ef5b521a3f5a2bc20c3f62fac019a4171a7665648a39c43b6a8",
    },
    "0x5f2caa99fc378248ac02cbbaac27e3fa155ed2c4": {
        "ticker": "JNTR",
        "signature": "304402204fef9aa7e23abf7bbab100489f1c8fbee2b19391da9529899596dd575f2c524a02202f6a7353ee5f319df20ef5e24e3b9182972ffcb3b839a5abf27f5b00adf37705",
    },
    "0xc13b7a43223bb9bf4b69bd68ab20ca1b79d81c75": {
        "ticker": "JGN",
        "signature": "3045022100d45041e34d3fff3272d2437f464a4d3f3a3f24e5dbfb52bb7093f6d9460c0a9f0220033e2d9ae1e74d5b25e3bbf69b94cc69e50ff62c896317dafc2d050c05e805d2",
    },
    "0x5a41f637c3f7553dba6ddc2d3ca92641096577ea": {
        "ticker": "JULD",
        "signature": "3044022030e408f4faf8d3509b952233a8c60123ba3ee89fb2e188967f14f050dd34e12a02205573e724ce0d83d658b555818e068e74e694897a1ab0296e1481aa8b73ca6980",
    },
    "0x02a40c048ee2607b5f5606e445cfc3633fb20b58": {
        "ticker": "KABY",
        "signature": "30440220575f919525bf000cd40121287f5148fd356a3f544ff77b5a3d6c08880d4b8a58022046107ba72b2d6c5e9b39e04df5519fdce59ad7f7ee6e13e7fe4e9a81019eb5bc",
    },
    "0x4ba0057f784858a48fe351445c672ff2a3d43515": {
        "ticker": "KALM",
        "signature": "3045022100a9aa9dd384eb3beae7ee0c6c08d5be88750cc24871893f4d6ccf55b1848f65660220556aa45eecda3018ff84bde697d0cac3504a96182b35f68c3e537cd5c23661c5",
    },
    "0xdae6c2a48bfaa66b43815c5548b10800919c993e": {
        "ticker": "KTN",
        "signature": "3045022100b58eabefa4bf883835b4d6761c45ce6f54634385055e94ed8f260c261eb1c9f90220686121c93a37e058197a8144d0c4947edc15feb6c709e7f0e75f7450c9375c1d",
    },
    "0x5f88ab06e8dfe89df127b2430bba4af600866035": {
        "ticker": "KAVA",
        "signature": "304502210080e5f75e398aa4e40f460414fd87813862c9e8d9e6a278e7dd3c2ad69efcbbdf02204f78fecb6a9473e227981d83d7469ff7ea17681f8d411a72aa874e23e751a052",
    },
    "0x5ea29eee799aa7cc379fde5cf370bc24f2ea7c81": {
        "ticker": "KP3RB",
        "signature": "3045022100f37334bc75c34bfe9dd9f7c172e26774fc2d1d25b6a52adcf19d9092741cd29d022042baee2ddb47e221ed181dc8078ff3d8cc8ee9f6fc5506ca5f560e9e86e2c703",
    },
    "0xe3ba88c38d2789fe58465020cc0fb60b70c10d32": {
        "ticker": "KIND",
        "signature": "3044022029cf9fd5615d252590bda6f9c83f6ed94b3af114ee0a6cbacf4bdb722743c91a02207f17402ce46ab2aeb9bbe6a07a23ac1471a171fd9277aee887bab24b1525df5f",
    },
    "0xcfdf8a80fecaeecc144fa74c0df8691bfd0e26e3": {
        "ticker": "KIWI",
        "signature": "30450221008e5598df7ca914fa11c5e636ca139cdfb2e6a495734740ac2cccf5cef9a13fe5022072183ffcc362a860a8ae0a4368c4147c557980502224d5ad2286d4027ee1ca60",
    },
    "0xc732b6586a93b6b7cf5fed3470808bc74998224d": {
        "ticker": "KMON",
        "signature": "304402201355c43fe23820c6e1a4ac505b66387927881141016a55d8a1006cf48f8a4aa5022023736ab2c84f3a2f47719ffe2785a4c3593fe31e3caa53fe457087deda21dd6f",
    },
    "0x5d684adaf3fcfe9cfb5cede3abf02f0cdd1012e3": {
        "ticker": "LIEN",
        "signature": "30450221009c5e398d5c6a14c5d46f10b73d5e25b1a98ff491139df900288611c3f25390b1022030fa27e49dd23add95904e12b0afe1f10bd9e2b02b14e49874a555774064b24c",
    },
    "0x037838b556d9c9d654148a284682c55bb5f56ef4": {
        "ticker": "LIGHT",
        "signature": "3045022100b2754061eb5b6c30e9c0a6b1bff5d9eabd1ccca5ba219bde83d87950c3c7279c022019903cd9e5226ec97529634726509e35624493a48a8fc7fb08a526a41c736eac",
    },
    "0x762539b45a1dcce3d36d080f74d1aed37844b878": {
        "ticker": "LINA",
        "signature": "3045022100eafa75e11d7e601fb5ce4e240a8287326e03b1ae2f3cc23351543ab976d52c7002202ef874c7243383e353fbacf6ad4e3133b126950953db6aaba836d9b999b5a919",
    },
    "0xb59490ab09a0f526cc7305822ac65f2ab12f9723": {
        "ticker": "LIT",
        "signature": "3045022100c2e714548bd1cbbde9e7e8dce7b64a3bd2f3e3f35febe3c3e3f90cc02e578a960220589a273ce81856bf2fe7aea227ff980947324657c4fb30b32367bf0d9ac9897c",
    },
    "0x17d8c396a55d6c1c2130efc23f4936b56860df9c": {
        "ticker": "LMOOSE",
        "signature": "3045022100b1b4b85936fa66d30c3046459ccaa3600e076b0a913c1e581ec9468ab0d9d14902200537df39333e55df23d1054f680c12a0d5457b680c2eada9365f13f30e5d3831",
    },
    "0x9e24415d1e549ebc626a13a482bb117a2b43e9cf": {
        "ticker": "LOVELY",
        "signature": "3045022100d8fe9abcea3ad5416276ad2e406c39a6911b2408985c60f996addb5a1130e9580220114bfb8e72df6e813f064e451e3bd2e75327be9bd27e640fca6f3ca92bb255a2",
    },
    "0x857b222fc79e1cbbf8ca5f78cb133d1b7cf34bbd": {
        "ticker": "LTO",
        "signature": "3045022100f09f346b4ef00fe0be0019a1ddd21bd1ff5a545235d392e64fbf715bbb22318602203af9a65412f8f0c44072afe00ee2121e351b85b7cf9a9ec26b44cc1b9e9a59f6",
    },
    "0x23e8a70534308a4aaf76fb8c32ec13d17a3bd89e": {
        "ticker": "LUSD",
        "signature": "3044022050532f1c989e16da5474373dca7264cc3d84b6add6175580a0bb184931b59fe302203e46f653c7c4f9990f2dad6a2552b2c72dc8fc8855c4c06aa1d96c5c4e67bfde",
    },
    "0x9617857e191354dbea0b714d78bc59e57c411087": {
        "ticker": "LMT",
        "signature": "3044022052d3cea4b3dbc51e55eb01ffc95023afb37dab78774700d85fdd482f09929c160220612ca478cb2f986f1417775cec31d0acb4eee326d749ed1fb1f255e42d2bec5e",
    },
    "0xacb2d47827c9813ae26de80965845d80935afd0b": {
        "ticker": "MCRN",
        "signature": "304402207aae112f32848d805b16cc044fca0b12588ae6e7fe8f47469bbc9c2accfe44eb02202e5e7efc35bef94473ad70e6553d76a49d6a32fc79d0e297be8e021a612ef629",
    },
    "0x4b8285ab433d8f69cb48d5ad62b415ed1a221e4f": {
        "ticker": "MCRT",
        "signature": "3045022100b9bf596ae325d3d349b72523aec03b22fb757e5b0f38477782dc477eb17057ea02201f4582ed2b8feafb1294355894ba3bd1c237cd5aa1a9912f434b0f1ffa4eb7d7",
    },
    "0x231cf6f78620e42fe00d0c5c3088b427f355d01c": {
        "ticker": "MOVE",
        "signature": "30440220168ea32ec256ed34e960f65984c3267f0eb161b44642afe71e42a0c18dcc02ab022028ea8d8919da414816877ecd1bcd239f0cc6e195175ce3f52ac681e64c9a7468",
    },
    "0x2ed9a5c8c13b93955103b9a7c167b67ef4d568a3": {
        "ticker": "MASK",
        "signature": "3045022100f233d78076eefd948bd4e074b8a998bd469593293e61981211af60de9930419e0220782eb26e09be0ca775014fc2added450e218f35441864e0fbf2dd9e44b0df1a7",
    },
    "0xf218184af829cf2b0019f8e6f0b2423498a36983": {
        "ticker": "MATH",
        "signature": "304402203a94510559473632e69bdb75fc574f0b895eceff7a673f6cb333eebb148c52850220417ba14ee52515956af022b3200f6f94c70f150ed1b54b8ca0674ec35de9fb55",
    },
    "0xcc42724c6683b7e57334c4e856f4c9965ed682bd": {
        "ticker": "MATIC",
        "signature": "304402205df236f3ad785468995572b13cf4afbecd99f3f6bed9825bae846c9977ae521102204ef3cb49ab531c095b2d6bd97d5353cf911f98b3238405c5c880ce46b4917d94",
    },
    "0x5fe80d2cd054645b9419657d3d10d26391780a7b": {
        "ticker": "MCB",
        "signature": "3045022100a59f92d39ceae9b25d10e70ce9145b77cf6a3e8baef4f03c3f412b38e3bbadad02204816ad9d299c98de18c1b3333c9e9995286efcd1ff827bf63cbe540356964352",
    },
    "0x668db7aa38eac6b40c9d13dbe61361dc4c4611d1": {
        "ticker": "MDT",
        "signature": "30440220310cc29e1adffb4c28f6c59052550b5d89a82d3bb7dc330ee5617a07ab84ae600220443ff06bd664e215fc8056fe2dbc73367bcad5a54152e6141499406115466800",
    },
    "0xd40bedb44c081d2935eeba6ef5a3c8a31a1bbe13": {
        "ticker": "HERO",
        "signature": "3045022100feb3f951420d71e47a27fb3337ef523b57af839f277b2cd569adbfd79a0ea99302202974b2c09cc07d79058189b8599da6ace3b636da31e005e82ce1edabe2c7a5c1",
    },
    "0x8e9f5173e16ff93f81579d73a7f9723324d6b6af": {
        "ticker": "MILK",
        "signature": "304402200bd1cf26f9bd0ec88a3fbb09eabffd402421d86f0ded8050d8d9c776097756f6022006c67120418a9fdf8081e36d9cd6a775b6724b9dc82b644288c384eebe57833b",
    },
    "0x68e374f856bf25468d365e539b700b648bf94b67": {
        "ticker": "MIST",
        "signature": "3045022100dd157ac534fcba2cf67a83453321c030bc6e3e3d9e19cb33156bb52aaa6e784d02201debdffe4be944a60e5393e48c284b39a0fac953b1938c8acb784ee38733754d",
    },
    "0xb67754f5b4c704a24d2db68e661b2875a4ddd197": {
        "ticker": "MIX",
        "signature": "3045022100f2886cc5a2a9c5a170cd526dc492e56a8ff2461a072dea97a5693f1497f624110220027016d2c76d0636b65b13fa0115e25a05e704c6bb4a3880cbab3d96d650e539",
    },
    "0x3203c9e46ca618c8c1ce5dc67e7e9d75f5da2377": {
        "ticker": "MBOX",
        "signature": "3045022100e096b677293334047919a62bb84b3330d5098b31288c6e229e399d682845808202207c7f42288e6e3cee8568717d1a16f71799a1d59f0e3863b443c23e4b45bf5956",
    },
    "0xd72aa9e1cddc2f6d6e0444580002170fba1f8eed": {
        "ticker": "MDA",
        "signature": "30450221008866298f91a5da47ed6df5191b9131b5e8f06562c9d908665eee6719097790bf02202bfca2ce83a5d9470f9f36348cc3de6312fd4b196693b626ed9d28c299984da5",
    },
    "0xefb94d158206dfa5cb8c30950001713106440928": {
        "ticker": "SEEDS",
        "signature": "304402203acc6bfa67b8e9e4c2a6880936808aabae0915faf4970b5131b8955387d34f7102203a9a24b4711a9952bc5ea5100123a37a1c34f41e040b93714f9cf751304bba9b",
    },
    "0x9573c88ae3e37508f87649f87c4dd5373c9f31e0": {
        "ticker": "MONI",
        "signature": "3044022047c024bb3848483d013fdd65f97e1af80b51c6c0cd59be827f14f778f0f908e4022065cca6e98a5548d91e14453295ba022f86d1915f907a9cb3738605ab18a2e1eb",
    },
    "0xa29b6f4e762874846c081e20ed1142ff83faafef": {
        "ticker": "MOO",
        "signature": "304402200e378f3c78613309e3fd813c815f667dd350f4da8e15fbaa5d8ce9a273f39ade0220157ff84c8a507ccba1109ab973f8f87c2e3ad560cf7f195236491848c32d9d4c",
    },
    "0x3fcca8648651e5b974dd6d3e50f61567779772a8": {
        "ticker": "POTS",
        "signature": "3045022100fce5fc69e5be24400ac6f52411cf41cd7d67c15fb69bf32e77b8eacc51bf4c3d0220414aaee8795223eb30c5a9289e696f8706fb577ff5451fd111af757c5f262920",
    },
    "0xaf1167b1f90e4f27d9f520a4cd3a1e452e011cea": {
        "ticker": "MRFI",
        "signature": "3045022100bad2ed8f0804b908d3f311f7e6a6a8dd16e2c363c3ceea4ecd743fde1a5a5f7002202ec75a4b94ed2f1df5dc29d3befa0dcbfe530b809ee67135c0b0338cd17208b8",
    },
    "0x4c97c901b5147f8c1c7ce3c5cf3eb83b44f244fe": {
        "ticker": "MND",
        "signature": "30440220516600b0627ebf813d1caa5686896b61edf4743960bc376689b701279f341c4702204b570aebdb62407375bdebf4393d50ce47feacfab7ccb24eaca6adadf74767e1",
    },
    "0x5921dee8556c4593eefcfad3ca5e2f618606483b": {
        "ticker": "ANYMTLX",
        "signature": "3045022100c9bde0f4994d1326df4d7fa075ace351afd469c541640b2bc82f4967949ab4c302207c8072aade834fa277a91b88cf92f290b4879584d099691e48947ee094a2cbf6",
    },
    "0x4131b87f74415190425ccd873048c708f8005823": {
        "ticker": "BMXX",
        "signature": "3044022022a50bd48f41776a197e4cc54073a232ba51b98d1020094de4ff660a9ecda528022024221f47a90ff4f9f3c6ac8a01e8d16cd26dd395ede01d001c21d22d9270ed08",
    },
    "0x9f882567a62a5560d147d64871776eea72df41d3": {
        "ticker": "MX",
        "signature": "3045022100f6de4e23a250b96c58a147c85fcd4ecbae78b42270540ad90383a00b732673c902203bf4d977d3c5ecc185a330c60792c8c14889d52ae86d86a8a282b3afbca99a6d",
    },
    "0xfb62ae373aca027177d1c18ee0862817f9080d08": {
        "ticker": "DPET",
        "signature": "304402204d897aa22c96257c97837aeb9a1d56158ff2d8f5f60f993be995435fb27a788d02201bf14dad4e624763c2e8ab4fe593c0f10da25daf58e20d15e2ba53f5f5a1b684",
    },
    "0xd7730681b1dc8f6f969166b29d8a5ea8568616a3": {
        "ticker": "NAFT",
        "signature": "304402207d47d45e781fc56d4e85f56abd0d881aa0c7cc10cd2803f6af5c7ff9b021d7c102200308429fe4cdd4b820827e8eda0dfd02d828e6b6e0e3a722fa119dc707445d6b",
    },
    "0x758d08864fb6cce3062667225ca10b8f00496cc2": {
        "ticker": "NAOS",
        "signature": "3044022068c4403fdb9c76020781e61603fec042f11eb1e2c991f1801e094914ca5d22750220053dea8c3a0628e6614e42a0514feedb69b780da2a4da2c11b6112e6183c76e5",
    },
    "0xa1303e6199b319a891b79685f0537d289af1fc83": {
        "ticker": "NAR",
        "signature": "304402203dc0331f5aaac244fc5e7cdf78ffd8867d07ceb1cfb3f38dfe4c98a9e4e54f4802204b4321e0c6f0518bbfc139a41b22b7fb3c0c6e70ef40d414bbfbae4a85669e73",
    },
    "0x42f6f551ae042cbe50c739158b4f0cac0edb9096": {
        "ticker": "NRV",
        "signature": "3044022011c0ccb048c422b0ef6c7bc1255b3e3c6da7e49d754a8c238bb63925ece0ffc902203f3242e505ad072cc830c3b9dc0df2df300c1feb9173b16bc786f75e019065fb",
    },
    "0xf0e406c49c63abf358030a299c0e00118c4c6ba5": {
        "ticker": "NVT",
        "signature": "3045022100dc8152ba0e65397a1afb6227a0051693d974cc14d04024b89e834f80e344882602206620f3d7fbc1d327ab9a6ab0a1fa8f791e7cdc90c7a98aeabfa4584c66973d71",
    },
    "0x968f6f898a6df937fc1859b323ac2f14643e3fed": {
        "ticker": "NWC",
        "signature": "3045022100ab33c10c6ea591b2dd327cfefa23e30d4c9931349372fb8301a4c2b6ccdc1dac0220600bd20faacfed550838ec1f72881d908a3b610a15f4b7c1405eb5363d85b1d6",
    },
    "0xf7844cb890f4c339c497aeab599abdc3c874b67a": {
        "ticker": "NFTART",
        "signature": "304402207ce1fde7f047db674dcb5a879c1e00ebed94bbc9ff573faea0583063f07bfe9b02207e8e48a8e53cbf5a5feaeadc5d4f2bd85d5118ad504bd6d13b53305d1816e595",
    },
    "0xde3dbbe30cfa9f437b293294d1fd64b26045c71a": {
        "ticker": "NFTB",
        "signature": "30450221009687152f9a108775f295b3a711a393d9da204234d353837146a49d6cd7b9aa900220591cfe6716db9a9719a5f9eb22e922813d65c990e179bde690dfa3e3ef380bcd",
    },
    "0x3279510e89600ee1767a037dbd0ad49c974063ae": {
        "ticker": "NFTBS",
        "signature": "3044022074e9fff8e66c7cc5ef8338fac9d7d57c7ef7547bcfe8ab5b6bdf45f13acad9500220053bd6f171835f3a6f166fe480215a054bdfacc88b9ccb1d681e89cc5cafaba9",
    },
    "0xa67a13c9283da5aabb199da54a9cb4cd8b9b16ba": {
        "ticker": "NBL",
        "signature": "304402206b5a172a984a4032af3fefd81e186e69bc8b0b8d99ab6ea7e9b179b89539876702203984a205f69a857e7d07688bd4f1052b637f4c1484f19a197f6f71dbb06b7a19",
    },
    "0xd32d01a43c869edcd1117c640fbdcfcfd97d9d65": {
        "ticker": "NMX",
        "signature": "3045022100a2c7a2fd6c47f25a7bf8207231813933d6a682ca15ff84cbaab8c09ebf4c8c57022051c1afdcc60d0a0e9408036c67b524a122eec1ff3b01188c3ca3d456bf7c8461",
    },
    "0x8cd6e29d3686d24d3c2018cee54621ea0f89313b": {
        "ticker": "NULS",
        "signature": "30440220596ab7a783f83e723e22a3eee89e4680614df12710adfdbfc60af9594e4cee91022000aea09849f1f44e227a5d954b1a353d19605b80aa3381ffccf8a7597a75ce8c",
    },
    "0xbfa0841f7a90c4ce6643f651756ee340991f99d5": {
        "ticker": "NYA",
        "signature": "304402202da712e87e323fab053a7a92c5cb14933895521c7a692ae7236e5ef18c9dc76502207102aac41f97b95adc73451807bf22b968955c787b79163e0d7e49493f0e6025",
    },
    "0xee9801669c6138e84bd50deb500827b776777d28": {
        "ticker": "O3",
        "signature": "3044022031fe86d28b28ddb8ddb23f0446e111a709d6101cdcadb136434709f1ff1d090e02201a5b1257f80d7f0495bbf2c8d1663c02bde7317fe1adfa253bfc22179a6c4fd2",
    },
    "0x49277cc5be56b519901e561096bfd416277b4f6d": {
        "ticker": "OCT",
        "signature": "3044022041e69e36de1fffd57de22b4e06ee9b32a89aaf431b78a4b3301883c505248dc9022059ad61d96bc0c51379bcf3a0356f8f0528f06dbd4324d364c86b73366130510b",
    },
    "0xcd40f2670cf58720b694968698a5514e924f742d": {
        "ticker": "ODDZ",
        "signature": "3045022100f76a4a548a149d5e5ebeb71a06e3f1710a8c3ebb92173efbca4dd5f72a5790ec02206d58cfcc1973f5887a7b02906e14b0b5f2e13dff568388f38854946e618db44c",
    },
    "0x658e64ffcf40d240a43d52ca9342140316ae44fa": {
        "ticker": "OIN",
        "signature": "30450221008e8a424f46562cb1258ce401586b7312c402420c8ad00be108794f77e1a9a6f002203d8cde0c3dfdadcbd2b6cd78fb88b5bb77c04d213eff9c1ca491f75075fb2cc9",
    },
    "0xabae871b7e3b67aeec6b46ae9fe1a91660aadac5": {
        "ticker": "POPEN",
        "signature": "30440220351dfa8595187f89113403a7f8b0190a2a7b1c894ebe25920991204bb5378c84022046916774c319522582881aac885fe6f5dedb70e6f8bcf73c8840ef184c07056c",
    },
    "0xebd49b26169e1b52c04cfd19fcf289405df55f80": {
        "ticker": "ORBS",
        "signature": "304402206e9bc02471042b25f803b9bd5668eed166cb8bcf72fdb4eb8a1125733138df560220765be43b826dd72c9c1bc2f3689f47ab81a2b0f9f039531f7c32b8519fb4dc97",
    },
    "0x93d5a19a993d195cfc75acdd736a994428290a59": {
        "ticker": "ORE",
        "signature": "3045022100aaf8ae2b05bb6810516f1b770d92c6f6ff254d7bd38893230153e5dada0d491002203b875a40e76f76ca32d1e1426fdb173106dccf98d59d937b25949fcda28f148f",
    },
    "0x7e35d0e9180bf3a1fc47b0d110be7a21a10b41fe": {
        "ticker": "OVR",
        "signature": "3045022100ba0edaab9e0415ba5d7afe07d4e2d395473f928d22d1a4aedf9c78c66b4620e60220380ddbd75920333b3395ec3feec52aa61b88fddabfbe1c0e87aca1eca0362311",
    },
    "0xad86d0e9764ba90ddd68747d64bffbd79879a238": {
        "ticker": "PAID",
        "signature": "3045022100aba674f9a01f1ca697e240f5d68bc3bcc91445335a28c8688e7fed89c0bf5299022048816219801e9f3952ff8cc32f0b09e060508721fc648307f735335789ef2fe4",
    },
    "0x0e09fabb73bd3ade0a17ecc321fd13a19e81ce82": {
        "ticker": "CAKE",
        "signature": "30440220568070fa3a4c88d54d6383fa4c1e2f3f16d69446ae5baf4b712c8d8931118f0502203cd35a099046b325a5bd2698a55a36feeb1e1fd392d47053205c0d31b13a94f7",
    },
    "0x4c4da68d45f23e38ec8407272ee4f38f280263c0": {
        "ticker": "PGIRL",
        "signature": "30440220224779f04813c2cc69d91a616ba12e85a07f1740c92347c2be701c8888ee55460220618f44c98c928378ab17b8be136434731676e8fe3203d65576859317ca5d4072",
    },
    "0xf3edd4f14a018df4b6f02bf1b2cf17a8120519a2": {
        "ticker": "PWT",
        "signature": "3044022006d28ef81b4903d664b7f57ca846ced93c87bc580163e83776d629c883e073b002200333c27d15d6418952bc7fba2ce1b1dcf2c8491746412d456ab27e3056f2b4cb",
    },
    "0xab9d0fae6eb062f2698c2d429a1be9185a5d4f6e": {
        "ticker": "PASTA",
        "signature": "30450221008cc82931d047310eed53da5d1970c0c81c506bb91d3f4dcefafe5e6f6788570d0220799f234458811a83591a0610c883eb20f258020ad9499861dfdd43ff6bde21bf",
    },
    "0x0f9e4d49f25de22c2202af916b681fbb3790497b": {
        "ticker": "PERL",
        "signature": "304502210087884b1e25dc6fb3ea89c18f699db1de2f30cd6a80329bb435bd8b6b15823ea30220024091929d1e4de87e4af6f5d34cc271878a991942e34e7b501e33a83dacc4ea",
    },
    "0x09607078980cbb0665aba9c6d1b84b8ead246aa0": {
        "ticker": "PETG",
        "signature": "30440220184a1b899a7e4ef4b9651ca4f6105742d4fb83a50a50e581c229d2137c92c0610220659c7acbd006fcac8253c4182659e5da33a95d9b09f21de037fae902e3514b9b",
    },
    "0xb9784c1633ef3b839563b988c323798634714368": {
        "ticker": "PHO",
        "signature": "30440220452be4152869f47f771debfa8a1db077ad65f1167aa407534788641f1a40d10e02206629d45a7621f2f7e72a10323a4bd6609b689821dc7298c9122347335ee80596",
    },
    "0xf03e02acbc5eb22de027ea4f59235966f5810d4f": {
        "ticker": "PINU",
        "signature": "3045022100d89911238dcdf710129906d19ac481bb671c3d716ba239ac71ef25b7bca3196e02200da77edbf0f1c1383d1034b809f705286e97094bf3c3f16550bcf828c69f04b8",
    },
    "0x9133049fb1fddc110c92bf5b7df635abb70c89dc": {
        "ticker": "PINK",
        "signature": "304402202e54139a66ba997cc846099d81548a03510af0551c49a2cebc1db84f8b8612550220396fc0efbb452f4af6370caeedcb67d9b2226bb1afa37a8e642838415ebecf7c",
    },
    "0xa57ac35ce91ee92caefaa8dc04140c8e232c2e50": {
        "ticker": "PIT",
        "signature": "30450221009d2996441aa3fbbc2f7307b5dafee3a4415eb774c7e7a5bcf150bfcb6882b53a02201c6247e2268bd10a2886d96e08d55c95e3d843b32945272ee8f4107f27450308",
    },
    "0x2cc26dd730f548dc4ac291ae7d84a0c96980d2cb": {
        "ticker": "PIZZA",
        "signature": "30440220406198fa86c916884d6cdc3233b9914287ddb6f16fa28a36338da6fd126ef3420220370bdf4eac0b0d4be7e4d5ac106c1d23b10976688cf5ce2e3bffc08616824b68",
    },
    "0x89a851764a427f48c21c1557d94458267fe3d372": {
        "ticker": "PKGB",
        "signature": "3044022024ccfdba9faad97a95feb968fb18c9a35eac3f63b60e1ce140d8e4827bf5e5cd022041a2fceab0f7b44492ea1c147d68e1fc8074c2e85eda47448adfc747dd56da58",
    },
    "0x31471e0791fcdbe82fbf4c44943255e923f1b794": {
        "ticker": "PVU",
        "signature": "3045022100cabd487c2ff590e326e00888192e3bfe91b14fe48a712518d3a6c6b7ec9fe6d002202e5a4031d4eb2d60bdc51a97090c883a5e98600c314fcdf99222fb1fdc748be3",
    },
    "0xdaacb0ab6fb34d24e8a67bfa14bf4d95d4c7af92": {
        "ticker": "PNT",
        "signature": "3044022013e5dcda140a3acc2d44e632102a90fc75f1a1ad557a7b8910eb597027fc7c2a0220492eb254dc115fe630577c968dc838419ec71de038f8c3321a19075f009a768d",
    },
    "0x394bba8f309f3462b31238b3fd04b83f71a98848": {
        "ticker": "POCO",
        "signature": "3044022041e4858110e76e9ef11429be64a4cd87c6cf2e5290f3e3ff7dfb06d23c9b6d4e0220588ef5f447f8a36ae77fd9426ff2a0767c0211ebd828d6b8d02031fb6929b23a",
    },
    "0x4b5decb9327b4d511a58137a1ade61434aacdd43": {
        "ticker": "PKN",
        "signature": "304402200ee244a781f51be9c2e912443cc581b8b87b6b48e1532b67d48e3ee63925077702200ddc33ce2c93aab03c65c3fbc734a0df74a84b43d8f1992b50bc2cfe19304e01",
    },
    "0x1796ae0b0fa4862485106a0de9b654efe301d0b2": {
        "ticker": "PMON",
        "signature": "3045022100a89d20606d742cb7ccb9c3b88c554a637fca1b5defdf210f6c1631eb93ab90cd022002065b00fa07ad409405bf9a82d891b1b1fc3c28e46146ecb7cce2cb9b2441cf",
    },
    "0x7e624fa0e1c4abfd309cc15719b7e2580887f570": {
        "ticker": "POLS",
        "signature": "3045022100a5c159f28e399f6dba8e137363f43ab4fef166802498a33690973f0f2d87af6602206f1d68ff8c3eb1feedf3dc0ff7e82517b22975c205878202b7e82aa546cbc8b6",
    },
    "0xbcf39f0edda668c58371e519af37ca705f2bfcbd": {
        "ticker": "PCWS",
        "signature": "30440220332d221af912e191984b6555bc107827eaf11512d81cd9c569a53c45370d4e2b02200e9d0b7220d7c0ed2fe58c54be1a47eeb9f72dae9cdab642cc56e8b504b3913c",
    },
    "0xaf53d56ff99f1322515e54fdde93ff8b3b7dafd5": {
        "ticker": "PROM",
        "signature": "3044022050f1ff6bf73307b640ded608f149616f4c568a81500fcb3c16d8c52d990bbe9f02203acb4b61c7bd19dc0daeadde0ee389ed07f50b462fcd2766fe5921458771f921",
    },
    "0x9b44df3318972be845d83f961735609137c4c23c": {
        "ticker": "PROPEL",
        "signature": "304502210091e3f137cd689803b728d1ef762720a3d938c8d5ab059262649b9ed38d1475ed02204a47be797e565f5c2571cfd378a9bdb0dc2f3aa23cb8cfc5708f5e64b44b71b3",
    },
    "0xed8c8aa8299c10f067496bb66f8cc7fb338a3405": {
        "ticker": "PROS",
        "signature": "304402201732983221029eced85d03893d79d423d2a66341cf97e1378a17c527a32fd170022016059b33142e42756b47c661934165a2c2a0c0aad0ee2f4d037ec3d3ba0f1d67",
    },
    "0xed28a457a5a76596ac48d87c0f577020f6ea1c4c": {
        "ticker": "PBTC",
        "signature": "304402203d2279de030970aa6c80af3af956a471aaf1b34fdd4adf70865c23e7c9c3719702204a93264fb98ba7a7f8e4dfbcbced0ffdfe048aae054a2f12f93429bef923ed73",
    },
    "0x1613957159e9b0ac6c80e824f7eea748a32a0ae2": {
        "ticker": "CGG",
        "signature": "3045022100c69ac697e0a1469840d2de8ea5652e440ee29c0e2faaa89e84f48e9dc7a0b723022004d3580488ae53e6b9722b7f1dfdc3da8fd6ff6f541e07064dcfa6b0d2561bd8",
    },
    "0xb6c53431608e626ac81a9776ac3e999c5556717c": {
        "ticker": "TLOS",
        "signature": "30450221009166629584783c9421e9e7877c5a9e652572756280b46c7261438b746452f6000220526028898074134e886934cc066ff3ed04e5abeef059ce57e440ddf9a6297218",
    },
    "0xaaa7a10a8ee237ea61e8ac46c50a8db8bcc1baaa": {
        "ticker": "QANX",
        "signature": "304502210099c04d143a29f79601f3d9c163bb57674f642823d73634a8d2f2d90b6132ec2902204a2386452d4f02d62d72bb8cad1dcbb88f53a2a72f8128d9135cfc4901508af5",
    },
    "0x1a2fb0af670d0234c2857fad35b789f8cb725584": {
        "ticker": "KUN",
        "signature": "3044022008de1e23a6bbdd63f6fbee56be579a130fbcb1f42ae61b49c778237dd4db35c202204194f681de7a4be620489d793e494d0ee436170137af757aaa89168aeab31eee",
    },
    "0x07aaa29e63ffeb2ebf59b33ee61437e1a91a3bb2": {
        "ticker": "QSD",
        "signature": "30440220624faaff8b29ebae7eb410d669301041a9bc6dc90e36d1f12d99e9027da72a550220607f2a9613dcc0748324330fef5ba8c5077d068431ddb0f2a5678c40cb3105c3",
    },
    "0xa1434f1fc3f437fa33f7a781e041961c0205b5da": {
        "ticker": "QKC",
        "signature": "3044022077fed0018d553e0c74e0e02f2d54430df2c1542f4d2db51489f9f75cdca41d4f022013c57a2668aee48cb5010324ec0a77f2ee8ee658c316615883b1bc7d773cf0c9",
    },
    "0x17b7163cf1dbd286e262ddc68b553d899b93f526": {
        "ticker": "QBT",
        "signature": "3045022100d7db436136398173da01ff3655c41f6ad2322a63eb82d802898be3480a530fb102200c241b47788e5ad7a8a1e0f7da4dca2c5bdcdfaa316f77f97ad0f86e01e936a6",
    },
    "0xb8c540d00dd0bf76ea12e4b4b95efc90804f924e": {
        "ticker": "QUSD",
        "signature": "304402206c223cda5a48b0105ebaae032f32fd79d46cbeaf549417cf3b41fae13ddf26e702207457700cfef13e27e04ea0d6527962c23ce71a830949b3a264fe43e3e7f03ee6",
    },
    "0x95a1199eba84ac5f19546519e287d43d2f0e1b41": {
        "ticker": "RABBIT",
        "signature": "30450221008a7d2b94f004ca33c76627a1e3d3536327e3069972dc689f6a4b823336ac01bc022021f2f49d5038a3d0459e7ccdb1333b7538c8873220ef68eb4ef273fa2dcaf422",
    },
    "0x12bb890508c125661e03b09ec06e404bc9289040": {
        "ticker": "RACA",
        "signature": "3045022100d3c3a51dbf3602e5dde8792b585aa666268faab2e22d20735c3365ed53b93cf6022072b9ce1a1ce1473617d4ea07fea453024309c4eba5cab55ebdd3dfcaf2b7af45",
    },
    "0x4f47a0d15c1e53f3d94c069c7d16977c29f9cb6b": {
        "ticker": "RAMEN",
        "signature": "3045022100baa34c0b49d8228fff2d414bc27aa2ec25a41de8bcff05ecff037245e757efff022030cdb073d3ec059b89f1865f7b1e10cacfed74ff7ab7507178bdb7f71f65aafe",
    },
    "0x8519ea49c997f50ceffa444d240fb655e89248aa": {
        "ticker": "RAMP",
        "signature": "3045022100b31bdc9219ffd3a73442818a0c2074178f824ca7674861a24fd097862d9227d8022071b82e60ed532d896c0a18a0f871f90ba03aea6573039ef596984e3fd80f1ff3",
    },
    "0xc2098a8938119a52b1f7661893c0153a6cb116d5": {
        "ticker": "RPG",
        "signature": "3045022100dbd42ba4881fbfc93b587a99729b04d09b68fa3289b1bd806056ca1eb245e339022033c4b19a6add29b23f81237de30a0f589d8d47e295aa3df445b20473bf4a9588",
    },
    "0xcd7c5025753a49f1881b31c48caa7c517bb46308": {
        "ticker": "RAVEN",
        "signature": "304402206f687c2c9f70fda8fce6acb68f79ac85be03e537f6e4a8f051d00283699b9df00220614e165cae90221407ceee2c430a5d79397911ac5096d725e5e3a7ba89023c61",
    },
    "0xace3574b8b054e074473a9bd002e5dc6dd3dff1b": {
        "ticker": "RBX",
        "signature": "30440220591e3afdea32c520a0d0a38b7f29e317f2d1ecf2f88e240cc88864c4f898448902201eb4ebd037202c7b0e3cb8d05dd27a2d7b6bec2ba8d2c306bac3e6c5727103a7",
    },
    "0xf21768ccbc73ea5b6fd3c687208a7c2def2d966e": {
        "ticker": "REEF",
        "signature": "3045022100b4333ce9f802e0d407e9a3f2f1776e98aa4b53d03508c73c1607356504f455f902207f8c11d08c0fee829c3f568f58e2a83ec84ac94eb45dbfda225216ad62e7b8a2",
    },
    "0x4e6415a5727ea08aae4580057187923aec331227": {
        "ticker": "FINE",
        "signature": "304402203fbcb4317dbb365b036b394844f33a31fa62f1e21fe50aea2b7d4ee78e54b1c702202eb3744ba4f829310987b3c88e20c4898f9579eafcfb5acd8ec2b64d4cf87a6d",
    },
    "0x4477b28e8b797ebaebd2539bb24290fdfcc27807": {
        "ticker": "$RFG",
        "signature": "30440220227e65fa9684ebdcce28c1374ba96f03d881e5f0526c07d16630d0eced80d1e302206c0648b6d98fc009c282467611675ff2c2df9349f440cb9036295dc8c4a26ad4",
    },
    "0xfce146bf3146100cfe5db4129cf6c82b0ef4ad8c": {
        "ticker": "RENBTC",
        "signature": "304402203568db2b2f30b26a8073ae07289081fbbc4ecdb05c7b7b4ecd7ce0048c5ce24f0220697196ced9af384b39b14593c7524146b0e1497cf5936f0e716f0d69e64a70d8",
    },
    "0xc3fed6eb39178a541d274e6fc748d48f0ca01cc3": {
        "ticker": "RENDOGE",
        "signature": "3044022047e2bb00fe0cf3e062d623bdac228c45ede193e697105ff349fd103ea92794ee0220551831432e69cf0c8828c701eb9b177eb882ae2a0463f95a864f6d02504cfd9f",
    },
    "0x695fd30af473f2960e81dc9ba7cb67679d35edb7": {
        "ticker": "RENZEC",
        "signature": "3045022100a8af6329eba39766f4e89feba6ca64701c8a9ce862bd3e7dc6b5f021ecbcfd82022074397a7ac1a5720f9e0973b311218d84f24ae48bab26f301250bb1e44151f36a",
    },
    "0x833f307ac507d47309fd8cdd1f835bef8d702a93": {
        "ticker": "REVV",
        "signature": "304402205a7930b9450d6a46af7d1b94dd1bc6a19ba84852702272563dc53efd776e65d8022014a9ee97a2a8059f7665ee9f874a0b9f5bf03db31d712aa1b0facf8b458f0f84",
    },
    "0x0a3a21356793b49154fd3bbe91cbc2a16c0457f5": {
        "ticker": "RFOX",
        "signature": "3045022100cdb676ff099d6c06fcb2c8d6f4d1e1c4b722c6c5b574940507437bda3d9202850220353aa3d64f0650e626c9de987c2a22f28b2ea7780810f8fbd57f8561ef3524b0",
    },
    "0xfa262f303aa244f9cc66f312f0755d89c3793192": {
        "ticker": "RGP",
        "signature": "304402207c689f23188969d8b14fb210032cccb769651201fbe1318d547f5e21c9d7ba1502205c5876f1b1e66b15b8fe93d1338eb164697371d16762912e339701719e7906d5",
    },
    "0xe64f5cb844946c1f102bd25bbd87a5ab4ae89fbe": {
        "ticker": "BROOBEE",
        "signature": "30450221008e9a8c0cdffe44092fc27ba99fb59efb88a6350e285a46bab7d96e9a38b69cff02205a1346e5565cf15b1441df5613d4518a51b2d37bccd980e99a3bcd9e9e6ce8f0",
    },
    "0x07663837218a003e66310a01596af4bf4e44623d": {
        "ticker": "RUSD",
        "signature": "3045022100c0bf47d183e3a3d03836b0669d6677c77a2913af4f349f4eca4118e7fe27c4130220385a23baf9448649e1033e37117d003247ca903c465e6b7a3e1ecf24a38e18cc",
    },
    "0x42981d0bfbaf196529376ee702f2a9eb9092fcb5": {
        "ticker": "SFM",
        "signature": "304402206a302699568ea5f8802b656f6ee05372f2201129fab3e29e609e3ce168c3589102204e5feb2837d81f419c5e46f1b518628879114298817ecffdd0eaf5d029710871",
    },
    "0x8076c74c5e3f5852037f31ff0093eeb8c8add8d3": {
        "ticker": "SAFEMOON_",
        "signature": "3045022100f0371aee4795e7cafafc65cebb6d49e0f520d7e113c9e1ee4a456ac11537ae1d02203c4f249d38ec68e5ce13bbea50147d8fbf9b03b481f9fec4562d52e30b2a49c7",
    },
    "0xd41fdb03ba84762dd66a0af1a6c8540ff1ba5dfb": {
        "ticker": "SFP",
        "signature": "304402204d09c51605ad031bfab07940c16db49c1a777b97a900e0f92940c80a1d00ec9c022077d16a53e75c781a0bec2a8376b095a520391b6a5b74149fdee59c651db7cb3f",
    },
    "0x8bd778b12b15416359a227f0533ce2d91844e1ed": {
        "ticker": "SAKE",
        "signature": "3044022056d44124553b4b1cab714beeeb011488820f4f65fbbdc490892e72389ce0c3c002206625488b56e16662616ee1a2adb1e1a4d2859c0c40d8f4ada364cb2bfdf0373c",
    },
    "0x477bc8d23c634c154061869478bce96be6045d12": {
        "ticker": "SFUND",
        "signature": "3045022100d050e19a0c1f291ac7aee469ac92b11e3d97c39037fe5fd874db59bf25042e2d02200973ce3ff2ba7a1d5957e99a39ab74c12a3bef3de86785d2c89cde5752451041",
    },
    "0x29f350b3822f51dc29619c583adbc9628646e315": {
        "ticker": "7UP",
        "signature": "3045022100ecb68fd9d0ed81e93e7aeb83e8c95e5a8d5c3fb5507bce95e08c8fc31ce0994a02200e28e9a7ae79b656d8284fcb3333ead64990c9ff07028f2b7384461e9c6204d4",
    },
    "0x76e08e1c693d42551dd6ba7c2a659f74ff5ba261": {
        "ticker": "SHAK",
        "signature": "304502210084cf605b6d13a807625fd9bbc084df57604b852f2cceefd460df3e2d2a3ab8c50220628525c87eae0d0c9a4a95f411560d4cf264bbca123787f7b2955fef8a3d3cb5",
    },
    "0xd8a1734945b9ba38eb19a291b475e31f49e59877": {
        "ticker": "SHARD",
        "signature": "3045022100dbd3fd2f38aece56fe207884d548c8fb3f7cde522760135781672c5094d9fa3f02201cb17e1258c659ff3c89d08d21dc21e3073cb6ef817a4197a6f2ac1ccfb9ce95",
    },
    "0x2859e4544c4bb03966803b044a93563bd2d0dd4d": {
        "ticker": "SHIB",
        "signature": "304402206308e1894f1762aa1132d27ce02fcf0ec5e75a6d262de438ae5bd67b869ac5d502207dad2132fcdd4a2684933c9c19350b0d469863f05b7bf2f983e5fb7a3b88d749",
    },
    "0x60b3bc37593853c04410c4f07fe4d6748245bf77": {
        "ticker": "SHIELD",
        "signature": "3045022100b09c75a458f7962a83cff9cae4a31dcf921ff9defb539c7d0ea53b8b04e1d8d402201d8117979eb8d29cc9d9c18ec2569008012eb1a893da86d44423e447b7103282",
    },
    "0xfb9c339b4bace4fe63ccc1dd9a3c3c531441d5fe": {
        "ticker": "SHILL",
        "signature": "304402204101dc9dd3789552fc4e735cd74b8da8723d4001d02c704b6e538e1ef081f6c802205040d451ea4953a634822afeb12eb11bebe8ed5a260504a41a2ab505a583aef6",
    },
    "0x7269d98af4aa705e0b1a5d8512fadb4d45817d5a": {
        "ticker": "SHI",
        "signature": "3044022039ce2bd36da48dad789279c435ae9cc378a371a4fc3ba3c3940a4f0753c0cfc90220181f785606d0b0bd62535c5c7e2e814091901aac6a08fee3a2b3b516c60ffab0",
    },
    "0x448bee2d93be708b54ee6353a7cc35c4933f1156": {
        "ticker": "SATT",
        "signature": "3045022100ee85772e04f546f3f5bf2934f995a712b110dffa4795c619814652a305f4e5e0022037ff525a70bbd38142b6c85c1014a410d5fba5127b272870cbebb56a318e5dbf",
    },
    "0x9a7e6791d7b23de3597463ae1d1bcc05f76e93a5": {
        "ticker": "SDF",
        "signature": "30440220771fa549f5fae8cf8da06d8e1bdbf67ad474ffc11e58ea26f6e04eb36eed2a0102207a2d9891a1ebb6842f2a5b5edaf5aaa947f3b6dc259044885e82ab3388982e81",
    },
    "0xfea6ab80cd850c3e63374bc737479aeec0e8b9a1": {
        "ticker": "SOLF",
        "signature": "3045022100c147871e38cad84cc788ad74ad5b0c871ce24dbefd4d00d2cb62e20dee416f5f022013a0bbcd5ca46b483991744d14f356d9791902de674e4e35b8ca6b7aa10f38d5",
    },
    "0x3910db0600ea925f63c36ddb1351ab6e2c6eb102": {
        "ticker": "SPARTA",
        "signature": "304402207f77ea4589c408404ca3e8b56d4df83092344d2c8ab0a92c605d5fb7d4d17cb20220750dea5c2a10b57e01efd95c0e185ce835a3e8b2c1b7cf3d921985a14bf9b478",
    },
    "0x1633b7157e7638c4d6593436111bf125ee74703f": {
        "ticker": "SPS",
        "signature": "3044022074e7b0a91523d9acb6f06267104d9243aea64ff5d350369a2e70944a99542d5702202b8f45ea17d3b2882d769a9bb91a6f9d9f21a8dc2655a7d14071ea0fcf8c5c19",
    },
    "0x33a3d962955a3862c8093d1273344719f03ca17c": {
        "ticker": "SPORE",
        "signature": "304402202ea47b296ef87dec815f5cca12e9b346d50335e260f8770c1435166a8f4390e302205d3abc9e685a9ea6bfb9a661591cbaa7e972fa5c485c2354cbc0612ae7a0d5d3",
    },
    "0x77f6a5f1b7a2b6d6c322af8581317d6bb0a52689": {
        "ticker": "SPORE",
        "signature": "3045022100a65a50a6120122cdc45daae89647f61c3a219a5cdbb8cb00834c3482100d453302206a27bb4d8a7160a5ad07d91e1093a9d6af96a74d076dc75b7895d848fcb171b7",
    },
    "0x8893d5fa71389673c5c4b9b3cb4ee1ba71207556": {
        "ticker": "NUTS",
        "signature": "3045022100c79f347707a9692c22325b4bd07c6a0ddd76bb7ccb2dad834209ce91dc01ae0c0220297185ef888b8870b1292ce2568a6875272a046461f1028358abad84cb1d48fd",
    },
    "0x0da6ed8b13214ff28e9ca979dd37439e8a88f6c4": {
        "ticker": "STAX",
        "signature": "3045022100fce872775cc6e3cb5f8344763a9c69387781db457ada53c35a2f93309f2871bb02201d057f329bb89857fbeabe5fae89439739a6ac9d5490748f13f383627983b236",
    },
    "0x26a5dfab467d4f58fb266648cae769503cec9580": {
        "ticker": "XMARK",
        "signature": "304402203122951984277baeebe42e438006f792a2f98be7bef7816fe5883f65259d03fc02207e4da4410f8d175567ce59d397532c549d1cec1cdc9cbdf5ebdec4b5aef84616",
    },
    "0x78650b139471520656b9e7aa7a5e9276814a38e9": {
        "ticker": "BTCST",
        "signature": "3045022100922a1d61057b68afdda8827398c31b9b752df0835aad06bb6ae803eba9372bc002204085ede69f7f3d083d25acdeafd26a5a8aa5394f75500c2aafbf588b2a0836d7",
    },
    "0xab15b79880f11cffb58db25ec2bc39d28c4d80d2": {
        "ticker": "SMON",
        "signature": "3044022052bef08fe1d873c79728c9a5ce2871bb441d8f3caec927236efda601cc3dc805022001a8750b7ff3533a88e7819982d2925534e98096055ddaac919258ad8aabe92d",
    },
    "0xc1d99537392084cc02d3f52386729b79d01035ce": {
        "ticker": "SBS",
        "signature": "3045022100af86f06e122de0d33cbab250231fd8d55009869e6851ee197085ffc9ad3494db02201d9a426f9396370ed38a44cefc3845fa48bf071dbc6e909465e1c981cca26059",
    },
    "0x43c934a845205f0b514417d757d7235b8f53f1b9": {
        "ticker": "XLM",
        "signature": "3045022100a188a8c981f143e4edca6f2934b1e7246532d8c82b522025cbc3eaa79049203502201ed9107b27bbdec2c160148ba428608f2d2c7d7fb4838553476b3cb1c0520167",
    },
    "0x90df11a8cce420675e73922419e3f4f3fe13cccb": {
        "ticker": "STM",
        "signature": "304402203a54a51d63cec3751b45908711d7388302472a819cd5f887a2466fbed6695cd802205e64de38a1ac2b5c7a0df148ecb9c0b8a14650063e12727b160faf3805c3b712",
    },
    "0xd6fdde76b8c1c45b33790cc8751d5b88984c44ec": {
        "ticker": "STRX",
        "signature": "3045022100da3a3d90bd17b96200ed890e0e472d0752339b418e2e7112191d2ee637810bbe0220522a251f615fe8a3f4754ff0638fbeeef395ebc5d9e7dc2a605fab154eeaf158",
    },
    "0x51ba0b044d96c3abfca52b64d733603ccc4f0d4d": {
        "ticker": "SUPER",
        "signature": "3045022100abd6084fc2ec25e9e53f65daf5bd1cf0a39fd5c6c50dad1c8742de5a1742ea6c022074a19f767c099ad0bdfd21b5cf4f8373912cbbaf03472fb623c2f24eca660f81",
    },
    "0xb5389a679151c4b8621b1098c6e0961a3cfee8d4": {
        "ticker": "LAUNCH",
        "signature": "30440220524deac6ab1dcb1f230bf0170333ffeefa00997f78eb289eb9b0543df631b0cc022060c668e8bbba4ac932413a2919ee931007e40b38ef90e47981a6fd5a1f636f19",
    },
    "0x4cfbbdfbd5bf0814472ff35c72717bd095ada055": {
        "ticker": "SUTER",
        "signature": "3044022046fd4b9cd1a4e92bdd2cf9727f62b21ff9db45225e4606f72e428ab468c5cb6102204b9e40014ddc499cc3151189f4798bfd88ccff8c5173f439458cb13f7b1013fe",
    },
    "0xc5a49b4cbe004b6fd55b30ba1de6ac360ff9765d": {
        "ticker": "SWAMP",
        "signature": "3045022100e8bfb628bd98bf280725ee0f6a93de0d4b89e7958900a8ab58602e7f8c289b090220438a768a92ddc9848bb1b5ced75f7f9a4cbc18f189d6efd9ba48a219769c24fc",
    },
    "0xe64e30276c2f826febd3784958d6da7b55dfbad3": {
        "ticker": "SWFTC",
        "signature": "3045022100a161083d2210f2ff6a54c7dc11568be59851e860de8205a8e4c0c39c3056209a0220664fece9304c5855d6d583bcef1a6dd606dec1536e80509ad5c41406d0c05632",
    },
    "0xe792f64c582698b8572aaf765bdc426ac3aefb6b": {
        "ticker": "SWG",
        "signature": "304402206e1f5be0e02640e1e616d7157e22af4c90df816f83d9567296022f6d068b2e59022073bacd164a75ce9b75547f44eede39aa9f5eac6096c1933d0838f69ea6e4f67b",
    },
    "0x71de20e0c4616e7fcbfdd3f875d568492cbe4739": {
        "ticker": "SWINGBY",
        "signature": "3045022100c7c62c9b3cd9c73a26b1a50763fd4629fb997068b36607f0685bd14d1c30e2fe02204ac726c98cb53a12f43e996b68d521d24e70390d42c3cd82203a3df4b0a0cd5f",
    },
    "0x47bead2563dcbf3bf2c9407fea4dc236faba485a": {
        "ticker": "SXP",
        "signature": "30440220348a4b1edd0089a9b050d1599e44c7bf72c9077f648b7847c553d73987e4ce1e02205c94f1c2daaf9d96f9f04d3e0244c200a2ca70c5691019a7fd18fc693c91d9ff",
    },
    "0x250b211ee44459dad5cd3bca803dd6a7ecb5d46c": {
        "ticker": "SWTH",
        "signature": "30450221009d6d7c351898a87f5bcda078ed541288251c660d01a22a4953492287f3f6aaab022033b5e48afdf384c174e118e69b85b4aee047ee9f26733ce06654584308ef43ee",
    },
    "0x7e52a123ed6db6ac872a875552935fbbd2544c86": {
        "ticker": "SYL",
        "signature": "3045022100be734b2d30de8915db2ff85f7a067deacdf0b4dda236631e33d9364d8b1b523902204909a3de410e1b40fbdb3e453eb07802871fc4a2c7ec3d3c9e97a409b9e0abdd",
    },
    "0x9066e87bac891409d690cfefa41379b34af06391": {
        "ticker": "TACO",
        "signature": "304402207de66b5412459db76124d1fdc43d6ba53b80ff019edccf4dd031bd98ed56f4b8022006c2dc60a4804637dac41cd6edb94557352247cf242cfc5f147a51b9b673cf1e",
    },
    "0x2cd1075682b0fccaadd0ca629e138e64015ba11c": {
        "ticker": "TBTC",
        "signature": "30440220580861c0676ac258ebe5dfd26313b75751d558caec962b868ae58024478e4f6e0220410abe3ecebfa7f23d73e2365cb6dd640aea3bff5dd301b01756722a89b3a23a",
    },
    "0xe550a593d09fbc8dcd557b5c88cea6946a8b404a": {
        "ticker": "TDOGE",
        "signature": "3045022100e5965a5dbbb06d203aa8ebc79a8569c16f5bb615b4e319a938ca5bce07febde5022034cefd4535687203af614bdebc31ff975ef9884f676be71d29dc35283e604a07",
    },
    "0xdff8cb622790b7f92686c722b02cab55592f152c": {
        "ticker": "TEN",
        "signature": "3044022079ab845323e9224b993eb3a443dd9d9f7a560938bd90ba268282083c3c0754670220714793e384d89595d450c1c955de93d270ae5b7058ede1484f6da0fd5395797b",
    },
    "0x658a109c5900bc6d2357c87549b651670e5b0539": {
        "ticker": "FOR",
        "signature": "30440220398f96e5a530e6eb449b08b35c3a0aefd994149382df41e0b1e1fd0ea1322eb802205c7802b1c71a5c4059be6979ec358af154a961f957206b1fe8e88ef8bd4ca28a",
    },
    "0xe10e9822a5de22f8761919310dda35cd997d63c0": {
        "ticker": "THUGS",
        "signature": "3045022100d3179592e9f76aef9e2a103f9a80997c8c4bd6b1f7d022a832a751cbe749737502200a80a9129c0e5d330fde9246276d7e8efa503022831025c19d443712560476cf",
    },
    "0x9b76d1b12ff738c113200eb043350022ebf12ff0": {
        "ticker": "TIKI",
        "signature": "304402200cf2dfe9903efe1416a97d771b459df4b660befbfaf39d22156666857409b825022028169556d93ec85dbe0545b17559fe023eeb2cec6d4735a97c12d039672e4dd0",
    },
    "0xe898edc43920f357a93083f1d4460437de6daec2": {
        "ticker": "TITAN",
        "signature": "30440220490368f646860a60c8f3abb25ac9377453a4d62a04a6c71a8594bc4235ad9ae3022032d99abc3b901da8b049fe73c2fa365e12abb55b31761aaaadce4e0c1903e7ce",
    },
    "0x1ffd0b47127fdd4097e54521c9e2c7f0d66aafc5": {
        "ticker": "TXL",
        "signature": "3044022074673bfd10aca228f7a0266882050ceb755d67e6f3adc2654a3f4d6c499b569002203d80c62f65ba208e1e2b30dcc8a7c6d6b15c711f92d992744135342e5ed3af2f",
    },
    "0xeca41281c24451168a37211f0bc2b8645af45092": {
        "ticker": "TPT",
        "signature": "3044022068100d73456fecab404a179dddb3ab456a0caee8cdf89d83ec7c51bfdf6085e0022039f7d4cd728667a5c296d5b0a23b3929bf838aff9aae1535fca25e9d13839afd",
    },
    "0x9f589e3eabe42ebc94a44727b3f3531c0c877809": {
        "ticker": "TKO",
        "signature": "304402206af721adfefebceafc554b28a9f05d7cee364fbf548a0e7465ad06267364a21802201634ebb03605e859a69ae6141f27b3506610160ed00d2cb976a3f81743322c9a",
    },
    "0x3f526a5a8811cc1812d923d5dbb20e4b1c6028db": {
        "ticker": "XTM",
        "signature": "30450221009d76f53f03ad717c422fef0fdd8dde71fc0555c4cc342cf02838032510c642f102204ca225ba03ee27e71610211a6d5986978ca43c78ebafeffd11b61c1a6ab1feae",
    },
    "0x0377facbddbee59d40869808bb67fa741038bc67": {
        "ticker": "XTF",
        "signature": "3045022100dd1eb29240f8840084348b7c7463158cd78a831939ba2b1f3985ee435c4a68e802207c1967c5d07a65a7b890c18f3978161252646f7a6406262c01e51c4b6a8e17fb",
    },
    "0x6c0a568a3ffb61957812fb3e300e4c10b708d336": {
        "ticker": "TARP",
        "signature": "3045022100ff6eb6db6971653cbe6d1455525dd9f7485be3b50edbca219b531883ba55e49302203fffe3636834b719ff21f95de0bd788f4fee4de4376929fe5651eb0f94c805c9",
    },
    "0xcf0bea8b08fd28e339eff49f717a828f79f7f5ec": {
        "ticker": "TOZ",
        "signature": "304402204c865a30dc462e4152bd47071bcb5966009dba9e69ba447183714af202d58d7b02204f03a1ff311145821b0778bf614a4a36eb4acee3c425deac46d153003def31ac",
    },
    "0x9bd547446ea13c0c13df2c1885e1f5b019a77441": {
        "ticker": "TREE",
        "signature": "304402200e400345cd2f0802373d84b9a31152a41a86eaabb1d6620f4442ee13c6c7bcbe02202dadb237ae3ae40bdeb55976adb2d80c6eaf6c87f8943e8ba605bf0ffdef7026",
    },
    "0xa4838122c683f732289805fc3c207febd55babdd": {
        "ticker": "TRIAS",
        "signature": "30440220623cec6ce73a5f8a7db0584efa6a04bd2b7d6a30d1828f00b0a13da171e25e7a02201e04ca76a59af1447f9045ffd6771945bb29363b6a1124d3c1ce5169a8e321ba",
    },
    "0x85eac5ac2f758618dfa09bdbe0cf174e7d574d5b": {
        "ticker": "TRX",
        "signature": "30450221009e098fdb92b93eafabe62d3d7eb70939d646fd32dbf1e8ae71fbbcb8ceaade6802205c64907deb7013fb43dd14f8ddc8548ca827e65183fd08d0d2893a1c93ac185c",
    },
    "0x1bf7aedec439d6bfe38f8f9b20cf3dc99e3571c4": {
        "ticker": "TRONPAD",
        "signature": "30440220282472564de845975d4548a9dabd224a33f111eb4e506a706cc9d78df19344230220562ed05914677e5e32568270f4bba603b6207d64451652adb51be60e9f9142b1",
    },
    "0x4b0f1812e5df2a09796481ff14017e6005508003": {
        "ticker": "TWT",
        "signature": "3045022100b7c67bb307b75574d8a8f3825f58b30fd635379f1a8f24c8209cd9c720dbb778022053ab485d6a2c8bd561e447e148955d3e84d521710e6457555847345ff12a70b2",
    },
    "0xa2a26349448ddafae34949a6cc2cecf78c0497ac": {
        "ticker": "TSC",
        "signature": "3045022100dfdff34cfee25ff9b2289e4acf1cb9b303b1a98d280015df64a2e3107ba1bf4602204eb74d9f3278544299ffc30efd767c27c2016c2ee389a94ad6b06c102d08492c",
    },
    "0x6a8fd46f88dbd7bdc2d536c604f811c63052ce0f": {
        "ticker": "TRVL",
        "signature": "3045022100fc30abdb28db7ab3310e02326dbcb5953d60409b958b14dbc965b3c591c4c1f70220647863e3647c56c1d0d71a0870dc66fa9b62450a8bee0c90db330fc39221b816",
    },
    "0xaf83f292fced83032f52ced45ef7dbddb586441a": {
        "ticker": "TWIN",
        "signature": "304402202ab4306f0da054866fe5342c19391a6b3b7a754daaa8b062690288332c55d22d022062514605ba5be4fa8e02f08688d00cd2110f98f3fbd858bd2d1ca2f1d9967d18",
    },
    "0x7d8156d75495def6181ca6ffcf7631c1965dca2e": {
        "ticker": "TYLT",
        "signature": "304402204ba78e66e5341978d3fcfb023b4d3fa7cf1f2fd6eb2f13cfa81be51196ce4d13022075c76f811693256a137eac8aefed514654610ec0b23e201903a1535755eda92a",
    },
    "0xd2ddfba7bb12f6e70c2aab6b6bf9edaef42ed22f": {
        "ticker": "UBU",
        "signature": "304402205af3e3492656a3d9143ccb590a58208f6377b8624ec9efb5a8775606bd8677f3022034f11ca2bb5651d75ba19227258df531f6a330222cbf4cbccfe781f10f9d9cab",
    },
    "0x0e8d5504bf54d9e44260f8d153ecd5412130cabb": {
        "ticker": "UNCL",
        "signature": "30450221008682764019dcc87541887968b1a6d12dc58384a63ddbd3d9d29964f65d4eee7d022034339684f7e4e3d504bb624ed1163f98e732c139f9817e4d07f34f00e502e3d4",
    },
    "0x728c5bac3c3e370e372fc4671f9ef6916b814d8b": {
        "ticker": "UNFI",
        "signature": "304402207994c0929a550a738260f6bfe2de9f2bec60188c1844481b370b2c6734c22dab022052c61698b944672a6bcedc42b7bc78eae7497ff68eb1d77a4d3ea689c4abfb5b",
    },
    "0x09a6c44c3947b69e2b45f4d51b67e6a39acfb506": {
        "ticker": "UNCX",
        "signature": "30440220226f7f8c4cb5bcecea170f4ccdbbcb4549c3bc0ec297d01ac2e0e1f1dee7966a02201eebc3abaf2a88ac521f89a6955db586d1a643e5186337094c880734bd29c4f1",
    },
    "0x7af173f350d916358af3e218bdf2178494beb748": {
        "ticker": "TRADE",
        "signature": "3044022038bb7a7620fd4cab0066b1cd86e579714ff5f03b10a7676d93ef5cacdf1c47b0022061490256f2d0c6b5b21a99ef148ad0cad2aa86526285ca9d8dd147c358a475e7",
    },
    "0x2fa5daf6fe0708fbd63b1a7d1592577284f52256": {
        "ticker": "MARSH",
        "signature": "3044022022cbabdd652a08795aeb59b7906c3ded7d26c8bf8ae3b81a0576850631859d3c02206cce382a8aeecf446be257356c376397f5eb3e034fce5db6911356fd32da63f1",
    },
    "0xbbeb90cfb6fafa1f69aa130b7341089abeef5811": {
        "ticker": "UBXT",
        "signature": "3045022100c0e839897f52d285b83ef74d4033bb71bf79726557cea01381aae6e177495312022018db5bc3d39bc8114097c755c97f534d37cb493a51164986fa94d30d9ea06090",
    },
    "0x1203355742e76875154c0d13eb81dcd7711dc7d9": {
        "ticker": "USDX",
        "signature": "304402206db6804aa8551c18943dafc594c7724cfeb27000a5b13c40759317da795aa36502200576b5a1488e846b98ee39bcd0ae3ce3c183f0a5dacc1d1afeaa44e17c5851ef",
    },
    "0x4bd17003473389a42daf6a0a729f6fdb328bbbd7": {
        "ticker": "VAI",
        "signature": "3044022071e73027e08de75e77fc3e720e69fd16478ca6a30cdec78857a2203dee07364202205095ff5429be358c2b34cefacf34f6ce550bcc31ba5271c2039e937d666df985",
    },
    "0x8f9b482b74afc0b7e8aefc704f1f04df208ee332": {
        "ticker": "VANCII",
        "signature": "3045022100b384e87a18b10da41fa069125c1b29f06746d9c535981b56cb83daf6f2d63c4402202695bbf99ce74204e39a368e0af1a5180fda942346ff4ff369ed9b166d147ea7",
    },
    "0xcf6bb5389c92bdda8a3747ddb454cb7a64626c63": {
        "ticker": "XVS",
        "signature": "3045022100f12e1bd8445053d9fe45ebe93b9a3220f7fbefa5971315a98745ebf2f4581ccb022064ef5a26998de58fba194b253f3f84a1c65934f9bca921a222f6cf6262a63d2c",
    },
    "0x9a0af7fdb2065ce470d72664de73cae409da28ec": {
        "ticker": "VADA",
        "signature": "3045022100f773cff2b283769cf993888f0a7a99fe32d99b26bf655e0d69b4a37f68ab9c7c02203b38b35f4393c1e957ed7dd935b48be9134e54e100c8bc4fb9ba6d11b60f2073",
    },
    "0x5f0388ebc2b94fa8e123f404b79ccf5f40b29176": {
        "ticker": "VBCH",
        "signature": "30440220507bda1743bb2c21c53839391bec0d1cb552104435e2315d5a2c5da0b1a833970220642b91735a89f021cc28fac264847367015255df18d18c3c9bad05c60ef60aae",
    },
    "0x972207a639cc1b374b893cc33fa251b55ceb7c07": {
        "ticker": "VBETH",
        "signature": "304402202670d1ce954344288b343ba90323591483385d98968124f7a19907504301b6c002202da268537a9fe6f5953057c1f50fcd12c755fb10ced8d4adfce03c68931da446",
    },
    "0x882c173bc7ff3b7786ca16dfed3dfffb9ee7847b": {
        "ticker": "VBTC",
        "signature": "3045022100a8352632950cd6791ee7eadcaa786d3d07c316026793dc25f4f66bb543e4c827022070acc439b004ce6f4667f8be1384afc1f311f8492d92405fc3321921cf6825b4",
    },
    "0x95c78222b3d6e262426483d42cfa53685a67ab9d": {
        "ticker": "VBUSD",
        "signature": "30450221008268d3fb0e0ca071947e622d2519c99aa0de514cc5b351645a244ec7dc10811202207770a8192e5aa09f956c9027adc520337c54371075fb2f526fa0da1cf6af77eb",
    },
    "0x334b3ecb4dca3593bccc3c7ebd1a1c1d1780fbf1": {
        "ticker": "VDAI",
        "signature": "3044022013e7b7882e5108a084f745fd3487a1a0fba15c73c4007de1f0a077a3ddaacc5f022015dc2d37a19e8df80393987e911a73b9bf396c75bbc664d35eff15dde559f6d8",
    },
    "0xec3422ef92b2fb59e84c8b02ba73f1fe84ed8d71": {
        "ticker": "VDOGE",
        "signature": "304502210086e9099d869244182da03b432fd49ce4a4fd78fa573dc8a791dcfd4f2892a8ac022069557485bb1615087e9f52fb9bbd9b0a28d4c6bb42856ad48f6dbc2c30c69c76",
    },
    "0x1610bc33319e9398de5f57b33a5b184c806ad217": {
        "ticker": "VDOT",
        "signature": "3044022073c40d616a886ad677aedebb3520b764de4f4fb41f5310108a5573db08bca6f602207a91b0c8279ec2b2132e8ced0fb61557d8b61ba6315e0cc2fa408d700c311ec4",
    },
    "0xf508fcd89b8bd15579dc79a6827cb4686a3592c8": {
        "ticker": "VETH",
        "signature": "3044022001ee0d90f0052df2083b34916ccb954d02d5fdf9dfc36d93ec950295da803fc502203bafe876371b06e0e5a323fc3f9d5f06e51f9e15d574b3701340ecf5782029ef",
    },
    "0xf91d58b5ae142dacc749f58a49fcbac340cb0343": {
        "ticker": "VFIL",
        "signature": "30450221009e6d1586e4920cf908c6e5c2924dd07f727e3f3629dd81bd6fd1631d30462ab902202a38e8488cd3deb3f7a62ee86d269b6b460e9de247181a7a98ca3980608861ca",
    },
    "0x650b940a1033b8a1b1873f78730fcfc73ec11f1f": {
        "ticker": "VLINK",
        "signature": "3045022100a71cfd395e1a7512fc5dcb2bb17eb76f9fd60d6b3d5dc44feca49d22f46b84fd02205978981b2a85fa347f3519d37ddd3d852ad412a19d88bfb5d89d253f58fb3270",
    },
    "0x57a5297f2cb2c0aac9d554660acd6d385ab50c6b": {
        "ticker": "VLTC",
        "signature": "304402200ed52a92457d0b5361ea690920be92e794f8bafb26776d2556c537a097a12f40022074cea03ac869a7229398808db7adf8976bd3f3f9a824b0bea09d932a27c2838d",
    },
    "0x5f84ce30dc3cf7909101c69086c50de191895883": {
        "ticker": "VRT",
        "signature": "3045022100ca2029e6335e6a6a903e0ed639352d8725da38788f773462735177a4e37070ed0220711af4bea6f3f0532c3d3adfa457a8970b7c1f0b9c8178aad060d91aa53d5a4f",
    },
    "0x2ff3d0f6990a40261c66e1ff2017acbc282eb6d0": {
        "ticker": "VSXP",
        "signature": "304402201fb0bf574976936e183545a9487a625670bf4e10ab42982d0bdd2ccc6803698702202e4127de822298b0de906be0073592b07209bc188373ef2a89a92e55d004aa38",
    },
    "0xeca88125a5adbe82614ffc12d0db554e2e2867c8": {
        "ticker": "VUSDC",
        "signature": "3045022100f3cc3e8d328124ab12de9fa9e8d8247624e8fa1b8b491ea4cc75ef47204112b202202972958bcf3cc552ba9fef59304b731d218c100f1258582c8e4a499b5002a360",
    },
    "0xfd5840cd36d94d7229439859c0112a4185bc0255": {
        "ticker": "VUSDT",
        "signature": "3045022100f12ae67d908b6dacaf18522d7bec66e77147040da0896eec64d14284112821f002201d85add411715a0771894dec579ebe702de952044967a66c67fa0502b13b0489",
    },
    "0xb248a295732e0225acd3337607cc01068e3b9c10": {
        "ticker": "VXRP",
        "signature": "304402205a06c1d9959650725fd8ef382c6895d4c1b4dd38b723e0abeebd7242bd409f1702207308e19c7e31b69932ec33f28cff521e6e74edb78c76e09279e12911d691abc3",
    },
    "0x151b1e2635a717bcdc836ecd6fbb62b674fe3e1d": {
        "ticker": "VXVS",
        "signature": "3045022100b575c24c6430cfd9b30d6e980b824e52944a3321efc56ef7b028c1e82129851e022043565587625052e931fd7f483a52dc83b593abf2a27162e7184d8069781e789a",
    },
    "0x4d61577d8fd2208a0afb814ea089fdeae19ed202": {
        "ticker": "VFOX",
        "signature": "3045022100f04e8403e45b5e5e64d0b081925045f0fcef6d279c58dfba1fe06ef39834f837022052b90710e40ddb79b313f9fbc6dd50b63d9d7c03c0d2dfdc0cb0b9c6a9c0c4bb",
    },
    "0x4f0ed527e8a95ecaa132af214dfd41f30b361600": {
        "ticker": "VBSWAP",
        "signature": "30440220358ded81de598430a12217a6435ba9935c39f70d39bc1069a374305a4436ad0a0220633a645dbb1e35ff4bca81b6356688795ec9c6ff9297b748e0b48fa180ecda95",
    },
    "0xa58950f05fea2277d2608748412bf9f802ea4901": {
        "ticker": "WSG",
        "signature": "3044022028be2835e99c6630f45ff6eee76e76d296818ea0e9c8a1bb91d636954ec1b40e0220641f6f277b63f1a6493fdac17078729d38ccdb81e06f167aa20611df99461f64",
    },
    "0x339c72829ab7dd45c3c52f965e7abe358dd8761e": {
        "ticker": "WANA",
        "signature": "3045022100a9a6e8c05139157798de24a292e9a0cdd9ca74630dcc4eee31e5852bece453a302207367af5c47e4238da0c2e1ad9a0842f2e936bb46edddade1f7206eb76ec80025",
    },
    "0xb64e638e60d154b43f660a6bf8fd8a3b249a6a21": {
        "ticker": "WAULTX",
        "signature": "304402202e9a50842a76d362c753b0f13b2c9b160161a158cc3d8938973cb279c9b05a5802206a71e86fa6f9ad53c939fad64a930d567ef1a02368a3f27f74e84d427a9d7d96",
    },
    "0xa9c41a46a6b3531d28d5c32f6633dd2ff05dfb90": {
        "ticker": "WEX",
        "signature": "3045022100a9a98be7bcf62e02619a8502899d1fa4654c38d1c80f7cc85d97b6a6bb66419802206404845a206ea9efc080af6f64643fca38a124b5e1540d6d058ef949d6f2be0c",
    },
    "0x8e17ed70334c87ece574c9d537bc153d8609e2a3": {
        "ticker": "WRX",
        "signature": "3045022100e908f7fe3fb4e91df4e46d609056c978e9aa63a29998c8f7f0a9e133960bb5c3022046c448abbcfb75a547687658b1d7637813e436235dfa9fb57ce81aa0c6beb8a7",
    },
    "0xbe3e4cfd929156f612df36042d79201ecf5344d7": {
        "ticker": "WEATHER",
        "signature": "3044022043fcd482a2c055b8084c6ee764df5602cd2bdb3826d4976a3a737066a2b5c77a022017ff8356f8106eb78539ab069646e0c2bc15d893aba55e909d599d9e9c43eb06",
    },
    "0xfafd4cb703b25cb22f43d017e7e0d75febc26743": {
        "ticker": "WEYU",
        "signature": "3045022100a03493e7eeb0bc9e9c209c2a389dca6752d4a265c4e4b2f081de9073b1fef20202202f8fc7af42b6b2f893f942c7604813ee8f9f95d69d10acbd1750f0d50a61a162",
    },
    "0xaef0d72a118ce24fee3cd1d43d383897d05b4e99": {
        "ticker": "WIN",
        "signature": "3045022100905bbf768fbebd4be0bc30170111e874139b3dd6efc1539e6817d5b6cf7a7da402207373124bb8faf3f11664b8c9f66648a388bf2bbcaf17d2529f2f30595c0c9d5e",
    },
    "0x4691937a7508860f876c9c0a2a617e7d9e945d4b": {
        "ticker": "WOO",
        "signature": "30440220724870491e6538a8a31d736107e51c825ad230c82f8e4824c8009fd1a86db71502202077cbc773b1b555dae6b3e6f1df0a28c27e484e90015a1ca6822585e27e7479",
    },
    "0xe20b9e246db5a0d21bf9209e4858bc9a3ff7a034": {
        "ticker": "WBAN",
        "signature": "304402207b1fda1663e2d01171de9aae9c4cf415744bcf6a29c90c610a5808f4eb159bb002203cc8a27bd35e2a3eeb1e571ea83bf92e2fe51c36b61dff4bdf58fcc40604304d",
    },
    "0xbb4cdb9cbd36b01bd1cbaebf2de08d9173bc095c": {
        "ticker": "WBNB",
        "signature": "3045022100833764594d07df7412279ae381beb0290dcc3111a8ed09263e7cf347bff9d1bc022060924b6531989108d016f3398184119dab014ef9e802e5018bdb15c74a14dfac",
    },
    "0x7e396bfc8a2f84748701167c2d622f041a1d7a17": {
        "ticker": "WMASS",
        "signature": "3045022100e1ecb0c24b0ce278e82637b50ed654d6143bb0978497bfda082ae1da25d14d12022031ed2cf75159cac2b5af522af44cee41f72b8a72ad2280ab9541f913380f6e8e",
    },
    "0x5b6dcf557e2abe2323c48445e8cc948910d8c2c9": {
        "ticker": "MIR",
        "signature": "3045022100fa918e540eb462df087557e24afc6a690b9ff1e13089bd1c68dedc1ab6d3396d022056552d08caf31a549d8ad849b7653b189854e0dd3b0219f6196c14379e9fa193",
    },
    "0x3947b992dc0147d2d89df0392213781b04b25075": {
        "ticker": "MAMZN",
        "signature": "30440220241de43069ba4c3e95db11dc2db0292276f7b792c0b0fbdb072228cbf56350cf02203e836ba5b23a4fa74e9164912eef021c60e739d8cd6bd196df9a17a5e585b097",
    },
    "0x49022089e78a8d46ec87a3af86a1db6c189afa6f": {
        "ticker": "MCOIN",
        "signature": "3045022100b958ecdfff2e9e7088dd3bea1546a6f0e6e7953ae81d99f71935aa3035a27b470220549c6499d29c5fd57ed8a277984ebb309ea2dc038347c0a50c227ae292a92d03",
    },
    "0x62d71b23bf15218c7d2d7e48dbbd9e9c650b173f": {
        "ticker": "MGOOGL",
        "signature": "3045022100b7617806d6b8972a9d712df365a136bb47939bf16134c0cf5dad7ada636ba9870220163828dedbb3ea6d360c393763ab56d654cb0bf5d1ec9dfcc7e0f46e9b709062",
    },
    "0xa04f060077d90fe2647b61e4da4ad1f97d6649dc": {
        "ticker": "MNFLX",
        "signature": "3045022100fec7aafd74c3ab812110204c5a755576cbe017d6c907134c074e9db2b7e39af70220427ca59e1404161ba76ea1a939b8275f1fe7c96b945d2c9908deeb34950b33a2",
    },
    "0xf215a127a196e3988c09d052e16bcfd365cd7aa3": {
        "ticker": "MTSLA",
        "signature": "304402201cd945cb66d28596bcfc9382439fced7b8da66d03d857f00fdf273a7a7d7c7180220118e77c2272e175091523f518e1f56c9233d97746e04723e28ba66913af54384",
    },
    "0xbd2949f67dcdc549c6ebe98696449fa79d988a9f": {
        "ticker": "MTRG",
        "signature": "3045022100b29743525a40ed1efee7504a47121919b07d6792ab25f3fcba5d9b573072e23d02205905970ec0547a83aa22059521db09cdaca8b78f3f144eb64ce588f1bf663ca1",
    },
    "0x541e619858737031a1244a5d0cd47e5ef480342c": {
        "ticker": "WSOTE",
        "signature": "3045022100e145981c9fbbaffc1c599047932a305c294042e67028fd32cb2dc119cf48f8d202203d6695ce0f7237be313c7ddbfa8686d9ed9b2db5cf35aee325fe78f7a621b68c",
    },
    "0x23396cf899ca06c4472205fc903bdb4de249d6fc": {
        "ticker": "UST",
        "signature": "304402204acd85764548290fbfa945abc4290b53c6497aeffdf08140c4758d457482503802203451ad30df072e2dba51bd9bb0e7e57df189605c862fd22e940862b7c625c079",
    },
    "0x4a080377f83d669d7bb83b3184a8a5e61b500608": {
        "ticker": "XEND",
        "signature": "3045022100b4aa69007e4ac6d0c89953e9c78e741b78ac2f3dc052b4f6d902ae48ed9a0ff802203a6a026a85fd81f39cacd772cb288e9fcc54640df897d798fabfa5e728c9435f",
    },
    "0x6b23c89196deb721e6fd9726e6c76e4810a464bc": {
        "ticker": "XWG",
        "signature": "30450221009d8d2aa08231c0c1d259d8c4e5925cf2a7e07a7d25791c50f2d694cd291ce58902204fa5275096f4005e6d755eea55cffa07dea7fe2d4ffe980595115f8dffe8e76b",
    },
    "0x524df384bffb18c0c8f3f43d012011f8f9795579": {
        "ticker": "YAY",
        "signature": "3045022100ebe33152f447989bb43b3a4cf2168e3095be399e20650c8172702c0fa4181abf022077141a37fb43ed2496a0b2fe554f74b0f2fe0120b08f1e6dc9a4b79ae2dee01e",
    },
    "0x7a9f28eb62c791422aa23ceae1da9c847cbec9b0": {
        "ticker": "WATCH",
        "signature": "30450221009ad3557be6be1aa95938e2dda4493d9e024a1f8531e820004834ddc9936ddbf402203342bbed1ab7bcc15338f721248109f23294ac3c16754b0bbf8b36a3ddd12804",
    },
    "0x02ff5065692783374947393723dba9599e59f591": {
        "ticker": "YOOSHI",
        "signature": "304402203a277702e0fb03a69d8df49ab88ae5dc6b72c2b9ba228607b72961b9424889ea022061766b9dd6b68268476fc0cdff081838579e018f7f1ea57f69cdfc09d4d42b6f",
    },
    "0x44754455564474a89358b2c2265883df993b12f0": {
        "ticker": "ZEE",
        "signature": "3045022100c44ff8c97030279f65b74420e5ab98e9e826c6165bcb400f0ae00f3924ea5ae70220700b43ffa3e052d56afed580cc3555e2695cd158e1c8380c29aec6485378289d",
    },
    "0xb86abcb37c3a4b64f74f59301aff131a1becc787": {
        "ticker": "ZIL",
        "signature": "3045022100fd2edf3cbcfeecca704328ea37ff8f9a769aad6d630b3c54cb948651d0eab32a0220701e203cb421839ba4665d1332fd54b7f21cc51559b78cfb0e1f9f6263dc6f8a",
    },
    "0xfcb8a4b1a0b645e08064e05b98e9cc6f48d2aa57": {
        "ticker": "ZMN",
        "signature": "304402204b817f8f01709cd3c1ece3813650c9c213afe3ee1de7f3660d724ef6db8ff68c022032d830eb9626ed7cb94342fdaaf195afdb98d069198c01dfb42e35d3ea27c6da",
    },
    "0xfbe0b4ae6e5a200c36a341299604d5f71a5f0a48": {
        "ticker": "ZIN",
        "signature": "3045022100c3e4ab99c5863308ad8d7942c5cd916d7e2078547369791d7c93fc050c8f9bbf02200afb92da228024eaa9a0d057ee5d8e0ba73187103fc53eaadd8411182eca612a",
    },
    "0x21f9b5b2626603e3f40bfc13d01afb8c431d382f": {
        "ticker": "ZINU",
        "signature": "30450221009fb3b099e37b3d56ce14c7960aef5b216d5f72a7fdc1ab4210cc829cc4f102cd02204c078ad1029af9affbd64afb6f847f0bc7be48e6f9fe9cf18421225f9dd0eaf9",
    },
}

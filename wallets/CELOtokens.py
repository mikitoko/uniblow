# Uniblow ERC20 list for Celo generated on 2021-12-13T10:38:02
tokens_values = [
    # Mainnet
    {
        "Celo native asset (CELO)": "0x471ece3750da237f93b8e339c536989b8978a438",
        "Celo Dollar (cUSD)": "0x765de816845861e75a25fca122bb6898b8b1282a",
        "softbalanced.com (USBL)": "0x774b29bf6017bb36e7b7595e8dfde19de6d4c583",
        "PT Stake Ticket (PUSD)": "0xa45ba19df569d536251ce65dd3120bf7873e14ec",
        "Cell Doge (CELLDOGE)": "0xa2f26a3af6a2c5bc1d64e176f3bb42bf6e9c6c61",
        "Ubeswap LP Token (ULP)": "0x5d3be0ef8f83d2df0782a89bdcd28978ed4dd13f",
        "PoofCash voucher for POOF tokens (vPOOF)": "0xba58308d1784e06615eddeb7c7a2340d64acaaf3",
        "Ubeswap (UBE)": "0x00be915b9dcf56a3cbe739d9b9c202ca692409ec",
        "Celo Euro (cEUR)": "0xd8763cba276a3738e6de85b4b3bf5fded6d6ca73",
        "Moola cUSD AToken (mCUSD)": "0x64defa3544c695db8c535d289d843a189aa26b98",
        "PT Stake EUR Ticket (pEUR)": "0xddbdbe029f9800f7c49764f15a1a1e55755648e4",
        "Mobius DAO Token (MOBI)": "0x73a210637f6f6b7005512677ba6b3c96bb4aa44b",
        "FaucetToken (FAU)": "0xc4fc619bba4572656606bc7fdc0874d481ebebde",
        "Moola cEUR MToken (mCEUR)": "0xa8d0e6799ff3fd19c6459bf02689ae09c4d78ba7",
        "KNX (KNX)": "0xa81d9a2d29373777e4082d588958678a6df5645c",
        "Truefeedback (TFBX)": "0x035ee610693a29cb77fd6efbeb9d9d278703e145",
        "SushiToken (SUSHI)": "0xd15ec721c2a896512ad29c671997dd68f9593226",
        "Moola (MOO)": "0x17700282592d6917f6a73d0bf8accf4d578c131e",
        "Moola CELO AToken (mCELO)": "0x7037f7296b2fc7908de7b57a89efaa8319f0c500",
        "Source (SOURCE)": "0x74c0c58b99b68cf16a717279ac2d056a34ba2bfe",
        "Wrapped Ether (WETH)": "0xe919f65739c26a42616b7b8eedc6b5524d1e3ac4",
        "Carve Network (Cave)": "0xd566269effd2d3a5e4c8fa93c976cab5fa99eb78",
        "Symmetric (SYMM)": "0x8427bd503dd3169ccc9aff7326c15258bc305478",
        "Interest Bearing cEUR (dcEUR)": "0x998ba352ad84cc0cd7e71b1cc11fd192d624254c",
        "USD Coin (USDC)": "0xcc82628f6a8defa1e2b0ad7ed448bef3647f7941",
        "PoofCash (POOF)": "0x00400fcbf0816bebb94654259de7273f4a05c762",
        "Dai Stablecoin (DAI)": "0xe4fe50cdd716522a56204352f00aa110f731932d",
        "Mobius cUSD/USDC LP (MobLP)": "0xd7bf6946b740930c60131044bd2f08787e1ddbd4",
        "CeloStarter (cStar)": "0x452ef5a4bd00796e62e5e5758548e0da6e8ccdf3",
        "Interest Bearing cUSD (dcUSD)": "0xb104422f2fbc050055671265b95e08ad6057b0b3",
        "Moola variable debt bearing mCUSD (variableDebtmCUSD)": "0xf602d9617564c07f1e128687798d8c699ced3961",
    },
    # Alfajores testnet
    {
        "Celo Dollar (cUSD)": "0x874069fa1eb16d44d622f2e0ca25eea172369bc1",
        "Celo Euro (cEUR)": "0x10c892a6ec43a53e45d0b916b4b7d383b1b78c0f",
    },
    # Baklava testnet
    {
        "Celo Dollar (cUSD)": "0x62492A644A588FD904270BeD06ad52B9abfEA1aE",
        "Celo Euro (cEUR)": "0xf9ecE301247aD2CE21894941830A2470f4E774ca",
    },
]

ledger_tokens = {}

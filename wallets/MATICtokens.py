# Uniblow ERC20 list for Polygon generated on 2021-12-13T10:38:02
tokens_values = [
    # Mainnet
    {
        "Wrapped MATIC (WMATIC)": "0x0d500b1d8e8ef31e21c99d1db9a6444d3adf1270",
        "Wrapped Ether (WETH)": "0x7ceb23fd6bc0add59e62ac25578270cff1b9f619",
        "BNB (BNB)": "0x3BA4c387f786bFEE076A58914F5Bd38d668B42c3",
        "Tether USD (USDT)": "0xc2132d05d31c914a87c6611c10748aeb04b58e8f",
        "USD Coin (USDC)": "0x2791bca1f2de4661ed88a30c99a7a9449aa84174",
        "Crypto.com Coin (CRO)": "0xada58df0f643d959c2a47c9d4d4c1a4defe3f11c",
        "Wrapped BTC (WBTC)": "0x1bfd67037b42cf73acf2047067bd4f2c47d9bfd6",
        "Binance Dollar (BUSD)": "0xdab529f40e671a1d4bf91361c21bf9f0c9712ab7",
        "ChainLink Token (LINK)": "0x53e0bca35ec356bd5dddfebbd1fc0fd03fabad39",
        "Dai Stablecoin (DAI)": "0x8f3cf7ad23cd3cadbd9735aff958023239c6a063",
        "Wrapped UST Token (UST)": "0x692597b009d13c4049a947cab2239b7d6517875f",
        "Uniswap (UNI)": "0xb33eaad8d922b1083446dc23f610c2567fb5180f",
        "Jarvis EUR (jEUR)": "0x4e3decbb3645551b8a19f0ea1678079fcb33fb4c",
        "Jarvis GBP (jGBP)": "0x767058F11800FBA6A682E73A6e79ec5eB74Fac8c",
        "Jarvis CHF (jCHF)": "0xbD1463F02f61676d53fd183C2B19282BFF93D099",
        "Jarvis SGD (jSGD)": "0xa926db7a4cc0cb1736d5ac60495ca8eb7214b503",
        "Jarvis PHP (jPHP)": "0x486880FB16408b47f928F472f57beC55AC6089d1",
        "Jarvis CAD (jCAD)": "0x8ca194A3b22077359b5732DE53373D4afC11DeE3",
        "Jarvis CAD (jAUD)": "0xCB7F1Ef7246D1497b985f7FC45A1A31F04346133",
        "Jarvis JPY (jJPY)": "0x8343091F2499FD4b6174A46D067A920a3b851FF9",
        "Jarvis BGN (jBGN)": "0x60e91fc3a60362ca44aea0263dbc4b96302f6ade",
        "Jarvis XAF (jXAF)": "0x98fab5d1366de24f152ea683a1f23338351c47ea",
        "Jarvis XOF (jXOF)": "0x4A1E068BC23fAeC08a5817A2A58258e3378d36f0",
        "SAND (SAND)": "0xBbba073C31bF03b8ACf7c28EF0738DeCF3695683",
        "Decentraland (MANA)": "0xa1c57f48f0deb89f569dfbe6e2b7f46d33606fd4",
        "Theta Token (THETA)": "0xb46e0ae620efd98516f49bb00263317096c114b2",
        "Fantom Token (FTM)": "0xc9c1c1c20b3658f8787cc2fd702267791f224ce1",
        "Bitfinex LEO Token (LEO)": "0x06d02e9d62a13fc76bb229373fb3bbbd1101d2fc",
        "Graph Token (GRT)": "0x5fe2b58c013d7601147dcdd68c143a77499f5531",
        "LoopringCoin V2 (LRC)": "0x84e1670f61347cdaed56dcc736fb990fbb47ddc1",
        "Amp (AMP)": "0x0621d647cecbfb64b79e44302c1933cb4f27054d",
        "EnjinCoin (ENJ)": "0x7ec26842f195c852fa843bb9f6d8b583a274a157",
        "Aave (AAVE)": "0xd6df932a45c0f255f85145f286ea0b292b21c90b",
        "Maker (MKR)": "0x6f7C932e7684666C9fd1d44527765433e01fF61d",
        "BAT (BAT)": "0x3cef98bb43d732e2f285ee605a8158cde967d219",
        "Jarvis Reward (JRT)": "0x596ebe76e2db4470966ea395b0d063ac6197a8c5",
        "Celsius (CEL)": "0xd85d1e945766fea5eda9103f918bd915fbca63e6",
        "HuobiToken (HT)": "0xfad65eb62a97ff5ed91b23afd039956aaca6e93b",
        "chiliZ (CHZ)": "0xf1938ce12400f9a761084e7a80d37e732a4da056",
        "HoloToken (HOT)": "0x0c51f415cf478f8d08c246a6c6ee180c5dc3a012",
        "Frax (FRAX)": "0x45c32fa6df82ead1e2ef74d17b76547eddfaff89",
        "CRV (CRV)": "0x172370d5cd63279efa6d502dab29171933a610af",
        "Nexo (NEXO)": "0x41b3966b4ff7b427969ddf5da3627d6aeae9a48e",
        "TrueUSD (TUSD)": "0x2e1ad108ff1d8c782fcbbb89aad783ac49586756",
        "Compound (COMP)": "0x8505b9d2254a7ae468c0e9dd10ccea3a837aef5c",
        "SushiToken (SUSHI)": "0x0b3f868e0be5597d5db7feb59e1cadbb0fdda50a",
        "Paxos Standard (PAX)": "0x6f3b3286fd86d8b47ec737ceb3d0d354cc657b3e",
        "IoTeX Network (IOTX)": "0xf6372cdb9c1d3674e83842e3800f2a62ac9f3c66",
        "1INCH Token (1INCH)": "0x9c2c5fd7b07e95ee044ddeba0e97a665f142394f",
        "Synthetix Network (SNX)": "0x50b728d8d964fd00c2d0aad81718b71311fef68a",
        "Livepeer Token (LPT)": "0x3962f4a0a0051dcce0be73a7e09cef5756736712",
        "OMG Network (OMG)": "0x62414d03084eeb269e18c970a21f45d2967f0170",
        "renBTC (renBTC)": "0xdbf31df14b66535af65aac99c32e9ea844e14501",
        "Rocket Pool (RPL)": "0x7205705771547cf79201111b4bd8aaf29467b9ec",
        "Gnosis (GNO)": "0x5ffd62d3c3ee2e81c00a7b9079fb248e7df024a8",
        "yearn.finance (YFI)": "0xda537104d6a5edd53c6fbba9a898708e465260b6",
        "Bancor (BNT)": "0xc26d47d5c33ac71ac5cf9f776d63ba292a4f7842",
        "ZRX (ZRX)": "0x5559edb74751a0ede9dea4dc23aee72cca6be3d5",
        "Telcoin (TEL)": "0xdf7837de1f2fa4631d716cf2502f8b230f1dcc32",
        "Render Token (RNDR)": "0x61299774020da444af134c82fa83e3810b309991",
        "Frax Share (FXS)": "0x1a3acf6d19267e2d3e7f898f42803e90c9219062",
        "Dogelon (ELON)": "0xe0339c80ffde91f3e20494df88d4206d86024cdf",
        "SwissBorg (CHSB)": "0x67ce67ec4fcd4aca0fcb738dd080b2a21ff69d75",
        "UMA Voting Token v1 (UMA)": "0x3066818837c5e6ed6601bd5a91b0762877a6b731",
        "Ocean Token (OCEAN)": "0x282d8efce846a88b159800bd4130ad77443fa1a1",
        "Serum (SRM)": "0x6bf2eb299e51fc5df30dec81d9445dde70e3f185",
        "Golem Network Token (GLM)": "0x0b220b82f3ea3b7f6d9a1d8ab58930c064a2b5bf",
        "Mask Network (MASK)": "0x2b9e7ccdf0f4e5b24757c1e1a80e311e34cb10c7",
        "Klima DAO (KLIMA)": "0x4e78011ce80ee02d2c3e649fb657e45898257815",
        "XY Oracle (XYO)": "0xd2507e7b5794179380673870d88b22f94da6abe0",
        "Energy Web Token Bridged (EWTB)": "0x43e4b063f96c33f0433863a927f5bad34bb4b03d",
        "Wootrade Network (WOO)": "0x1b815d120b3ef02039ee11dc2d33de7aa4a8c603",
        "Injective Token (INJ)": "0x4e8dc2149eac3f3def36b1c281ea466338249371",
        "Fetch (FET)": "0x7583feddbcefa813dc18259940f76a02710a8905",
        "KEEP Token (KEEP)": "0x42f37a1296b2981f7c3caced84c5096b2eb0c72c",
        "Paxos Gold (PAXG)": "0x553d3d295e0f695b9228246232edf400ed3560b5",
        "HUSD (HUSD)": "0x2088c47fc0c78356c622f79dba4cbe1ccfa84a91",
        "Swipe (SXP)": "0x6abb753c1893194de4a83c6e8b4eadfc105fd5f5",
        "Polymath (POLY)": "0xcb059c5573646047d6d88dddb87b745c18161d3b",
        "OriginToken (OGN)": "0xa63beffd33ab3a2efd92a39a7d2361cee14ceba8",
        "Orbs (ORBS)": "0x614389eaae0a6821dc49062d56bda3d9d45fa2ff",
        "UniBright (UBT)": "0x7fbc10850cae055b27039af31bd258430e714c62",
        "Gemini dollar (GUSD)": "0xc8a94a3d3d2dabc3c1caffffdca6a7543c3e3e65",
        "Orchid (OXT)": "0x9880e3dda13c8e7d4804691a45160102d31f6060",
        "RLC (RLC)": "0xbe662058e00849c3eef2ac9664f37fefdf2cdbfe",
        "BandToken (BAND)": "0xa8b1e0764f85f53dfe21760e8afe5446d82606ac",
        "PowerLedger (POWR)": "0x0aab8dc887d34f00d50e19aee48371a941390d14",
        "SingularityNET Token (AGIX)": "0x190eb8a183d22a4bdf278c6791b152228857c033",
        "Ankr Eth2 Reward Bearing Certificate (aETHc)": "0xc4e82ba0fe6763cbe5e9cbca0ba7cbd6f91c6018",
        "FEGtoken (FEG)": "0xf391f574c63d9b8764b7a1f56d6383762e07b75b",
        "Aavegotchi GHST Token (GHST)": "0x385eeac5cb85a38a9a07a70c73e0a3271cfb54a7",
        "ETH 2x Flexible Leverage Index (ETH2x-FLI)": "0x66d7fdcc7403f18cae9b0e2e8385649d2acbc12a",
        "Balancer (BAL)": "0x9a71012b13ca4d3d0cdc72a177df3ef03b0e76a3",
        "Gitcoin (GTC)": "0xdb95f9188479575f3f718a245eca1b3bf74567ec",
        "Kyber Network Crystal v2 (KNC)": "0x1c954e8fe737f99f68fa1ccda3e51ebdb291948c",
        "Everipedia IQ (IQ)": "0xb9638272ad6998708de56bbc0a290a1de534a578",
        "IceToken (ICE)": "0xdf00c50a3dae240860f57b77508203b8d9593283",
        "Automata (ATA)": "0x0df0f72ee0e5c9b7ca761ecec42754992b2da5bf",
        "beefy.finance (BIFI)": "0xfbdd194376de19a88118e84e279b977f165d01b8",
        "STASIS EURS Token (EURS)": "0xe111178a87a3bff0c8d18decba5798827539ae99",
        "Synth sUSD (sUSD)": "0xf81b4bec6ca8f9fe7be01ca734f55b2b6e03a7a0",
        "bZx Protocol Token (BZRX)": "0x54cfe73f2c7d0c4b62ab869b473f5512dc0944d2",
        "AIOZ Network (AIOZ)": "0xe2341718c6c0cbfa8e6686102dd8fbf4047a9e9b",
        "STAKE (STAKE)": "0xeb5c9e515629b725d3588a55e2a43964dcfde8ca",
        "LCX (LCX)": "0xe8a51d0dd1b4525189dda2187f90ddf0932b5482",
        "EthLend (LEND)": "0x313d009888329c9d1cf4f75ca3f32566335bd604",
        "Ampleforth Governance (FORTH)": "0x5ecba59dacc1adc5bdea35f38a732823fc3de977",
        "PAID Network (PAID)": "0xeAEf6cAf6d5894EDB2D4EA7ec11eC4AB655f1cBF",
        "Mainframe Token (MFT)": "0x91ca694d2b293f70fe722fba7d8a5259188959c3",
        "Quickswap (QUICK)": "0x831753dd7087cac61ab5644b308642cc1c33dc13",
        "Litentry (LIT)": "0xe6e320b7bb22018d6ca1f4d8cea1365ef5d25ced",
        "Decentral Games Governance (xDG)": "0xc6480da81151b2277761024599e8db2ad4c388c8",
        "FOX (FOX)": "0x65a05db8322701724c197af82c9cae41195b0aa8",
        "Rarible (RARI)": "0x780053837ce2ceead2a90d9151aa21fc89ed49c2",
    },
    # Testnet
    {
        "Plasma (TST)": "0x2d7882beDcbfDDce29Ba99965dd3cdF7fcB10A1e",
        "Dummy (DERC)": "0xfe4F5145f6e09952a5ba9e956ED0C25e3Fa4c7F1",
    },
]

# From Ledger trusted tokens data
# extracted on 2022-01-18
# from https://github.com/LedgerHQ/ledgerjs/raw/master/packages/cryptoassets/data/polygon-erc20.js
ledger_tokens = {
    "0x8505b9d2254a7ae468c0e9dd10ccea3a837aef5c": {
        "ticker": "COMP",
        "signature": "3045022100bc6c39d0831aabe4207836ccfef88f0331dfc8b764bc4980f6bfe0fd2546c5cf02201636c8b88bb165998f86ff398a3da68e9557b3ad6f6d7f33e99b162ba7a7b201",
    },
    "0x8f3cf7ad23cd3cadbd9735aff958023239c6a063": {
        "ticker": "DAI",
        "signature": "3045022100b74ee418eb7eef1cde0876e9589a6ba217a3318ad840bb797c68adb4ecc57bd4022014fc46f93500576aca05034e243d12f4a28e56b81a3606d4c99d617ebebadaf5",
    },
    "0xc2132d05d31c914a87c6611c10748aeb04b58e8f": {
        "ticker": "USDT",
        "signature": "3045022100a9588096900e77f2989386c862582b225e587ba063401e53e953b94d55a04698022033fa8853df43d6ca84b10561aff0d9d804a2d0a49684cdf084a86732e03209b7",
    },
    "0x1bfd67037b42cf73acf2047067bd4f2c47d9bfd6": {
        "ticker": "WBTC",
        "signature": "3045022100839e22224fc6ece318210c2cd6508d4e4e428c718e36a44b1c3590796dd4fdbb02202e86a2e423132e2fd0e2aabb580b1e06112c7de74f540e17e4042849877abcce",
    },
    "0xda537104d6a5edd53c6fbba9a898708e465260b6": {
        "ticker": "YFI",
        "signature": "304402206d3c20aa3d59d2e41123c6a4d9f497b7af19b7d2a40a8149669debc6b3adbea0022043d30baa1b9ec44cf11d7b6b4f820bed89518032aa40a8137c6b2ca8643f794f",
    },
    "0x71b821aa52a49f32eed535fca6eb5aa130085978": {
        "ticker": "0XBTC",
        "signature": "3045022100acea17a49b701f45bf61999c920f223ebc1a65334ae4b109cef9e38a01bfa0e302207c8703d468b66e523b6c568aa0a57de437362dfd54e0d282b7029fead9fd5e7b",
    },
    "0x9c2c5fd7b07e95ee044ddeba0e97a665f142394f": {
        "ticker": "1INCH",
        "signature": "3045022100e379fb6706bb16d8f39ed26a4f0c7e105a3fa1b50e01176d2702b6aba2209fa502205c347dd1fe3a1447a10ced5de9b58a01661d2381e4f18ef36dc51a03dcbb02e1",
    },
    "0xd6df932a45c0f255f85145f286ea0b292b21c90b": {
        "ticker": "AAVE",
        "signature": "30440220456a4411c8ca81b689f3f80ee2b198a754f8795194966b596469abef607cb34b02207a1543fcf5ceadd67a2fd87b57a71cddd74c3fba0a5973a765ac5b25eb7691d6",
    },
    "0x1d2a0e5ec8e5bbdca5cb219e649b565d8e5c3360": {
        "ticker": "AMAAVE",
        "signature": "304402203b79fc40f823123941b2e22262f7ca5492f93c24c634bcfab3381f07b8190e230220367863887615b626da04e149c1ffc4db11dcb72792d0776588d3bbbcd3c0c656",
    },
    "0x27f8d03b3a2196956ed754badc28d73be8830a6e": {
        "ticker": "AMDAI",
        "signature": "3045022100ddb49a516633e80020dd6f0c7657e9034ac973e41b0b85ac10c23e530362182a02205354bfd224ed2e7e02d88ad6119a640735b760e089f591684929657ec398090b",
    },
    "0x1a13f4ca1d028320a707d99520abfefca3998b7f": {
        "ticker": "AMUSDC",
        "signature": "3044022007f7b18fba725f141716377f7e44ee6ca8695dd7d0b36a6282521cd0677f25a2022033e28b25305f736c5c62ed798b22da0d9d3a6a1c9f115c32f923da5c0891fe86",
    },
    "0x60d55f02a771d515e077c9c2403a1ef324885cec": {
        "ticker": "AMUSDT",
        "signature": "304402201c2a034690a7d69c8fb844856b1d6a891beeebfc6ebc978b02ca62b80b02f11e02206e3b98e8b3f62722553134571b3ff1abc4331ecd05164ad577ccabe9b7183376",
    },
    "0x5c2ed810328349100a66b82b78a1791b101c9d61": {
        "ticker": "AMWBTC",
        "signature": "3045022100a8d864178f5f332844e5b8945461efca72f4f4e8f2d82073cd53ae5e65460a490220205ddf05d10acbba4e663eb4dc191b67b06829340c903c4449b6935c2c6cf017",
    },
    "0x28424507fefb6f7f8e9d3860f56504e4e5f5f390": {
        "ticker": "AMWETH",
        "signature": "3044022061b6b779525e3df37e6d8e40da163b3edca2f90cf5dbc1e4b6e52f0c956bbcbe022035e3069875ab5c79b7d65efa57e46973d00252cdc40a5deff3460f5e02e597b3",
    },
    "0x8df3aad3a84da6b69a4da8aec3ea40d9091b2ac4": {
        "ticker": "AMWMATIC",
        "signature": "304402205a27811f2e1837d46fa0179ed90ec344bbf5c6af49c8dca044a9b4ae754742c502200e2bc57d5260bdc4171b4443c598b6e524915fb7c3021170086664763721c0fd",
    },
    "0xc3fdbadc7c795ef1d6ba111e06ff8f16a20ea539": {
        "ticker": "ADDY",
        "signature": "3044022058ba00c91c27c8a7073ffeb67cf705f3d7727d34452c5304ac0f0cab3ce428c302206ee94a0e832239d7c4fe771f7fe25acf688881889a64b132f55d6b1a2f2718f5",
    },
    "0xe2341718c6c0cbfa8e6686102dd8fbf4047a9e9b": {
        "ticker": "AIOZ",
        "signature": "3045022100d7327187553ba84adc1bc9284238ac7c0068e5d4c17233a1e512eaa4033aee700220631bda671092d9a8996cd44ae39347ba0f3fb0c5d0df655d1dca6b914b4026b1",
    },
    "0x0169ec1f8f639b32eec6d923e24c2a2ff45b9dd6": {
        "ticker": "ALGB",
        "signature": "3045022100ffc9b269048fe1268ea09a1eeda0a21f7a9b70c1013ad9e61d47f21950a73773022027caef903f25f2fc8fbe655b764566a988226a6a31db39b6fba999b3636ce7ed",
    },
    "0xa8fcee762642f156b5d757b6fabc36e06b6d4a1a": {
        "ticker": "ALN",
        "signature": "3045022100ea8ba091c2a7cb77a4154ee6295cef650f31e3efffbb25617239f5a8d4c6d3d902207c1f5cb6d78051a01103f276e4d3d5016ed307eb604e27a6eb9c9fb27fed2e67",
    },
    "0x0621d647cecbfb64b79e44302c1933cb4f27054d": {
        "ticker": "AMP",
        "signature": "3045022100de0b2a62ac2b686b590e16f5ad304a55fbbb942cd8f1fdf143163a7bfa77cea0022029c0819b4bcd42965b7b44c3e4fa459ef52cdb6933facdae09e3c5244af068f3",
    },
    "0x5ecba59dacc1adc5bdea35f38a732823fc3de977": {
        "ticker": "FORTH",
        "signature": "3045022100c5645d780a9d15b5ff91593ac851a778f0d639fa140b63a53a7ddb4146a2739202206ccce793ea9cc19c944fb55974788b13a494a95803ad736ee24fd3bb01f6ceb9",
    },
    "0xc4e82ba0fe6763cbe5e9cbca0ba7cbd6f91c6018": {
        "ticker": "AETHC",
        "signature": "304402205626e4fc42d2da26e30d6df185804db141f95ba0bc9554a693b694d85c82539a02204a1833c7c05590c71326c02f90c57aa39db4d4221f0e1e5e9f4fcfecbd3d980b",
    },
    "0x5de4005155933c0e1612ce808f12b4cd8daabc82": {
        "ticker": "ARMOR",
        "signature": "3045022100af076c9ab6176715c9361059a33de9d83d7bb33a7c44487e638968fe2d3a099402206c7e70ba8d1bb5e8c0e52dfae1ca44d4c39b8af04c60fca94828d98396076805",
    },
    "0x76e63a3e7ba1e2e61d3da86a87479f983de89a7e": {
        "ticker": "OMEN",
        "signature": "3045022100d21d6e7fd83bc5940537b858bc6c6acecae970e8b13501bf25e2b12acb022f1e02201965c2d4f24945dead8914dc2b34f2483f3b9040799f3d17b40dcad3339243aa",
    },
    "0xcde5e3d90631cacb842fa86f62dd0fc08a3ce43d": {
        "ticker": "BAC",
        "signature": "30440220530fab88d00262cce862e93dfa18f0390b70451d73b2ee8ad204b35403fbd985022009075a365756e0e12a8b189d02b7c136459afe05f7d8ecc6afc8654a0a2ccd33",
    },
    "0x9a71012b13ca4d3d0cdc72a177df3ef03b0e76a3": {
        "ticker": "BAL",
        "signature": "3044022065dd446f0899eaf96b76e325fcdbb44011aa2cc74c97c8efeaefcba268c8ef88022004b20ead8b2627dfdb27295fd1549390205ffdfb101259c5e52049012e1b5516",
    },
    "0xc26d47d5c33ac71ac5cf9f776d63ba292a4f7842": {
        "ticker": "BNT",
        "signature": "304402204b1b00e8d2b713b42b1cac76c4977ba8b327da2e7edaeacdb208061dd28b214202204d3e78d072bfb6bb11a342f4764444bdee2cd5b1cb4e748f0425571dd7021fc9",
    },
    "0xa8b1e0764f85f53dfe21760e8afe5446d82606ac": {
        "ticker": "BAND",
        "signature": "3044022075f96cea4bb8035072670571807593f8ef1ea06ab86aef4ad07ca3bee21ad1df022076c94c82bee2509c4fe0a7c9db819188ba3805567a9ecca3c98caac8740291e4",
    },
    "0x3cef98bb43d732e2f285ee605a8158cde967d219": {
        "ticker": "BAT",
        "signature": "3045022100cf93e17a4073c83ddab8b64e01c0c64d6c2cb96a87c3a84a537bc1905933762902205bf6f9e94ee53d75e7c3ee7381703eff8131befbedfd72ec6cbc60d2ff942009",
    },
    "0xfbdd194376de19a88118e84e279b977f165d01b8": {
        "ticker": "BIFI",
        "signature": "3045022100be4c0e7ec00fa9fd985b145dea6b86f89ea3284a17912b8de1c1238910f69ad402207b62b18577fc9d8acf3927f6fc3c109ca76eb66366e389e4a86d93d826211db7",
    },
    "0x07cc1cc3628cc1615120df781ef9fc8ec2feae09": {
        "ticker": "BEPRO",
        "signature": "3045022100db4b16fb793cdfdd83067cbdff8cf6cc0552cb159ee8e0187174bef6fa2224f4022011ca25280fa0eff139fec6e0c76628258d319ff0b37abe24147c623751ea26a4",
    },
    "0xdab529f40e671a1d4bf91361c21bf9f0c9712ab7": {
        "ticker": "BUSD",
        "signature": "30440220306d65bd1bf54b51405a55dcea267903367f453aae13e7216f6cebb23dcbb6e602203c279f533074b262ca907f0c1c7444ba9cd244154f3d655e03b7383dd346d07c",
    },
    "0x06d02e9d62a13fc76bb229373fb3bbbd1101d2fc": {
        "ticker": "LEO",
        "signature": "3044022054e6d7c9f16d4a6128edd523e016180098c29c0585bbe48ea410254e681952a502207c5a3860f2f295f2b07171b2e0724a3e806d654a378e08baedc422f5c7bd865d",
    },
    "0x6863bd30c9e313b264657b107352ba246f8af8e0": {
        "ticker": "BPT",
        "signature": "304402207f74e8cc4b32aafa132655f8da80366dce0d7ec60ba53eb595d9ada6388609d502200d12abff69a31fa70ea9afa896cc3b0065673b6be884ca2d87d226417f115076",
    },
    "0xf1c1a3c2481a3a8a3f173a9ab5ade275292a6fa3": {
        "ticker": "VEE",
        "signature": "3045022100d773ab7c886b3a27979a1bbbb8e9f74d4859c533304092ea8ce81de559be0c96022002fefc041758c7b9ffbf6870c876af5ba66f8ebc86af37552fa5562186c8c474",
    },
    "0x229b1b6c23ff8953d663c4cbb519717e323a0a84": {
        "ticker": "BLOK",
        "signature": "3045022100c163912ceda6b2f5ae6c7adf21f46dc85d282a914e08fba485fd24cef0af5274022072824adf7a17b1f200710c1f052bf3dce11a0eeed1c59faca05acf3c9e55574a",
    },
    "0x438b28c5aa5f00a817b7def7ce2fb3d5d1970974": {
        "ticker": "BLZ",
        "signature": "304402203b0e1be78acac09c1f4d661dd3dad5c291c2b0c107531be52687d1834ff9afd50220443f3ab75a8337c7d3cdaf857ef35cce24f0bbc82d83cea08713ca64c8346df8",
    },
    "0x3ba4c387f786bfee076a58914f5bd38d668b42c3": {
        "ticker": "BNB",
        "signature": "3044022076d39107a36eaccd897219063fa00eed78903b739ff83bad07990d3f2122b81902203916c2533447e0b7bbfabb45e1dfce2440c664ec29500a6f4e571bf0f0b617a2",
    },
    "0x80244c2441779361f35803b8c711c6c8fc6054a3": {
        "ticker": "BONE",
        "signature": "304402203a15aaeced1965f8a9d19a1ee716108e1c73f1f5705d766bdbd3490e7471d18402205cc59469cdf7e431eab6a0ab7f0ba4518f487d5a8216e7130b5ae496bac6c304",
    },
    "0xff88434e29d1e2333ad6baa08d358b436196da6b": {
        "ticker": "BORING",
        "signature": "30450221009c9cac09e7e27fd6b64bd3627851b4e4d320aee8cb53b811b8574320cd0c885102207d239b7d1bc3d685fcb3bceb1ae8e6eae208f07ad5e315509043620a3e4ed75d",
    },
    "0x0f40d69ff494a9e74b1f20a89eff299b2a05918f": {
        "ticker": "BNTY",
        "signature": "304402206bb140450d162c3bf97a049dd4af1e09b9f245e88b79ec80896ddbbc321d707202200868a516eb569065c5158fc6214b56e70fb0aba000f42ff6e5c15f0e6bae7e6f",
    },
    "0x4b54bc363f5f9c6e0fcd82eac6919ae213464cc6": {
        "ticker": "BTC2X-FLI",
        "signature": "30440220260fe077037f2ea7283410a03b93ec0880fa4ea14e55ee5c36e0c017caa72fe4022059ecd804021753fb1fbe928c16f825b44654454c7d1d5d775dba8de0eea7491d",
    },
    "0xfdc26cda2d2440d0e83cd1dee8e8be48405806dc": {
        "ticker": "BTU",
        "signature": "304402200e7b49c9063a554ae4ce257e36df3da4afc6ce7ffc99d8c2c02419381475fe61022027aae3d6973f9cfd77ceebfd77e3fdcbb51f2d148a6801724d51b626f1cd7e3f",
    },
    "0x54cfe73f2c7d0c4b62ab869b473f5512dc0944d2": {
        "ticker": "BZRX",
        "signature": "304402206fa371d32fe05821a0bb28306c3f3e2f0cf2f1a6efec349bcef67894575459c8022025e11ef31b7be14dd464c05237267105df54691f27ed50a6d49c64f1d798f0b4",
    },
    "0x3a4bed49ab592a227bae8f41efd0d38e6e06d96c": {
        "ticker": "CARD",
        "signature": "30440220692705b2b40824a4e54ea19f3c25d012bf770fbfceddb6dbad50b7d18c31193b02207df69bfea34b27d87a075503dcbc3fe5dc1c1274d80a95ab487dc5e406414aac",
    },
    "0xf2ae0038696774d65e67892c9d301c5f2cbbda58": {
        "ticker": "CXO",
        "signature": "30440220306051149039bf55bf4c3f68194216508b514becc225db631b1140470399a59c022046dd1317f9ccd33c076fcc3d9222373b548a3754bf9339b8f64a45e62fce541d",
    },
    "0xd85d1e945766fea5eda9103f918bd915fbca63e6": {
        "ticker": "CEL",
        "signature": "3044022073bf3843575fd64633ca39a68160cad26a12c1a6714e5c0ef3464ccf564c7e9802202f40c3359d904bd7623e41f374ec0e1aa34451239cf5468ff1f75a2eb25a22f4",
    },
    "0xdef1fac7bf08f173d286bbbdcbeeade695129840": {
        "ticker": "CERBY",
        "signature": "304402200f2ea2ed631fff9298f1293cc976c4c27ad2af9f1de804d54ee700379d3e2ebb02201f6bbb1f7765f7aa60592e1c390449014a86eadc53ba9b03577bde85a6cc150b",
    },
    "0x53e0bca35ec356bd5dddfebbd1fc0fd03fabad39": {
        "ticker": "LINK",
        "signature": "304402207253a5dc3dd9edd9a3fbae8779c20bfff5e3853a6ee46d76903c4c27f7cc521502204f3355c7ff2ce78fd5ba38b13037cafd481a5c058635e649588c8e42eb5e36f7",
    },
    "0xf1938ce12400f9a761084e7a80d37e732a4da056": {
        "ticker": "CHZ",
        "signature": "3045022100e2b143261ef36b6118d1d1ed78dd4c7b93bf7b781a7dcbf4775533c1dd5b3af30220187790828d395732ab555173bfd676c9a6044bc8f48221a0338d5f3209f47e93",
    },
    "0x8ba941b64901e306667a287a370f145d98811096": {
        "ticker": "CTI",
        "signature": "3044022004499c41cb7abd51c9aa9f92f1e9284c16f6cd717cc05db775b0425a535517e90220723894c31d8c429c2a4244b698d6cc9adc3b255329e9895aa217282b96e519e1",
    },
    "0xfa3c05c2023918a4324fde7163591fe6bebd1692": {
        "ticker": "XCRE",
        "signature": "30450221009d09ae90abde22b61ab29b2e4d2e5942af655b59af035c37b73a60c1e68da28202207b70156fbdc3f85f0d1a87c855cfab5f7e51888b21aeda77b78d68e0c9169869",
    },
    "0x172370d5cd63279efa6d502dab29171933a610af": {
        "ticker": "CRV",
        "signature": "3045022100f379c74676f646ea2c59b1ad896ebb81ddee85db0f1a7cd9163251a49f31367c0220466c4cbe449cc63e5a94b262aa7bcd6c8f6cfd99ef1fcc356f9f597ee4148b56",
    },
    "0xd1e6354fb05bf72a8909266203dab80947dceccf": {
        "ticker": "CNT",
        "signature": "304402201e1bf3b5cff585b6d0ac0a92f55ee0c01130a08c5c07caa4969310c1fa300a1a02203347f607a4e38df75289ceffa07470356e079930fa2c54371c31954a8b0d4dd2",
    },
    "0xada58df0f643d959c2a47c9d4d4c1a4defe3f11c": {
        "ticker": "CRO",
        "signature": "3044022016fb0d88052730549a29a2164ae51a5ae21b750f9906e0a5210510d66f4aba550220700aa5c692177bb2bd88f47c6c392e401edba868cc7d44de9a31c4b1d96fc5f2",
    },
    "0x23f07a1c03e7c6d0c88e0e05e79b6e3511073fd5": {
        "ticker": "CDS",
        "signature": "304402207418870351d4a4cca44b1441db2b321f034a52b2c2434aa90e3183995daa2489022050747189f082a03629f4e7450a92cdf62ed74f5155356732868d8552dd4303f5",
    },
    "0x314338767151e6ed23db8a41c6f6943c1958ee56": {
        "ticker": "DEC",
        "signature": "3043022011d7b66a4a5ff05f0fb26a4757a203386d4391989b394635c6e9555d60050434021f09cf35aa21e77fd650c78054ca528f663804b005b8cec5806f72c5f92b1a2a",
    },
    "0x2a93172c8dccbfbc60a39d56183b7279a2f647b4": {
        "ticker": "$DG",
        "signature": "3044022055528abd7fd74f4689271456bfdeed4fdbddf93c40b726d95fbcb3952173e8a9022007a30afdaeabb781232958bb8846000703b6f470c907366a8cea510796225c67",
    },
    "0xa1c57f48f0deb89f569dfbe6e2b7f46d33606fd4": {
        "ticker": "MANA",
        "signature": "30440220436699eef62d0e27cde881617e2bcc77e99fee8ddf20f7fd716bdfb49a2d5699022073b43e4d889526c69e07a87d377fc401986c2f620287c98396ee7e449e82062d",
    },
    "0xb35fcbcf1fd489fce02ee146599e893fdcdc60e6": {
        "ticker": "DERC",
        "signature": "3045022100f7b8da95f5ef57071eaebe38cca8dec6e843547de0c595f80235b6c28b681a2d0220088b5f2ca301ef392049d312723dad859abbbfaf783a8dcf71372674642f479e",
    },
    "0x3d1d2afd191b165d140e3e8329e634665ffb0e5e": {
        "ticker": "DERI",
        "signature": "3044022051e056f54d588094f90a33f8407931b7a93c6c2f36edd9ae1bfe8995339fe69f022061993acf220da2c692f13eaa075e8e77e6195d495704c45df7c0c075571f3cdb",
    },
    "0xde5ed76e7c05ec5e4572cfc88d1acea165109e44": {
        "ticker": "DEUS",
        "signature": "3045022100d7908e152e7416bf5e31981e15bd108c77387bf592f2f8c56be4b681798a99bc022067729913b3d6e419519faa9a95f6e6ddc79969e8d44da712b30ef2867a1a3b8d",
    },
    "0xe7804d91dfcde7f776c90043e03eaa6df87e6395": {
        "ticker": "DFX",
        "signature": "3045022100f4a521690a92991e537c140c01e8e55a975b4e2ef6e66b4c31b4ba7897874df7022030dec285f9009065db7f294953aa1f4ae615331387024f2fc2092162e669f04f",
    },
    "0xc168e40227e4ebd8c1cae80f7a55a4f0e6d66c97": {
        "ticker": "DFYN",
        "signature": "304402204903b3c6afa507aef4cd88fb080d92a0b55a5b230c987b1add3493736db56e2302202e4b03fc70c0c026096556ef44fdfcbb48a43b9bc6ceef324dfe1122e868db57",
    },
    "0x8c92e38eca8210f4fcbf17f0951b198dd7668292": {
        "ticker": "DHT",
        "signature": "304402206da3cefa573162edfe52a66dd7418c6f5014647cc970879a94ff83200def0bbf022043a610d9a386092babadd87b3aafb82c919d3631c19a89310cc10d16c3e11051",
    },
    "0xe0339c80ffde91f3e20494df88d4206d86024cdf": {
        "ticker": "ELON",
        "signature": "304402204b0eb245902417b53d92071ea313cf7fbf4423bcc554c8a317676e35c12b964b0220078a990d4cbf28bd53a0a7a64252f8f074f678e6d6af10e8740faa577ebeeac7",
    },
    "0x0e0ffc562d72316b783e887bbaae1fd794adb530": {
        "ticker": "DOV",
        "signature": "3045022100f367c43817b181ca597d3eb32297cab50f90973ce3dda7857d6f663a74a514f40220055e8d0606c5d36b5c25c62c7d70cf8bb009bf8a8ab101612424d0ca3859574d",
    },
    "0xf28164a485b0b2c90639e47b0f377b4a438a16b1": {
        "ticker": "DQUICK",
        "signature": "3045022100ce96a6d8016442db7cb288d06e9df774d8615f40c4a8cd90c639c665d1f1bc5b02207acb92af880448671857d8a43508ac8aed68eb809c5ce2ae9a449bb6dd0bc930",
    },
    "0xa0e390e9cea0d0e8cd40048ced9fa9ea10d71639": {
        "ticker": "DSLA",
        "signature": "3044022029ae2e5d5f851834c388141564c60a9d26c53bc81e9c6bf887f1206a86371b8f022075b60ffdf098243d8feab6ec0f55eca8c1722c1d7bf9a4c27cfcd44c439b557a",
    },
    "0xd9e838dd60c8ea1e7dd4e670913323bb87db112c": {
        "ticker": "EWT",
        "signature": "304502210090dd86bd0463a48c3b947b1e3bc08ebf3631202c676a857c5fc10525c8b7a65e02207b80b742250dcab35088fa81fc150ff998e3c070cb0bb455c11425e56ddcca52",
    },
    "0x1ac417f1dd23504aac2076d854581f03196403fd": {
        "ticker": "EDR",
        "signature": "3044022007b3f74047928a24b8bffc08998b542d2631e354788be568e669bbe402eb474902201660b60dedbded95b83607adbad5f20a6e6f83a5c3070ef7c932b8b49b417249",
    },
    "0x43e4b063f96c33f0433863a927f5bad34bb4b03d": {
        "ticker": "EWTB",
        "signature": "30440220765fbb44728292f2fbc4c486ad1e12cd657d9772328bda64a1f64d8258a3bea302202042c8bc7cca6c63f55b4e596ccd3ac0279901a0fd8f7a0d0173fea7f2e8d5f3",
    },
    "0x7ec26842f195c852fa843bb9f6d8b583a274a157": {
        "ticker": "ENJ",
        "signature": "304402205eb28eb20df347c98e9d337feadd95b1cf96a6eeec81d65e607b2edc84e0a96c022011a4215ea48f0675496059434cc2f78bc1cfaf99049476081434a180e05700c6",
    },
    "0x66d7fdcc7403f18cae9b0e2e8385649d2acbc12a": {
        "ticker": "ETH2X-FLI",
        "signature": "3044022017c1a8e6222874f80536e4ac34a1355de892dd5ce3dae95e7de66e9831fc90d30220323bf6e8165a353c3c2f2e708109c60bfda10b0a79930a219e94982fbdcfdb41",
    },
    "0xe7f960685aba7327ada3b161ea252703c87be0bf": {
        "ticker": "BLUE",
        "signature": "3045022100ad25b148a6136b4e931687ca0985728c855b59edbbb667856f0fd2a25d8f6047022025783a6bc2c6d17e055788d5a9655edc48bce0013fc9137ac6549ce794452754",
    },
    "0xd6a5ab46ead26f49b03bbb1f9eb1ad5c1767974a": {
        "ticker": "EMON",
        "signature": "3044022059c22f354a0e18a98798e7b3e924cedae2ac0ca9317aa311386b6d5744248afd02200a391ead7e74393423f1e6bd07b6d8464372de55adcf8013bd22db60315211c0",
    },
    "0x313d009888329c9d1cf4f75ca3f32566335bd604": {
        "ticker": "LEND",
        "signature": "304502210080a16f1f74224b502aa6cca21ece946463fb6d3363037e7e15c09dd10c3515ec022002342a8eb6cd1c876874818336ffa04f796bfa511ddd19d573fc2d0eff048d18",
    },
    "0xb9638272ad6998708de56bbc0a290a1de534a578": {
        "ticker": "IQ",
        "signature": "3045022100e1427ef7323b3f37d465fb9689fb018dec757aa8366b8e343dae3ba2c83410d6022013c549d42ad32760d28babf415b5b7fb7d212fbc4c6565c197960bf807e85ae3",
    },
    "0xc9c1c1c20b3658f8787cc2fd702267791f224ce1": {
        "ticker": "FTM",
        "signature": "3045022100e53adaa3478dafe8a78e80d3ac0f1368a22e3b57457b006f341fee784b95255e02200e06fa79671d4a54f3eb4185e79032422d75612dba1433f709971f252c13b302",
    },
    "0xf391f574c63d9b8764b7a1f56d6383762e07b75b": {
        "ticker": "FEG",
        "signature": "30440220723ec9773b3cc9e6c3ce465c2b06acdb0b3382c445ff3ad263000e370219936c02204453a1c7ba6b266535f8651af12ad969a7744170f1ecd942a4263afec69b51cc",
    },
    "0x7583feddbcefa813dc18259940f76a02710a8905": {
        "ticker": "FET",
        "signature": "3045022100f462e081f273bb1ae69168d64d538ec202cffb609c7bfd306d12297e84825f1902207e82b440a4deeddf67292873c416ea065ef9663961a3ebfda099538f75d3c1d9",
    },
    "0x3a3df212b7aa91aa0402b9035b098891d276572b": {
        "ticker": "FISH",
        "signature": "3045022100b5c3f2cc9c6292ef20bac4d17e9e2cb4047a478cf75354817dd1fb6ac3fc7edd02202eba60971eb6987b3ee658c637cf905ec995600b3104beb7d97e03cb71010c90",
    },
    "0x5f0197ba06860dac7e31258bdf749f92b6a636d4": {
        "ticker": "1FLR",
        "signature": "304402200a1e2ddbfbc9d4e948fa507ed54bc72d38d64c1ab9885527734a8c8836dc016b022062afbe992e01cf401a3c914e958983c55fb357db3228bde2c17771793ed3f3cf",
    },
    "0x66e16d50c07a01bb473ec794349d45aa1a0e5dc2": {
        "ticker": "FOAM",
        "signature": "3045022100b66fe2c72a4073cad992f5c1f345fb1d4f057b79a4097fb913238e1e3f433785022006f6059384a3fd45623b8bdbd2514f3cebed6343d2548f3f34a2b5a2be39ebf9",
    },
    "0x65a05db8322701724c197af82c9cae41195b0aa8": {
        "ticker": "FOX",
        "signature": "30440220344a0fcdbff02568d465b09e2cffc047eb9169f9f89243cad21d20f78ab5749602206cee295f9945b6f10f984541b6975df9397edf35a3c9a0feba5d93fed87b04e0",
    },
    "0x6f934b45fc6043527d73c7c99313dd838e723db7": {
        "ticker": "FCL",
        "signature": "3045022100eb5ba2c5be243b68d60ae5c7295b79c491fe8b358d91186a81be9049136bc463022034399e2223bd6718aa718c5e29abee9f7c8035fb369ab50eb05f1856d852cc68",
    },
    "0x7cef6ed1e07079e174601d39066ad0856cb47988": {
        "ticker": "FREE",
        "signature": "304402201b40be30f6cd23cee9a6aa7330cfd204e7ee5147bb54fbd62654ddddb7c66a160220250ffc02afb80d706ddb5be1ada48bd924df48adb4e43a4ef1ccad6c8706d5f9",
    },
    "0xa3ed22eee92a3872709823a6970069e12a4540eb": {
        "ticker": "FRONT",
        "signature": "3044022031e7a7961b55db287bcc987825a632348a952dc9352abae8db1b0f3fe3b7353c0220404f9ea73f5dd8a9498fd6db2a69d5a4eb4dcc4847d0f3344567b6548676edbe",
    },
    "0x6ddb31002abc64e1479fc439692f7ea061e78165": {
        "ticker": "COMBO",
        "signature": "30440220126d341ad85e5d34cd11de5a0bd47e4e8ea494cd73af13b8b898aafd5bcd426b0220102a177dc04dd8db0200a5bf5f9607d898dc0dc39f74c23e163dfb685e897195",
    },
    "0xfa1171334cb3a0f0a91e8ca6765f10e9638d1cbf": {
        "ticker": "FSN",
        "signature": "30440220777ef5482278d859c93f673444999f875168b22330e8a17323361f918217e64e0220351d14a816fb7a1d50839653927ed8d50d9d3084b496040a79085b6a2cb06808",
    },
    "0xe5417af564e4bfda1c483642db72007871397896": {
        "ticker": "GNS",
        "signature": "3044022100eece560275b0df892c526a4248e43a00bc82eedfa958f5211f737e2572077ff7021f35ce82649c501c384f6af2d3372456d93991497021371ab34afbdc3230e45c",
    },
    "0xc8a94a3d3d2dabc3c1caffffdca6a7543c3e3e65": {
        "ticker": "GUSD",
        "signature": "3045022100a5715074876b0c47c69ebaa15cdc95cd449ddbf41378f2325b78587bb48b233002200c7192a6d3fb6f5e6aeeea99788a38ad951110fa9fa5d0bf44225612660168b5",
    },
    "0xdb95f9188479575f3f718a245eca1b3bf74567ec": {
        "ticker": "GTC",
        "signature": "304402202c3d09cc0981d58ca9ec2ae33a571534f97b76e4cc6e012ab640e51d474df5b9022006c0b957028109d2c0b0130e11300b6653bbf169b08564bd162790d1b0adfff5",
    },
    "0x5ffd62d3c3ee2e81c00a7b9079fb248e7df024a8": {
        "ticker": "GNO",
        "signature": "30440220676c4fa426085479f0502e7c3d9bbeac273e356254ae3ba78bdf61efd225d8540220649a944cc30eb5ce55cb2d2899be899981716db2807514619d00adfd9fb8f3a0",
    },
    "0x0b220b82f3ea3b7f6d9a1d8ab58930c064a2b5bf": {
        "ticker": "GLM",
        "signature": "3045022100effedc8a8cdfbe49bbdd207000529e9e56e35b5f29b677515c1dca0f715ec68e02203cb1c92a1704b6f6b64f75a161599e48183dc16e83de2b25ed12c1e32a53a992",
    },
    "0x5fe2b58c013d7601147dcdd68c143a77499f5531": {
        "ticker": "GRT",
        "signature": "304402206e352ac6947f2d2089339e567429a4de41f8c0ef4d0a7d4c0fb0a706a56e45af022011a85b67e644478abee9ed8e3f8e9db3973d54a85a9ab43d10f970be76a0d59b",
    },
    "0x874e178a2f3f3f9d34db862453cd756e7eab0381": {
        "ticker": "GFI",
        "signature": "3045022100bae69c4008de6e60a5a10a93639d003596660d587fc53da23050e71beebaac94022046ce5c5a776410d2ca615dc7914c86c903f2c39a23a5f0baf0da4972f28cf9aa",
    },
    "0x733726968ae55dc58c26dbdc193fea256a704ba4": {
        "ticker": "GRUMPY",
        "signature": "30450221008a6a2a7ad22cef65134d40fdc80ea5166e249f96cf2a2d09aa47ef0cb5989cd202207786ae41332e7970ca184ffb012a3fafe37f16cf6b1123b481f8da17188a09fa",
    },
    "0xdb725f82818de83e99f1dac22a9b5b51d3d04dd4": {
        "ticker": "GET",
        "signature": "304402201ed891d84a0085b7b0ced4e014a74fd3ffaa6943a936163e842a72c658c564f7022028460e745c62e1ea20bbf3909070aca3a327e84bd0e5b9e10bc3af7abf606766",
    },
    "0xf89250e83fa2f33058385c98c7c44e723b040359": {
        "ticker": "GARD",
        "signature": "3045022100a0ea67897d8ec63a4b9e64eb7c1823b424cb37f87e44d22204416aadea405bc002202e4c70b23a7d5edfc3342df5743a0607e808324d0d774893915b80ca00d564a3",
    },
    "0x23d29d30e35c5e8d321e1dc9a8a61bfd846d4c5c": {
        "ticker": "HEX",
        "signature": "30440220479d463edf8fccb9cbed23a60d5b105114acd488386ce4012c6b2f7724e2eda002200332471b125a1422cbbf2a987abb80e46dff07b3cc8b0646d64ff30f0c39e95f",
    },
    "0x0c51f415cf478f8d08c246a6c6ee180c5dc3a012": {
        "ticker": "HOT",
        "signature": "3045022100f85cac4adb490920b248eb17b8d8616b4b2eefd1539b3e7fda92fb143d0ea3eb02200ae938327dc0121b1027a4adb4949c0c3cb64af16b7d75c724c7b39a43a6f2a0",
    },
    "0xb82a20b4522680951f11c94c54b8800c1c237693": {
        "ticker": "HONOR",
        "signature": "304402200e2e5a3658b3f5aadac860c834af9089896085f68c42e39441d84066db51729602207906e4da95931f6def2b18260d753c19461e69e189d3c6bc90523a0b2b9717aa",
    },
    "0x6ccbf3627b2c83afef05bf2f035e7f7b210fe30d": {
        "ticker": "HOPR",
        "signature": "3045022100ec6ee1c4afcf263626e01dabdd68d0c27a2830e8bc844f19dc7d5c8d01c5e846022031b10b45f3ea9c93074dcde99adf86f2042d298bddf0107f9ba473d11adea49c",
    },
    "0xfad65eb62a97ff5ed91b23afd039956aaca6e93b": {
        "ticker": "HT",
        "signature": "304402202cc3a20b918bc1fb944b573207f4e8025b44b10bbef93c66c27cfcf22f526261022057132c7905fec1d6450034867a53f4c57ea4db958ff70755eb080f840caf6aaf",
    },
    "0x2088c47fc0c78356c622f79dba4cbe1ccfa84a91": {
        "ticker": "HUSD",
        "signature": "3045022100df67ae2679473df7d0a7c7f4a77fdec16418988169d636f68c6c4355ddb0d66902203b6d8724bcb64fe4fb8513d46ae518abdd3ec88a6037d082a435e3a4d899e5b8",
    },
    "0x60bb3d364b765c497c8ce50ae0ae3f0882c5bd05": {
        "ticker": "IMX",
        "signature": "3044022057045fbdcb5de6009d1a0b0a982363e38d2f1bdb48a383da30f52da897f544d002207fdd46c2b42b4b1447fb1b831e9f75bafd1ad21c09e0c584e5e55f58ea125609",
    },
    "0x4e8dc2149eac3f3def36b1c281ea466338249371": {
        "ticker": "INJ",
        "signature": "3045022100901789cfe2edb9719c1b3297f10c15dc3b33e7778118f2c731027795bf386c7702202db85f2c221e575fd3eeebc3a55273de7d820c2f14011bfb1ec2bdea2d372ec2",
    },
    "0xe64106154816a252da7728e35a0060f15d66cb34": {
        "ticker": "INXT",
        "signature": "3044022017c8702e4eb6de4412fe11ef9bb79b465a3a652ca9a19e8a8b9e176a5ad432a002206413dc14ad5b40e55c9dfd407e16ae768b040f15a272ce4cfb916b5d08bda2ef",
    },
    "0xde799636af0d8d65a17aaa83b66cbbe9b185eb01": {
        "ticker": "XIV",
        "signature": "304402202c2ed3446f1a9d10e6691bf6ab73fd2172335642b4d358fda5253229ee70d18c0220399d76cf157f660673f2abaac5daaad0953d14b2ad681ef84a79c18e883ee270",
    },
    "0xf6372cdb9c1d3674e83842e3800f2a62ac9f3c66": {
        "ticker": "IOTX",
        "signature": "30440220110a3388decbf42ab2fa309d56abeef2181140533adff9e72f9f89b27c1e7ad402202423fd328566079e9a9f78a0a040189daa275b5feb6c935032ff931428aebce7",
    },
    "0x4a81f8796e0c6ad4877a51c86693b0de8093f2ef": {
        "ticker": "ICE",
        "signature": "304402205ba575a2e5ec8428b5833ef20673f1a5b72ca974fb8d672ba24d592ca160b24602206eab86bbdb0aae8bfcbfc9d9c298e63a6e1e7831f82ffbf19575a1f8aacc6609",
    },
    "0xd86b5923f3ad7b585ed81b448170ae026c65ae9a": {
        "ticker": "IRON",
        "signature": "3045022100cdf97a354a7a3d78bd1e9f4fad24b7f795004ed8f8542d310a04ce9c511899bc022044f68b145028da1ce6f3ef32f3c89e0c1e943b24e403e410b43d134f3e2b7c06",
    },
    "0xaaa5b9e6c589642f98a1cda99b9d024b8407285a": {
        "ticker": "TITAN",
        "signature": "304502210086601ee708d443615e629b377537bb649b3c327b49faa654daccf232e2ab22cd0220357ac0f5cd3866211d0f90cf590f838cbb881929e8138fa67ed6ba68e9576b41",
    },
    "0x596ebe76e2db4470966ea395b0d063ac6197a8c5": {
        "ticker": "JRT",
        "signature": "3044022064f387e73f3549e57f3d3e491d891050bb22f3fbba60b50bccb05dd269a9dfae02200e7c59234ce6ae72b1c5495f14ae289b07eca39e28e168bf83cc2672e4559e31",
    },
    "0xc17b109e146934d36c33e55fade9cbda791b0366": {
        "ticker": "KRL",
        "signature": "3044022007d27359e5a392b47f7ebd60a9741d19c9d0272f607b6cb7f6cff00a816810950220205025d3e774c634540c65cbcb4acea2ca8b24bf0bc41c900e51bf9c74490a32",
    },
    "0x235737dbb56e8517391473f7c964db31fa6ef280": {
        "ticker": "KASTA",
        "signature": "3045022100de98dbd09128c748cf82459c1bebdd57e86387bdf88a50f14745195bff26747c022002036319474361dbeb6af529a51e8885d95c0e95612aaedf5731a8836b2e1b48",
    },
    "0x42f37a1296b2981f7c3caced84c5096b2eb0c72c": {
        "ticker": "KEEP",
        "signature": "30440220421592d52f76c4a708667d808924c882743f4e158529f11ba66acccf012cb84a02207c34642ab8380c5c3ddaa0ccb112b35483c41c06678589e09c1e79dc54636a16",
    },
    "0x05089c9ebffa4f0aca269e32056b1b36b37ed71b": {
        "ticker": "KRILL",
        "signature": "30450221009145d9d117de32047ffd5acc07f236b67dd59529b9fe9c3adb9e3ac0233ba0300220514c59fcc843eb3d4820d374dd17d3a415c33b4f40ff192e4c6b686ad615f932",
    },
    "0x1c954e8fe737f99f68fa1ccda3e51ebdb291948c": {
        "ticker": "KNC",
        "signature": "3045022100ac428da56dd621eaaaafc550ec9970ac2c8c5297af8bc7bcff068f35890121010220779cdf971ea6b145fcca46cdf8a926acbaa9ff97dd309ed5770cf769e7cdaa3d",
    },
    "0x9e7deba360c77949bb75d16dc81b2393c15005eb": {
        "ticker": "LPOOL",
        "signature": "30440220215b5326f5901a6105e12af79906312949c5f8b004386edb10f4cbc903ba720a02205182105c2768f6ea61e18d3731cc50daf40e1eab935f3d94cfd135fb95146eb8",
    },
    "0xe8a51d0dd1b4525189dda2187f90ddf0932b5482": {
        "ticker": "LCX",
        "signature": "3045022100b49173776bd434db9fd5be2e72f12164cf77935adf7550077fc39950124e4ab102207bfa25c2ae8022695cb92931afd61caf793693ed318750a4f6a1255bbc4130e3",
    },
    "0xe6e320b7bb22018d6ca1f4d8cea1365ef5d25ced": {
        "ticker": "LIT",
        "signature": "3045022100a4d37029bb8562a35730ce18e8f2f9acff6c52d3fb2f67dd41bfc72b97390074022053a10ad4cfba695e88596bdca5d3ce6dfed8a596546c6f3e90a531e9ca237301",
    },
    "0x3962f4a0a0051dcce0be73a7e09cef5756736712": {
        "ticker": "LPT",
        "signature": "3045022100998fd2d3d64f163248ac84d8751d447edd362dda82ce83efd6ca7cbee8ef4e4f02205a78aff434d95ee987acb6823dd2281603141af99b287482ca8f2b04e337449d",
    },
    "0x84e1670f61347cdaed56dcc736fb990fbb47ddc1": {
        "ticker": "LRC",
        "signature": "304402200d9794985a79f6bcdfbd8284811d80249ca32467f9ef66a27c0fcdff43225c88022008e2692ad59eaf726882c8505fa2cec99451060181707abd41abb30565826881",
    },
    "0x6d0c966c8a09e354df9c48b446a474ce3343d912": {
        "ticker": "XVMC",
        "signature": "3045022100eddbabb393f94fede1c3f0dd764f4921b089ae8fcfb3bc753d1be46da59507f0022058b822bbe5bdb52b18bcc883a75752c3f9d94148483043bfc5110776ee3afa4a",
    },
    "0xa353deb6fb81df3844d8bd614d33d040fdbb8188": {
        "ticker": "MST",
        "signature": "304402200530a9ae82b7124e48f766bc53a4a5b7e1618b137206f7d320e9e1f2a318c8650220054be0aee9698591b3b89224ab63f7422b9db1f17ce69707916b940dc45bfd63",
    },
    "0x91ca694d2b293f70fe722fba7d8a5259188959c3": {
        "ticker": "MFT",
        "signature": "3044022050a25d9d1399fdda062949b6c8eec25b2144e39db8aa0e8103e69da78c68b7da02202459fe33547637f812422f7f397e19fe3b363107ad64b2cd8ef9c0b540ecda90",
    },
    "0x6f7c932e7684666c9fd1d44527765433e01ff61d": {
        "ticker": "MKR",
        "signature": "3044022028163f6351dc91fb1b53d02e407b7d41e7d43eebe30e9b51ba26c227ab1447f302202257ef7960134df342c1a74c06698e5f0cb2180032dad663ad17ff6b850f4588",
    },
    "0xc3ec80343d2bae2f8e680fdadde7c17e71e114ea": {
        "ticker": "OM",
        "signature": "30440220610533443b38a8a6567b13823a5e34275edf5532e0ccb28678b4571aa485428e02202f41793d82e11b5736dd936611854e6352eb08ddd5017e9b3f036e7cc66d8333",
    },
    "0x73580a2416a57f1c4b6391dba688a9e4f7dbece0": {
        "ticker": "POND",
        "signature": "30450221009f09ff2b458405be622481bc389a31906218473270a91c855103294b705e855d02204eebc0115274f969e0890fb3f6ea239a04af5e9a5ab4cb4545cb514c0910ee2a",
    },
    "0x2b9e7ccdf0f4e5b24757c1e1a80e311e34cb10c7": {
        "ticker": "MASK",
        "signature": "304402201610449bb6144e5fa5a0dff8a3937546c63f5425c3183040bc3af91d241fb10d022050f2e11ab90ba3355081db37e2e1391eb670458095a5f960a498746239bc887e",
    },
    "0x9719d867a500ef117cc201206b8ab51e794d3f82": {
        "ticker": "MAUSDC",
        "signature": "30450221009485660477820f0c20fe0152f8697419d74b86ae006571da3a82b92df08a771502201d43918816b858ca2afd797634b4061c5fea30d7689cec8aa37df8701ce782d5",
    },
    "0xf501dd45a1198c2e1b5aef5314a68b9006d842e0": {
        "ticker": "MTA",
        "signature": "304402200deefaa443552d4634507754bf388eab461a5be5489fcaac40795ada4bfc366702203b2fbb7076432cbdaff88002e616f80de97841426b9a3cf824fe2bf57035c7db",
    },
    "0xa3fa99a148fa48d14ed51d610c367c61876997f1": {
        "ticker": "MIMATIC",
        "signature": "3045022100e90762884ff53d623b79c5bafc7b6ef9b15dbce9a0b2a6cb9606ef41d8c9f967022030aad9cd5db83ed7ab378fcfae58b462c52c147b0cc432304fb822e4edf0cef2",
    },
    "0x8f18dc399594b451eda8c5da02d0563c0b2d0f16": {
        "ticker": "WOLF",
        "signature": "304402207219cde47ae2f296ba82fa38d0170c7a28002339e39e362138c4acd5d8c53c46022016ceebddc7101938b8ac86f21b16cc2366f95c5a50b2b79e98f896a75e30b2fe",
    },
    "0x521cddc0cba84f14c69c1e99249f781aa73ee0bc": {
        "ticker": "MOVE",
        "signature": "3045022100c481954a8735c5670354f4355213d85f4145739fdb5084ee33ec448bbedfc634022012023de7dcfe7f37fcad39506ec865738bae14a12c15d31b0241250c42ceec3e",
    },
    "0x1379e8886a944d2d9d440b3d88df536aea08d9f3": {
        "ticker": "MYST",
        "signature": "3045022100b3c724140250803ab3294d03eef28097363b46446913a382990608994b7f6df40220255f5f5b4c23cbe16f8d32a28f5cae94c2f4e81eb0372e87bf884b02221c1271",
    },
    "0x106035dedca635d75d6ff05f98f25916829edb77": {
        "ticker": "NPX",
        "signature": "3045022100948dd459eb87f596ee1555f6be889bba96ff1764a2648e43c24720f0e3fd7d8f02205b3100d2b087d3555954788b1991cb4543535f84954bdccfa3a87a5fc700c35a",
    },
    "0xda6f726e2088f129d3ecb2257206adf7d8537ba5": {
        "ticker": "NCR",
        "signature": "3045022100bdd5489d4dd7cf302678e96fc09c757a7abf7f8480f08bfe39fe2032982a8f9202205d9f97b690d49f46444dd434bb95064702d91b65f5a234516c062dbd7e46263c",
    },
    "0x41b3966b4ff7b427969ddf5da3627d6aeae9a48e": {
        "ticker": "NEXO",
        "signature": "3045022100e55f2f5e587f95f627932523c95ea0d1d748dbecc9921d0def3f0acccb70efee02207c3b2fee173e99d90c28eb324832db0d14b4a477c1ec5f2468442b6404d41d8c",
    },
    "0xf30355ba14b2d3b31597ec71a4542f57e902cb80": {
        "ticker": "OCC",
        "signature": "3045022100ae2ad9e38419d203873ef47a3450a0ff1cefcc02db4820ba2d78eb366af1b828022018e0d61c5904170cc32090826efbaccfa813988a2043588fd70badaa8bb1b975",
    },
    "0x282d8efce846a88b159800bd4130ad77443fa1a1": {
        "ticker": "OCEAN",
        "signature": "3044022042dbec9c2f3a5270e89008659bcd0d3103922f210f9e4d574e189690690ece5c02207a959c67010943e050dd27a9a01f1362bb82a4aae92ed011edc748215edc115e",
    },
    "0x65dfa90171e1bd9060209405026c4e1f4a8e58df": {
        "ticker": "OIL",
        "signature": "3045022100d677ed4bffea3fb7bbbd7ff8e0c5b7e5a0a4da19492b5467cc08998446454e9002206da6c0c70ad320c4e03f132d6a65a24887d757f2e6cb6c92387fb5760bdfb818",
    },
    "0x62414d03084eeb269e18c970a21f45d2967f0170": {
        "ticker": "OMG",
        "signature": "3045022100cbcd714a57cab92d9e29b9dccf24d2cea2e37316e178fbc20fd2b4058b437e6d0220494f8fe2f94df6a1cac8db3b6a2d8e9093e275c6456a82f80c9c1aead272ce2f",
    },
    "0xb6f10be9201fb110b3aa275267a234e84ade8e95": {
        "ticker": "ONG",
        "signature": "3045022100d88317ad84d674f7e3514f762f55ca36684bc64dbc1167db15e4d148dfdfaf7a022070562a7c3723bd7e06125eea7826ab1bd22ee3e3e1c994ef5172f5b5038acbea",
    },
    "0xf4310adb83694bc4808e0c5ca8f7027c6127921d": {
        "ticker": "ORAI",
        "signature": "3045022100c095ca88734051e34e2264f62de8135067c0bcc097f22442a804ccc5894a117b0220398f6352b3284da54ca95a8cfa2121db96ec84efc96dcfaba83e55e124c32c82",
    },
    "0x614389eaae0a6821dc49062d56bda3d9d45fa2ff": {
        "ticker": "ORBS",
        "signature": "3045022100acc9116af0f76a425b4c455e6e9d6b1b59f09c8e0df801d28d0bc2741366d0fb02205b2e79fbb2e0057a7ba49dc952998b914c25130a82f8d2e1f3dd2bcb3d5012dd",
    },
    "0x9880e3dda13c8e7d4804691a45160102d31f6060": {
        "ticker": "OXT",
        "signature": "3045022100b1a01a1074550b0afa035da72fef4b5d8f26bcb00cd9dfca25c4e183fd89da32022022ad8f4396b1c3f2f0c59094b3cca92fdf0813528028474515bb4be335b912cb",
    },
    "0xa63beffd33ab3a2efd92a39a7d2361cee14ceba8": {
        "ticker": "OGN",
        "signature": "30440220690953d2d100ddca74287806ed55aecf0f33c833fb414c6a918e17c56aa6a35a022001f5be1f70b997835f8ca49a3251b1dda16b3336b48f9a987069cb6fac2e21c4",
    },
    "0x1631244689ec1fecbdd22fb5916e920dfc9b8d30": {
        "ticker": "OVR",
        "signature": "304402207b0b6c2a15167323c450f6f97c3db3f4781216a898d8e8b0f9d9807c4787435e022050dff7a08925fc5d4e63a46fe61ea18a521bd1be1719ef0cd15871fa272beef5",
    },
    "0xeaef6caf6d5894edb2d4ea7ec11ec4ab655f1cbf": {
        "ticker": "PAID",
        "signature": "304402207110f2460a44236388ef4fc64d24e185a0b08ef5ae0d8b0e2775e71fa1afc12602205da639c5a7ad3a87b5ba721a3565792b3cdb4525279c26dd21db896d365884de",
    },
    "0x7b367a058f370c0057762280547d0b974cf3ac10": {
        "ticker": "PAR",
        "signature": "3045022100f918b620d0dd82c5e59ea97b41e3dd05a7a449b1686c1a7373e48b81cae9c314022021bc9a3a8240143cd92d453ee5dc7a2fbc649ea4a67a833e0b242bcdf98fec74",
    },
    "0x9377eeb7419486fd4d485671d50baa4bf77c2222": {
        "ticker": "PRQ",
        "signature": "30450221008443ea75db0a8bbfecaa35a7867697ef097e9a09ccafd7e23742804e5bd5a92102207feeac4b67416298ec7a4b6288a111e8e60cbdbe0413832ddb5dd992ccfd77f4",
    },
    "0x553d3d295e0f695b9228246232edf400ed3560b5": {
        "ticker": "PAXG",
        "signature": "30440220366777a5244a25b29267cf4d4df4d285d3f288205aa25e453c9746315c04681c02206df03e2f914dd3561716e4fe1f8a9a5b2e4a59a4291dd777feaddcb3d660959e",
    },
    "0x6f3b3286fd86d8b47ec737ceb3d0d354cc657b3e": {
        "ticker": "PAX",
        "signature": "3045022100a0fd20a02a801d127eb9e6ad59856909ab580c6ba2ee7d8757705e4335f16e2802204949c6795e2e23b11d07f78a045b42900195f1b3d3599357aa9058a84e8058ba",
    },
    "0xc1c93d475dc82fe72dbc7074d55f5a734f8ceeae": {
        "ticker": "PGX",
        "signature": "30440220431b2054344c423673cdd3ded15c3a5ae82dcd1e2683b8ced95fcef042a4660f02205d1ba3b5e9fa516a49abea1477639b62c09a3380fadf3d4964abb87327761c73",
    },
    "0x127984b5e6d5c59f81dacc9f1c8b3bdc8494572e": {
        "ticker": "PPDEX",
        "signature": "304402200b590012ea3cb13b39348b11863fc7073f54ec13b94b5345d2d021695ea7056402207c7990ae61a9e4cbac791896d57da4609180f5738c1d806c27ffeb09ea251e7e",
    },
    "0x990f860968714c2d16e91ec0cd9709a94264cf64": {
        "ticker": "DOUGH",
        "signature": "3045022100f08450a344629b75ff4f4ac08a21cea60603a46c0c2795e2782b770180838de602205a50961be1e93f6b71b91a3444e70c13c22ed37315c0767e45bca8835c874554",
    },
    "0xad93e067e149f0a5ecd12d8ea83b05581dd6374c": {
        "ticker": "PNK",
        "signature": "304402204c37665d5ae80c17bb4710b7db4e924a8479675c8255637566b8ebb56d052dc902204091c0317a8497f4267eb1c70d3e953cc9332785f59a912ddf80cb6e91af8fd3",
    },
    "0x4e92f6f10496f3fcb790cc6969cc15b87c8f9199": {
        "ticker": "PNODE",
        "signature": "30440220351b833f6ba2a58c10d9bf92a04dd0e507bb0ec1bf0cc228da153e98a648324e022070025b9a70ebf6a9e8367dcda3ee5d9f1325f585516d51627a60e9875cb48bf4",
    },
    "0x7dc0cb65ec6019330a6841e9c274f2ee57a6ca6c": {
        "ticker": "PLU",
        "signature": "304402207f4935970038b11ffc3bee1567913d40f1ae3f30471d752a8531ba795a89cd51022035938e84b0edf51bb21d820eb561d08567dbe8918b396136e5afdc9102b7ef14",
    },
    "0x8a953cfe442c5e8855cc6c61b1293fa648bae472": {
        "ticker": "POLYDOGE",
        "signature": "3045022100932c4b2dd1a5e77023209930d92f84cf27d9629b75f8368434760cd2bde68924022013d2b965abc44f75bb5c7d91609a123d5dd050d4ed0ff6fac4ae9cf8a00aeb29",
    },
    "0xf4b0903774532aee5ee567c02aab681a81539e92": {
        "ticker": "GAJ",
        "signature": "30450221009f4e2e496ba2ec107711f8299f17cff60df06aac4f746ac5d8b2af63411db53202202b1bc6868e97589d9881575e323553e3b6f0392f374bf57d6444e6ebffb605f8",
    },
    "0x4c16f69302ccb511c5fac682c7626b9ef0dc126a": {
        "ticker": "POLYBUNNY",
        "signature": "3045022100869cbabf75d726a396640260dff557a3ec9013beb83a31b5a7298f2f24ecdb31022015dc5e73d6e454a611a9f6f5683646c1d070d6e9836bbd24393d03e43b2a3043",
    },
    "0xcb059c5573646047d6d88dddb87b745c18161d3b": {
        "ticker": "POLY",
        "signature": "3045022100d8c158f7cd8b12a3931e4e9ccdeca52b5e70a8ac95a1ecafc12755a7609e1f3a0220762baedaffad7867ad7bc3ab7c267e42f93dc428fa4062a8d3f92f626be95585",
    },
    "0xc68e83a305b0fad69e264a1769a0a070f190d2d6": {
        "ticker": "ROLL",
        "signature": "304402204eb664cfb3381398d621333893bb0348191f8feaeecaffd6a7018be0614f455c02200374feeedd2efadfcf538f80cdd1ccac63d18363b7d48bddd85202cba52cedd3",
    },
    "0xc56d17dd519e5eb43a19c9759b5d5372115220bd": {
        "ticker": "MOON",
        "signature": "3045022100804d18682dc5993c71ebd91a8b6fd2226b2359e114848f838488cbaa1ea412360220232804817181ca82b42ae0f76dbf668407185eab9bb1a01d76471f333343c9bc",
    },
    "0xd0f3121a190d85de0ab6131f2bcecdbfcfb38891": {
        "ticker": "YELD",
        "signature": "3044022065de87aef41deb4b0c77747e9a6e23f4acafe7680ab3a3d57d3b9bc31b71a8e502205057cd1023774cd5773ffedb71a693c63822e1e666a960795229d172c1630546",
    },
    "0x25788a1a171ec66da6502f9975a15b609ff54cf6": {
        "ticker": "POOL",
        "signature": "3045022100c847f7dc9b69206c02c8bae2374dfb365ca9131cb2b919b9e6659cd6f9af3c4102201639148e169f38ce66414d51ad8575ff60daa9d6e43782db9eb0dbbe7184c2ac",
    },
    "0x0aab8dc887d34f00d50e19aee48371a941390d14": {
        "ticker": "POWR",
        "signature": "304402203c6468e0d9c6b1727eaf7ecb9caba9c26a99bc5fa5564f8b75ddda5ea20594bb0220448956a4ba2f5dc4831e7ab79b645ed01e2151aeeadeff42493196b0fbaab3c4",
    },
    "0x121ef177a0489271b4339bc29ee64609b47d43c4": {
        "ticker": "PBTC",
        "signature": "3044022068140d5c2ddf3a44fca2f26a6501ad1403292cd35ec6d1a84592eb1d11bc2170022046361f0075c1a0f7d3c9304fdecc050babc1edf9b5ab030bd233f168933330bf",
    },
    "0xcfe2cf35d2bdde84967e67d00ad74237e234ce59": {
        "ticker": "PUP",
        "signature": "304402206a285914d15826aae6434475ae938c43063a4d92aa1784609ce54de24f9cea6b02207881f884d2220550021c997a0df85e83c38c1e04add8ba2de9e994d5129e3b6a",
    },
    "0x9af3b7dc29d3c4b1a5731408b6a9656fa7ac3b72": {
        "ticker": "PUSD",
        "signature": "3045022100bfa2a989bbe0efcf2c8da49b356da55c00b4b434afcfe4eedef863ccff92df4f02205d6ff069bcc8c60d2c891528498667057223ed65ca26867b8aaceb991d113d19",
    },
    "0x5a3064cbdccf428ae907796cf6ad5a664cd7f3d8": {
        "ticker": "PYQ",
        "signature": "3045022100fd7084feafd0b7aa463ec98d8e3df78c2aa0c661d56b6d0e1b247dd9fb0cffdb022040b00978f591d18e2d2f197c1d511709302ae970a2d20eadbec36fdfdb254eb8",
    },
    "0x580a84c73811e1839f75d86d75d88cca0c241ff4": {
        "ticker": "QI",
        "signature": "304402206efdc615e0a058701d70bd924a858d8cf38214d1136a170e19a680ae31395f23022049c98bf1df2c5d7d2d1739060c0554f8631e278919b3a1e739246bd848c2bdb7",
    },
    "0x831753dd7087cac61ab5644b308642cc1c33dc13": {
        "ticker": "QUICK",
        "signature": "3045022100c098673b45df93df9cf10141bf5e43c673dc0f8b5d5ccd9f1e9bebea6a8ba022022000e645464709f47288a7b38e30054094854a898efb478fb31cc2971564d7ef6e",
    },
    "0x780053837ce2ceead2a90d9151aa21fc89ed49c2": {
        "ticker": "RARI",
        "signature": "304402201d63d1eca3dcc61f0c155bd0cdb34c5cd7c2ac446a64d8a25c2fce5dfc0ff11a022012243245f3ce383d22c2bf8a164f23f9b59c1ea9ce8a9abaf681df4dcc3e30d6",
    },
    "0xd6c23852b94fede6ab571e4b4cfdb745b49dc9eb": {
        "ticker": "RENBTC",
        "signature": "3045022100c05722046d1029e5be43e39344b8bbf08ceb191e1cb308bd56d531769dc4268a022017b53fff3c2aa453070696d94037f583fbda8f68d5758afa701eb3b141f4bc0a",
    },
    "0x61299774020da444af134c82fa83e3810b309991": {
        "ticker": "RNDR",
        "signature": "3044022042d62a1be583b33bf641857e3519ef7422a350e21258f030b8c25fdd4c7e8260022052f4e10c82849f947f4a5acb87db379c56c84576bd083073c16a6a59869096cf",
    },
    "0xce829a89d4a55a63418bcc43f00145adef0edb8e": {
        "ticker": "RENDOGE",
        "signature": "30450221009459770c7f112f22fe117f69f315d68f97fe0e4627415732e4ff57e9f768052602206a26ff549a74a8c5c40e114bb19e3f2cf30c0f7dec91d5745148ff218b5236ff",
    },
    "0xc4ace9278e7e01755b670c0838c3106367639962": {
        "ticker": "RENFIL",
        "signature": "3045022100bb66e93c3aa7aead7ef944c20614e837aa963d064330d0b98ead27c915101c1702204fee135f515103754423dd144d822efadceb848489508c2477150969a88242e1",
    },
    "0x70c006878a5a50ed185ac4c87d837633923de296": {
        "ticker": "REVV",
        "signature": "304402203d883d329adb570698e893c5ad8f83b3716e89f5e8749bac4a742c83f741e5ca02203884a0cf71fdedbb30d41ec293722cc39e89b2b31fd9d0ac4363020c39bbcaf1",
    },
    "0x89c296be2f904f3e99a6125815b4b78f5388d2dd": {
        "ticker": "RCN",
        "signature": "3045022100b5b8c036b1906e9f286ac8ef6ccf20bdbd944c22476b1a4fc7a8cff541cbfd85022060c5a569ca87042188efaeb352fac5cd41a9f0416aa304e713684232cb009ed1",
    },
    "0xbe662058e00849c3eef2ac9664f37fefdf2cdbfe": {
        "ticker": "RLC",
        "signature": "30440220763bbbb5486d36f9d455764c8632008b1c5b67d45c099e3ad492b8ecbbd290f5022007ff2d836f1668768f9abde172de2a7e5d11299f12a91b1172e17b0f4263c610",
    },
    "0x7205705771547cf79201111b4bd8aaf29467b9ec": {
        "ticker": "RPL",
        "signature": "304502210088e2753ca251b934fb95645f451b30f0d69b72228f76e671c71f4784d80523df0220695ecb3258a48d7fc434e73910e1718b20a6359e4501c796d541337a2c820a79",
    },
    "0x16eccfdbb4ee1a85a33f3a9b21175cd7ae753db4": {
        "ticker": "ROUTE",
        "signature": "304502210089f5b01d29803b7a94d038092c05f2378ed2fb73a4bccb8a08fec6e75f43836302206851dfb9b543f033484550b1041bdc9bed00d43a03b70bc3d312c8edd94c8314",
    },
    "0xc3cffdaf8f3fdf07da6d5e3a89b8723d5e385ff8": {
        "ticker": "RBC",
        "signature": "3045022100f849984f462557a6b74b48337387eaff6439cba4e90242e958d91d5e3c88e5a30220740585ce2bec064bbc8c9a4a77b1bd7c923ffabf97b0f1ee6535237d759985c4",
    },
    "0x66c59dded4ef01a3412a8b019b6e41d4a8c49a35": {
        "ticker": "SDO",
        "signature": "3045022100b9cd7e36d080fa92721d5ea291caa453784a3b84070fe0387ad5018792a0648a022028b80062c9598c4977b9c2e5e908371599801732ee8341f55c0758d51f692d3d",
    },
    "0xab72ee159ff70b64beecbbb0fbbe58b372391c54": {
        "ticker": "SDS",
        "signature": "304502210084d9c869d4bc12ca8a6653a736c52284fbf9503bba4659acc52161f7db97213f022056fcad1644a1cac28d21a905c0376550cb430105dea74112b841e5a20b2564bf",
    },
    "0xbbba073c31bf03b8acf7c28ef0738decf3695683": {
        "ticker": "SAND",
        "signature": "3045022100c21119cb079c76295c8524b3091064854b02ff371416a1a5e48e9095edb3bfe502206f785716e63e80a99eade8d3fb273031614393e086883d866b5f9db2809b9ebc",
    },
    "0xc6d54d2f624bc83815b49d9c2203b1330b841ca0": {
        "ticker": "SAND_",
        "signature": "3045022100e49d3eb141bdd203f2c1b85d351540ab0632be7a398a39498182387c4adc3875022027f9a9c5f5927727682d71e7224828dbc79926991cee0521501c840f8cb2e57d",
    },
    "0xeab9cfb094db203e6035c2e7268a86debed5bd14": {
        "ticker": "SPN",
        "signature": "3044022077e4b88efdb063bfaea7f1cd49b1e3698c7eec3f700889a34f2df8c5e6dc5f44022036ccfac493a01ef8268aff6b295633b36ec5df127c74daf35d8c3ea2eae91f01",
    },
    "0x48e3883233461c2ef4cb3fcf419d6db07fb86cea": {
        "ticker": "SENT",
        "signature": "304402201f023fd294b88242f14e4061fe352aee3aeceaf28d47c3989f1ea7aa97f2aa0302206856a4013b36c60d7a0fdcba49e43f007d252cddc25cecad7bcaf71e59eab064",
    },
    "0x6bf2eb299e51fc5df30dec81d9445dde70e3f185": {
        "ticker": "SRM",
        "signature": "3045022100c649349ccb7fa669ae7fcb0397f145d7ac72f1fb472842b28d838b250595c9ef02200b67a2fb4e51dee5b542d74d62dbea0892e0fc59e9a0c02772670cf83f327056",
    },
    "0x068180071617528606371c31892ecbf2b70ac1d2": {
        "ticker": "SPI",
        "signature": "3045022100a62733824ef978d69bf87b0b58cf62781c4dec1986e88c79a9ce90252e1a48c902202ce77ea5f7759fe688619f0a8bec2528b459c715e47c3d43636e3d94f2c9c39c",
    },
    "0x190eb8a183d22a4bdf278c6791b152228857c033": {
        "ticker": "AGIX",
        "signature": "3045022100c8522f1c1ab31af32f71740b443c3cc1039c74764c59d4ea15cad27c4097410802202a2dacb662495b79a7573dad329ae177d40bc37976d94a9d4ec9c1eedd21c7e7",
    },
    "0x1132f58810ee9ff13e97aeccd8dda688cc5eb8f4": {
        "ticker": "SRAT",
        "signature": "304402201ed3b017ce531c52a7f9df50ec38610d8463b7729e44511f1bf0f58d07bab49402201af4319aecd3c33a0209e355e195b2cc20fff1292b39f0aef67a10d0d2be35cf",
    },
    "0xccbe9b810d6574701d324fd6dbe0a1b68f9d5bf7": {
        "ticker": "STACK",
        "signature": "3044022011c1caac8801e8a3209b04f8516f2618339ac18f9ccf2438f9793545772caaed02207956082348e52ab602bd61bc732c93d18a7b288d9b5c20a4bb9309876be72a6a",
    },
    "0xeb5c9e515629b725d3588a55e2a43964dcfde8ca": {
        "ticker": "STAKE",
        "signature": "304402205f7a4020031482ade4568896f97a0575a3dc1919646e7d8b4b414739d0847c4a02200d254c84b384ec61376d83d53a7b2d14bd246acbce7a8d3234636108fb3c1322",
    },
    "0x9e2d266d6c90f6c0d80a88159b15958f7135b8af": {
        "ticker": "SSX",
        "signature": "3045022100bc6a6b01d812191c0c9fd8d5936ada0a2230ce6d0b7cdc1d31271f5340f9f4fc02201694ec8737dc0f7d320fc811a6d6d5f077f84490f8aab578e1551af360fca37d",
    },
    "0xbbbe128568222623d21299f019a978c8587b33dc": {
        "ticker": "SWISE",
        "signature": "3045022100cbb1f417533c99ff0635b3b5d79de8af2f58c81dce21c45889f5d558cce33bf4022049ea64d7339b8f65c1fdc22dce407a2bf3e39f90c08713019eacb3a42d58e2ee",
    },
    "0xe111178a87a3bff0c8d18decba5798827539ae99": {
        "ticker": "EURS",
        "signature": "3045022100cd91f0b64f1981b6c541efa3d28c1eefd0df08cde9c385b6eebc5a1d76725f2e022047f17ddc307ed6941857f5cab5acbd5f1bebc3551a987d8614342c7379825672",
    },
    "0x779b299ea455d35a44fe9bac48648be22c08dea2": {
        "ticker": "STC",
        "signature": "30450221008682480e8ac8a19f23dc4d21ef7a6701d663e0b047e9315bb214695515b076fb0220295e4854bb0862e5aed648cb7fd4967d0998e2631ceb23897e5c2983048513e3",
    },
    "0x0b3f868e0be5597d5db7feb59e1cadbb0fdda50a": {
        "ticker": "SUSHI",
        "signature": "30450221008884a0addca753dc4393d8e5541819547c6d6e5ead02a5e1a7235fcddbb0c64902204c5c0f309402efa8767c9114ccf2b866d2c0a2aa05cb7d14aa18e00bf841a508",
    },
    "0x6abb753c1893194de4a83c6e8b4eadfc105fd5f5": {
        "ticker": "SXP",
        "signature": "304502210091117e42be90ced27fecb5c5061e7924163c8fa2aa5f97ff015591825ff034bb022036fb7411de65f0cf2c3f657b40b8426447b5d3ac5f2d7f17e1f02d7a705834b5",
    },
    "0x67ce67ec4fcd4aca0fcb738dd080b2a21ff69d75": {
        "ticker": "CHSB",
        "signature": "3045022100b336701b18926b318036944930d9bc8961785d5b8bff98108909e99ca63a54410220687e186ba97314ef15a15d3b753d4162240505a2a6730b65a30d96b5645a4c79",
    },
    "0xf81b4bec6ca8f9fe7be01ca734f55b2b6e03a7a0": {
        "ticker": "SUSD",
        "signature": "30450221009977e9457dbc4e54cbeac056370046e91aba5cce6b2f4ed218c520404a352f4502203b23604adeab58c71558dcb261054b3bdbe16aa29f58c767807765ee1b3b3673",
    },
    "0x50b728d8d964fd00c2d0aad81718b71311fef68a": {
        "ticker": "SNX",
        "signature": "304402201f2c8892ac4f7edda2054cc124893abebf1b1341609c310b010c24f3eb5d53ab02205622033d1f4ded85a0de1061b3e65fc707718029f02b75d56bfe470e6061661b",
    },
    "0x10635bf5c17f5e4c0ed9012aef7c12f96a57a4dd": {
        "ticker": "TAP",
        "signature": "3045022100f0507c1386c9af0892dfc5a7fd72b880126ebaa601b0c528655dbe2f434cabbf022018de2cb1bbb5d67d633e25429573ed9210ae5b8466acdf64a858d23a9511fd38",
    },
    "0x2596a8b90d39eb89d5668ca5b6dee54e9ae3d4c0": {
        "ticker": "TAT2",
        "signature": "30440220618d64e47a74b6b398341005a1ea578c72789f45f74351655c9b2fd435a53452022078ab85fec2bd2f10513770e1bee7f36c6c9503f4b8865e5d0f9d774596a15586",
    },
    "0xdf7837de1f2fa4631d716cf2502f8b230f1dcc32": {
        "ticker": "TEL",
        "signature": "3045022100df6734e8dc79763f3a2e3ccbbe547f9380f24a5d25ec0e7ef5e3357aa23e105b0220484abf1214cdc1bda02a17900fc9f3ae73ffbddbebb101f51ad5674c860d4aa2",
    },
    "0xe3322702bedaaed36cddab233360b939775ae5f1": {
        "ticker": "TRB",
        "signature": "304402207d9872134e96a0deebacd719b1a3d684a8a3282449d721ae0bcdfdf1a864f6e402200c07a0aa5e0d897fdc795ccc9794e5e288b8e22e30594b25f8afbacb053b4370",
    },
    "0xac311fa85e8645bb1db53ecc04fa8d41997facf8": {
        "ticker": "TIDAL",
        "signature": "30440220347494aaf6a8f7b0bb0d7fc44955c6d9be79f3b969a5032a1a94eaadedf4fefe022052ffbf3c709f8ea5840f68e014edb071b234563d901965ee69a4db5a4f5b4b42",
    },
    "0x23fe1ee2f536427b7e8ac02fb037a7f867037fe8": {
        "ticker": "TORN",
        "signature": "30440220707fcc4100917670748fd1d0268837866914590842aae8ccda38a1c4f06f996c02206d66134328fecd3d9a19c560e102f5eb870030c87a2c65ae55a9b9a79665ee1e",
    },
    "0x2e1ad108ff1d8c782fcbbb89aad783ac49586756": {
        "ticker": "TUSD",
        "signature": "3045022100eef61cb3c069df526c50ffd331b25fdccff35a1e6485047b00617ad316e305cc0220235b4b5259c38256e11d6cf9710b4ec6ab02aad6509754a5e8cd63c7bab9a56f",
    },
    "0x3066818837c5e6ed6601bd5a91b0762877a6b731": {
        "ticker": "UMA",
        "signature": "304402204c7d36dceec2aec4512964e8ec79a69c64dc3b306f6bab57f7a8e4bff48e464a02200ab10dc46282b49981110c5c30e192dab11542e2a4987e3e40055058b176ff61",
    },
    "0x5095cc6511b70e287bc525aa57afbc60eb73ac47": {
        "ticker": "ERSDL",
        "signature": "3044022049b075aa93c410fbcc94ea0019774ed7cc28d4579653cb8a77529b30de2899ab0220535ce5daa886df77daef8638a29764de550515896a873de93a41198f13093370",
    },
    "0x7fbc10850cae055b27039af31bd258430e714c62": {
        "ticker": "UBT",
        "signature": "3045022100ea250710f1e7f25a56fd4abac52ee893c8c09099ec56e7918c1146bb381b511d02205243299925580b29209f9de2d85dcb7a3d0b35358330cf55cf75434b8d5d8d3a",
    },
    "0x4ba47b10ea8f544f8969ba61df3e5be67692a122": {
        "ticker": "IFUND",
        "signature": "30450221009b57ca04ac3d25d14d1bae394ba30c0fa459154f7dfe702953db535d78c4287a0220440a23085a1004f725bac0d9e20d279ef854b14c27379bb71df1c02e47d7de64",
    },
    "0x5df1a47e05b4caba0ed3df13662642f05314764d": {
        "ticker": "UMX",
        "signature": "30450221009a031e72a2716c560623d166433722ea1ddc68739be51dfabc80d9b18dc604fc022076849337d463345ddabe847a931aa220ca6677e1d4170fd7491a90e960980282",
    },
    "0xb33eaad8d922b1083446dc23f610c2567fb5180f": {
        "ticker": "UNI",
        "signature": "304402206fd1ec530e75eadcd8adca249ec060cec1ee532d2a63c6617de8ec73cc62456502207d2e780bb93cca35ea810596b764cbe8fa72a593379ff4f48512687cc4611fd3",
    },
    "0xadbf1854e5883eb8aa7baf50705338739e558e5b": {
        "ticker": "UNIWEWM",
        "signature": "3045022100ea8aadc21ac89ff39521db07d8e1d438f3085e7455ae2aff9565b5faf2cf573e022072493a0d257545f3cda3fe6d4922a01fef131a1ffd2eb1c5a760c5bb1b05b9ef",
    },
    "0x853ee4b2a13f8a742d64c8f088be7ba2131f670d": {
        "ticker": "UNIWEUSDC",
        "signature": "30450221008340415d778cff8a08d547eed2ff628523e59759d5ed14e19768f15f5bdad1fc02203283dad240ae0da57df3a62869af97b31833db7741f235a2ee2838e86192a4b1",
    },
    "0x6e7a5fafcec6bb1e78bae2a1f0b612012bf14827": {
        "ticker": "UNIWMUSDC",
        "signature": "3045022100ead796edd11d1451ca745a495b92b00e193b5798d038e274c31c04de90059f0e02201bc8be5b7b1151ee55e1f0061d44285e075a2c408c1063e09c5f0935fae22829",
    },
    "0x604229c960e5cacf2aaeac8be68ac07ba9df81c3": {
        "ticker": "UNIWMUSDT",
        "signature": "304402205c8d7db793ff7e0208eb885646b28bf4a0a8d94036dfcfc60749d03fe183492302200fe71aafae326ade9464ccaa6691a221177e06ecce1dfaa817888718b79b502b",
    },
    "0x2791bca1f2de4661ed88a30c99a7a9449aa84174": {
        "ticker": "USDC",
        "signature": "304402200ef170e3f0a71b03ad7758e201d9861102f78cabcbbc5dbfecc5a946236438c7022043c591264f57cf3026f12f98e9b5112d2dd2e7b2d6c04206ace2475ed21e80f4",
    },
    "0xd07a7fac2857901e4bec0d89bbdae764723aab86": {
        "ticker": "USDK",
        "signature": "304402205613ac257b797ef31c6890956a12d192b89d03a7a2078898dafb0b79b0cb1b5c02202dfe6eff014166937d540742538335257dd9f5f9dce2491b7c05dbbbe92f4080",
    },
    "0x09c5a4bca808bd1ba2b8e6b3aaf7442046b4ca5b": {
        "ticker": "VSP",
        "signature": "3044022046419ec25dab4fc07fe8a0936cea029dfc04ce9cc9955a7d1b16c5f6361a3b940220336259779ffbec502fe8a318101def15008d5077e8ed88a0af1ea8db15bee2a0",
    },
    "0x054c42b6414747f5263b4a86f21b1afad00326bf": {
        "ticker": "VGX",
        "signature": "304402207adb14a4402bd7f0e74ed94c4e0212785b145e671f2ae9b888ef6fa1e85cddd7022070f8f3ea98dd86ae7434cece3acf132dc6f0a34b4e51c439c03807e4abefc580",
    },
    "0x4c4bf319237d98a30a929a96112effa8da3510eb": {
        "ticker": "WEXPOLY",
        "signature": "3045022100bc695093f7bc1f4a5eca39fb0ad45bb7d3421fb24c0b582456e7a3e6556cfeff02207aa4ef9f0b705bbc5ee20fc26a27d8292a21ddf5c3b1f679bbe224d54246397c",
    },
    "0xcbab27b2d5b468eea509bd430100922341bc3f1a": {
        "ticker": "WPR",
        "signature": "3045022100b2022385b9fb931b51eb9dc58b6acfd9329dc01fd876143fca05dd13f1a00072022033c14a9f00ce8b5ca8618f64bde930921e09af636208776f3a0520a247747508",
    },
    "0xfce22ad802bbc9fe74534575b166d06198bd66ea": {
        "ticker": "WOOFY",
        "signature": "3045022100a708d0d98a979ed4407844ee000dc827f2812baf88abb3b547f644c610a467ae022064c0c24a928ef14c212a73ebffb566150861a026ebb503a2886e331766fb1fa8",
    },
    "0x1b815d120b3ef02039ee11dc2d33de7aa4a8c603": {
        "ticker": "WOO",
        "signature": "304402206fa91ec7507c0b90e8072900b0ec42a60496deefa710cb0a942178610021100f022040c899804634ebce099a9c8ffb3bd1e34a229e73a4190c0f320662fda1ee0994",
    },
    "0xc30dd0cce2e683aa68c87c280e73beb3dec9b3ba": {
        "ticker": "WCELO",
        "signature": "304402204533f1d264f27aaf2bded35c70e342f9e093d3563e21a69581099694b76296630220224c28490a748c183cda281bada3a27958f561fdc33f71e998d62001a21f4e1f",
    },
    "0x7ceb23fd6bc0add59e62ac25578270cff1b9f619": {
        "ticker": "WETH",
        "signature": "304402207b674dc939fc9958aef413f7e1b3d755ffbcc8240933d7b6d888e4adeae5c8a802206dd9b1c0c8291e28977497ac8139902446fbb770cd82f6aad7eb7c7b72d69b50",
    },
    "0xede1b77c0ccc45bfa949636757cd2ca7ef30137f": {
        "ticker": "WFIL",
        "signature": "3044022078b0af0f86bc7b62ca450d4868649cf8df6973d3331a7ea244abec015abbbf5c02204a80853c3be77db55d88d647b8a1522defa0a0ea6a30482ca815453d180451d4",
    },
    "0x0d500b1d8e8ef31e21c99d1db9a6444d3adf1270": {
        "ticker": "WMATIC",
        "signature": "304502210083ae7e7508bbe06a1b6c59543e62a3dcb2e825a6c39c1adc9ccd439253ac7f4d02201f60dcd93ad430b79b4b0b39571f5f15988233c04e5d2e45a4ecce1639fabf07",
    },
    "0x692597b009d13c4049a947cab2239b7d6517875f": {
        "ticker": "UST",
        "signature": "304402204ad3702836823f826e934b945997b15c3238887767be33ef21f1aa5c9337c95d02206ed2ae004976c4d4ef8d6d62a785602f620490ad36b447d08de0aed9e46a4a02",
    },
    "0x22308317a4aa9e8196065e0b3647e582d44c4d7b": {
        "ticker": "XTK",
        "signature": "3045022100eb4c86fd40f2cb862f560b6f2c39a7e20cd7756ccee500e5a71105d7ebecfec002203f445753745434711b27ad0470b5f407933742a90a1abfc0a63d332606e91361",
    },
    "0xd2507e7b5794179380673870d88b22f94da6abe0": {
        "ticker": "XYO",
        "signature": "30450221009c6c9847cbb09c138ed3b5af562c43d8a7e749cce38bfe69db795a409c74afc202202931be6a66a39f91e7e2854753e98cca122aac46995fa708778b10e2d5249dbf",
    },
    "0x103308793661879166464cd0d0370ac3b8a2a1cb": {
        "ticker": "YOP",
        "signature": "3045022100e0eb845c5687d6ff81a15dbd88b1e7389f98ba31bddd3e649714bffcf8c50d5e022079c623c5aa3f0ce51dceffef85c4cd38fed5425e528d320c3b75904f5c52294a",
    },
    "0x5559edb74751a0ede9dea4dc23aee72cca6be3d5": {
        "ticker": "ZRX",
        "signature": "30440220214308fee0366583ad1a0b7d187d3ef60487cf052fab2961d58863aefe56f52c02206fdc734b775ed5197a1f97010370e05eee2c6486f1df04beba0adef0a2d64c39",
    },
}

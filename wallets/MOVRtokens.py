# Uniblow ERC20 list for Moonriver generated on 2022-02-06T22:36:25
tokens_values = [
    # Moonriver
    {
        "Wrapped MOVR (WMOVR)": "0x98878b06940ae243284ca214f92bb71a2b032b8a",
        "USD Coin (USDC)": "0xe3f5a90f9cb311505cd691a46596599aa1a0ad7d",
        "Dai Stablecoin (DAI)": "0x80a16016cc4a2e6a2caca8a4a498b1699ff0f844",
        "Frax (FRAX)": "0x1a93b23281cc1cde4c4741353f3064709a16197d",
        "Frax Share (FXS)": "0x6f1d1ee50846fcbc3de91723e61cb68cfa6d0e98",
        "Crowns (CWS)": "0x6fc9651f45b262ae6338a701d563ab118b1ec0ce",
        "SolarBeam Token (SOLAR)": "0x6bd193ee6d2104f14f94e2ca6efefae561a4334b",
        "RiverBoat (RIB)": "0xbd90a6125a84e5c512129d622a75cdde176ade5e",
    },
    # Moonbeam
    {},
    # Moonbase Alpha
    {},
]
